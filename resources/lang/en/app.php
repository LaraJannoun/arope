<?php

return [
    'footer-title' =>  'WE KEEP OUR WORD. ',
    'link-home' =>  'HOME',
    'link-about-us' =>  'ABOUT US',
    'link-social-impact' =>  'SOCIAL IMPACT',
    'link-products' =>  'PRODUCTS',
    'link-careers' =>  'CAREERS',
    'link-contact-us' =>  'CONTACT US',
    'link-apply-for-a-loan' =>  'Apply for a Loan',
    'call-center-number' =>  'Call Center Number',
    'our-address' =>  'Main Office Address',
    'working-hours' =>  'Working Hours',
    'follow-us' =>  'Follow Us',
    'faqs' =>  'Try our FAQs First',
    'try-faqs' =>  'Try our FAQs First',
    'apply-now' =>  'APPLY NOW',
    'apply' =>  'APPLY',
    'locate-us' =>  'LOCATE US',
    'read-more' =>  'READ MORE',
    'refer-a-friend' =>  'Share',
    'lets-go' =>  'LET\'S GO',
    'personal-information' =>  'Personal Information',
    'contact-information' =>  'Contact Information',
    'professional-information' =>  'Professional Information',
    'submit' =>  'SUBMIT',
    'loading' =>  'LOADING...',
    'full_name' =>  'Full Name',
    'email_address' =>  'Email Address',
    'your_age' =>  'Your Age',
    'employee' =>  'Are you an employee?',
    'income' =>  'Your monthly income in $',
    'guarantors' =>  'Are you able to provide one or more guarantors?',
    'business_owner' =>  'Are you a Business owner?',
    'age_business' =>  'What is the age of your business?',
    'expenses' =>  'Your monthly expenses in $',
    'governorate' =>  'Governorate',
    'yes' =>  'Yes',
    'no' =>  'No',
    'view-all' =>  'VIEW ALL',
    'first_name' =>  'First Name',
    'last_name' =>  'Last Name',
    'dob' =>  'Date of Birth',
    'gender' =>  'Gender',
    'nationality' =>  'Nationality',
    'marital_status' =>  'Marital Status',
    'place_of_Residence' =>  'Place of Residence',
    'country' =>  'Country',
    'mobile_number' =>  'Mobile Number',
    'phone' =>  'Phone',
    'position_applying_for' =>  'Position Applying for',
    'level_of_education' =>  'Level of education',
    'major_of_specialization' =>  'Major of specialization',
    'last_job_title' =>  'Last job title',
    'years_of_experience' =>  'Years of experience',
    'field_of_experience' =>  'Field of experience',
    'message' =>  'Message',
    'attach_your_cv' =>  'Attach your CV',
    'choose_a_loan' =>  'Choose a Loan',
    'age' =>  'Age',
    'address_city_providence' =>  'Address (city, providence)',
    'male' =>  'Male',
    'female' =>  'Female',
    'download_our_app' =>  'Download our App!',
];
