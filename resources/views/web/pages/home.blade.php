@extends('web.layouts.main')

@section('content')
<div>

    @include('web.layouts.headers.header')
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title font-bold text-blue" id="exampleModalLabel">Contact Us
                        <i class="fas fa-phone-alt text-blue mr-2 my-2"></i>
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body text-center">
                    <div class="widget">
                        <ul class="widget-list">
                            <li class="media pt-2 pb-3 border-bottom">
                                <i class="czi-phone font-size-lg mt-2 mb-0 text-primary"></i>
                                <div class="media-body pl-3">
                                    <span class="font-size-ms text-muted text-blue font-bold mb-1">Customer Service:</span>
                                    <a class="d-block text-heading font-size-sm" href="tel:1291"> 1291</a>
                                </div>
                            </li>
                            <li class="media pt-2 pb-3 border-bottom">
                                <i class="czi-phone font-size-lg mt-2 mb-0 text-primary"></i>
                                <div class="media-body pl-3">
                                    <span class="font-size-ms text-muted text-blue font-bold mb-1">Motor:</span>
                                    <a class="d-block text-heading font-size-sm" href="tel:01905757"> 01 90 57 57</a>
                                </div>
                            </li>
                            <li class="media pt-2 pb-3 border-bottom">
                                <div class="media-body pl-3">
                                    <span class="font-size-ms text-muted text-blue font-bold mb-1">Medical:</span>
                                    <a class="d-block text-heading font-size-sm" href="tel:01905790">01 90 57 90</a>
                                </div>
                            </li>
                            <li class="media pt-2 pb-3">
                                <i class="czi-phone font-size-lg mt-2 mb-0 text-primary"></i>
                                <div class="media-body pl-3">
                                    <span class="font-size-ms text-muted text-blue font-bold mb-1"> Property:</span>
                                    <a class="d-block text-heading font-size-sm" href="tel:01905770">01 90 57 70</a>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="modal-footer justify-content-center">
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn text-light  dark-blue-button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slider-images-container">
        @foreach($home_sliders as $key => $home_slider)
        @include('web.components.slider-object-home', ['image_order' => $key, 'image_source'=>$home_slider->image, 'image_main_title'=>$home_slider->title])
        @endforeach
    </div>
    <div class="container home-options pb-5">
        <div class="row m-0 w-100 h-100 justify-content-center">
            @foreach($slider_items as $key => $slider_item)
            @include('web.components.home-option', ['image_order' => $key, 'image_source'=>$slider_item->image, 'image_main_title'=>$slider_item->title,'link'=>$slider_item->link, 'box_color'=>$slider_item->color])
            @endforeach
        </div>
    </div>
    <div class="background-gradient-grey p-5">
        <div class="container custom-container">
            <div class="py-4 d-inline-block w-100">
                <div class="float-left">
                    <h2 class="text-uppercase text-blue">MUST HAVE INSURANCE</h2>
                </div>
            </div>
            <div class="slider-objects-container">
                @foreach($solutions as $key => $solution)
                @include('web.components.solution-object', ['section' =>'personal', 'apply_link' => $solution->apply_link, 'link' => $solution->link, 'page'=>'home', 'image_order' => $key, 'image_exists' => 'true', 'image_source'=>$solution->image, 'image_main_title'=>$solution->title, 'image_text'=>$solution->text])
                @endforeach
            </div>
        </div>
    </div>
    <div class="banner-background py-5 text-white" style="background-image:url({{asset($banner->image)}})">
        <div class="container py-5">
            <div class="text-center pt-5">
                <h1 class="mb-2 font-bold">{{$banner->title}}</h1>
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <h4 class="font-medium">{{$banner->text}}</h4>
                    </div>
                </div>
            </div>
            <div class="row text-center justify-content-center">
                <div class="col-lg-3">
                    <a href="{{route($banner->button_link, $locale)}}">
                        <button class="dark-blue-button hvr-grow font-medium my-3 w-100">{{$banner->button_text}}</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="py-5 px-5 background-lightgrey">
        <div class="container custom-container">
            <div class="py-5 pb-3">
                <h1 class="mb-2 font-medium text-blue">FINANCIAL HIGHLIGHTS</h1>
                <h5 class="font-book text-custom-color">Key Consolidated Financial Indicators for 2020</h5>
            </div>
            <div class="row pb-5">
                @foreach($financial_highlights as $key => $financial_highlight)
                <div class="col-lg-auto px-3 py-3 py-lg-0 hvr-grow">
                    <div class="background-white text-center p-5 dashed-border">
                        <img class="image-50 height-50" src="{{ asset($financial_highlight->image) }}" alt="{{$financial_highlight->title}}">
                        <h4 class="pt-4 pb-3 text-aqua-blue">{{$financial_highlight->price}}</h4>
                        <p class="text-darkgrey font-16">{{$financial_highlight->title}}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="p-5 background-lightgrey">
        <div class="container custom-container">
            <div class="row w-100 h-100">
                @foreach($home_services as $key => $home_service)
                <div class="col-lg-3 my-2 my-lg-0">
                    <div>
                        <img src="{{ asset($home_service->image) }}" alt="{{$home_service->title}}">
                    </div>

                    <div class="background-white p-4 min-height-300">
                        <h5 class="py-3 text-blue font-bold">{{$home_service->title}}</h5>
                        <div class="">{!!$home_service->text!!}</div>
                        <div class="py-2 to-bottom">
                            @php
                            $link = '';
                            if($home_service->slug == 'doozy' || $home_service->slug == 'allo-hayete')
                            $link = $home_service->link;
                            elseif($home_service->slug == 'careers')
                            $link = route($home_service->link, $locale);
                            elseif($home_service->slug == 'takaful')
                            $link = '';
                            @endphp
                            <a href="{{$link}}" _target="blank" class="d-inline-flex hvr-grow">
                                <p class="text-blue">{{$home_service->button_text}}</p>
                                <img class="ml-2 header-icon-small-size" src="{{ asset('assets_web/images/right-arrow-grey.svg') }}" alt="apply">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="banner-background py-5 text-white" style="background-image:url({{asset('assets_web/images/home-banner-2.png')}})">
        <div class="container custom-container py-5">
            <div class="row justify-content-center">
                @foreach($home_links as $key => $home_link)
                <div class="col-lg-2 my-2 my-lg-0 background-white mx-2 d-table">
                    <p class="font-bold text-blue option-box-title py-5 px-3">{{$home_link->title}}</p>
                    <a href="{{ $home_link->link != null ? route($home_link->link, $locale): ''}} ">
                        <div class="float-right arrow-circle-bottom-right">
                            <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-right.svg') }}" alt="go">
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection