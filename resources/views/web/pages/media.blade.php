@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="media-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="shadow-inner-lightgrey">
        <div class="container custom-container px-5 pt-5">
            <div class="d-inline-block w-100">
                @foreach($page_sections as $key => $page_section)
                <div image_src={{asset($page_section->image)}} slug={{$page_section->slug}} class="float-left option-title">
                    <h5 id="{{$page_section->slug}}Option" class="options text-uppercase cursor-pointer @if($key == 0) active @endif">{{$page_section->header_title}}</h5>
                </div>
                @endforeach
            </div>
            <div class="blue-border-bottom"></div>
            @if(isset($page_sections[0]))
            @php $page_details = $page_sections[0]; @endphp
            <div id="{{$page_sections[0]->slug}}" class="show-hide-div">
                <div class="row py-5 w-100 h-100">
                    @foreach($news as $key => $new_item)
                    <div class="col-lg-4 my-3">
                        <div class="picture-wrapper padding-top-100">
                            <img class="contain" src="{{ asset($new_item->image) }}" alt="{{$new_item->title}}">
                            <div class="media-overlay p-5 to-bottom-0 text-white">
                                <small>{{$new_item->date}}</small>
                                <h5 class="py-2 font-bold">{{$new_item->title}}</h5>
                                <p>{!!$new_item->text_preview!!}</p>
                                <a href="{{route('media-item-details', [$locale, $new_item->slug])}}?page_section={{$page_sections[0]->header_title}}">
                                    <div class="float-right arrow-circle-bottom-right-solutions">
                                        <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-right.svg') }}" alt="go">
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                    @endforeach
                </div>
            </div>
            @endif

            @if(isset($page_sections[1]))
            @php $page_details = $page_sections[1]; @endphp
            <div id="{{$page_sections[1]->slug}}" class="show-hide-div d-none">
                <div class="row py-5 w-100 h-100">
                    @foreach($events as $key => $event)
                    <div class="col-lg-4 my-3">
                        <div class="picture-wrapper padding-top-100">
                            <img class="contain" src="{{ asset($event->image) }}" alt="{{$event->title}}">
                            <div class="media-overlay p-5 to-bottom-0 text-white">
                                <small>{{$event->date}}</small>
                                <h5 class="py-2 font-bold">{{$event->title}}</h5>
                                <p>{!!$event->text_preview!!}</p>
                                <a href="{{route('media-item-details', [$locale, $event->slug])}}?page_section={{$page_sections[1]->header_title}}">
                                    <div class="float-right arrow-circle-bottom-right-solutions">
                                        <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-right.svg') }}" alt="go">
                                    </div>
                                </a>
                            </div>
                        </div>


                    </div>
                    @endforeach
                </div>
            </div>
            @endif

            @if(isset($page_sections[2]))
            @php $page_details = $page_sections[2]; @endphp
            <div id="{{$page_sections[2]->slug}}" class="show-hide-div pb-5 d-none">
                @foreach($press_headers as $key =>$press_header)
                <div class="border-bottom-blue ">
                    <div class="row py-4 justify-content-between" data-toggle="collapse" data-target="#press-{{$key}}">
                        <div class="col-lg-11">
                            <h2 class="text-uppercase text-blue">{{$press_header->title}}</h2>
                        </div>
                        <div class="col-lg-1 text-center">
                            <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-up.svg') }}" alt="go">
                        </div>
                    </div>
                    <div class="row pb-5 collapse @if($key == 0) in show @endif" id="press-{{$key}}">
                        @php
                        $correspondant_press_items = $press_items->filter(function ($value, $key) use($press_header) {
                        return $value['press_header_id'] == $press_header->id;
                        });
                        @endphp
                        @foreach($correspondant_press_items as $key => $press_item)
                        <div class="col-lg-12 my-2">
                            <div class="background-white shadow-outer-lightgrey px-5 py-3 w-100">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <h5 class="text-blue">{{$press_item->title}}</h5>
                                        <p>{{$press_item->date}}</p>
                                    </div>
                                    <div class="col-lg-1 vertically-centered">
                                        <a href="{{ asset($press_item->attachment) }}" download>
                                            <div class="d-inline-flex">
                                                <p class="text-blue">DOWNLOAD</p>
                                                <img class="icon-xsmall ml-3" src="{{ asset('assets_web/images/right-arrow-grey.svg') }}" alt="pdf">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
            @endif

            @if(isset($page_sections[3]))
            @php $page_details = $page_sections[3]; @endphp
            <div id="{{$page_sections[3]->slug}}" class="show-hide-div d-none">
                <div class="row py-5 w-100 h-100">
                    @foreach($blogs as $key => $blog)
                    <div class="col-lg-4 my-3">
                        <div class="picture-wrapper padding-top-100">
                            <img class="contain" src="{{ asset($blog->image) }}" alt="{{$blog->title}}">
                            <div class="media-overlay p-5 to-bottom-0 text-white">
                                <small>{{$blog->date}}</small>
                                <h5 class="py-2 font-bold">{{$blog->title}}</h5>
                                <p>{!!$blog->text_preview!!}</p>
                                <a href="{{route('media-item-details', [$locale, $blog->slug])}}?page_section={{$page_sections[3]->header_title}}">
                                    <div class="float-right arrow-circle-bottom-right-solutions">
                                        <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-right.svg') }}" alt="go">
                                    </div>
                                </a>
                            </div>
                        </div>


                    </div>
                    @endforeach
                </div>
            </div>
            @endif

            @if(isset($page_sections[4]))
            @php $page_details = $page_sections[4]; @endphp
            <div id="{{$page_sections[4]->slug}}" class="show-hide-div pb-5 d-none">
                @foreach($publication_headers as $key =>$publication_header)
                <div class="border-bottom-blue ">
                    <div class="row py-4 justify-content-between" data-toggle="collapse" data-target="#publication-{{$key}}">
                        <div class="col-lg-11">
                            <h2 class="text-uppercase text-blue">{{$publication_header->title}}</h2>
                        </div>
                        <div class="col-lg-1 text-center">
                            <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-up.svg') }}" alt="go">
                        </div>
                    </div>
                    <div class="row pb-5 collapse @if($key == 0) in show @endif" id="publication-{{$key}}">
                        @php
                        $correspondant_publication_items = $publication_items->filter(function ($value, $key) use($publication_header) {
                        return $value['publication_header_id'] == $publication_header->id;
                        });
                        @endphp
                        @foreach($correspondant_publication_items as $key => $publication_item)
                        <div class="col-lg-12 my-2">
                            <div class="background-white shadow-outer-lightgrey px-5 py-3 w-100">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <h5 class="text-blue">{{$publication_item->title}}</h5>
                                        <p>{{$publication_item->date}}</p>
                                    </div>
                                    <div class="col-lg-1 vertically-centered">
                                        <a href="{{ asset($publication_item->attachment) }}" download>
                                            <div class="d-inline-flex">
                                                <p class="text-blue">DOWNLOAD</p>
                                                <img class="icon-xsmall ml-3" src="{{ asset('assets_web/images/right-arrow-grey.svg') }}" alt="pdf">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
    $('.option-title').click(function() {
        var slug = $(this).attr("slug");
        var image_src = $(this).attr("image_src");

        $(".show-hide-div").addClass("d-none");
        $("#" + slug).removeClass("d-none");

        $(".options").removeClass("active");
        $(this).find("h5").addClass("active");

        $("#media-banner").css('background-image', "url(" + image_src + ")");
    });

    /* collapsing items for press */
    $('#press .collapse').on('show.bs.collapse', function() {
        $(this).prev().find("img").attr('src', "{{ asset('assets_web/images/circle-arrow-up.svg') }}");
    });
    $('#press .collapse').on('hide.bs.collapse', function() {
        $(this).prev().find("img").attr('src', "{{ asset('assets_web/images/circle-arrow-down.svg') }}");
    });
    /*-------------------------------------------------------------------*/
</script>
@endpush