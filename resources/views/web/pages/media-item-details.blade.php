@extends('web.layouts.main')
@section('content')
<div>
    @include('web.layouts.headers.header')

    @php
    $images = $news_images;
    $data = $news_item;

    if(str_contains(strtolower($page_section), 'news')){
    $images = $news_images;
    $data = $news_item;
    }
    elseif(str_contains(strtolower($page_section), 'blog')){
    $images = $blogs_images;
    $data = $blogs_item;
    }
    elseif(str_contains(strtolower($page_section), 'event')){
    $images = $events_images;
    $data = $events_item;
    }
    @endphp

    <div class="container custom-container padding-top p-5 mt-5">
        <div class="px-5 pb-5">
            <a class="d-inline-flex" href="javascript:history.back()">
                <div class="float-left mr-2">
                    <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-left.svg') }}" alt="go">
                </div>
                <p class="text-blue"> Media Room / {{ucwords(strtolower($page_section))}} </p>
            </a>
        </div>
        <div class="mb-5 px-5">
            @include('web.components.item-details-object', ['page' => 'media', 'slug' => $data->slug, 'main_image'=> $data->image, 'images' => $images, 'image_main_title'=> $data->title, 'image_subtitle'=> $data->date, 'image_text'=> $data->text])
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
    $(".header-row").addClass("bottom-shadow");
</script>
@endpush