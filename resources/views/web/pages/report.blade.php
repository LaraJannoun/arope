@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="report-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('report.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue">{{$page->title}}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="row w-100 h-100 pb-5">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-lg-4">
                        @include('web.components.dropdown', ['type'=>'values', 'placeholder' => 'Type of Problem', 'form'=>'report-form', 'class'=>'mt-2 mb-3', 'data'=>$types])
                        @if($errors->has('value'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('value') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="first_name" placeholder="First Name" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="last_name" placeholder="Last Name" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number" type="tel" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="email" placeholder="Email Address" type="email" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="title" placeholder="Title" type="text" value="{{old('title')}}" />
                        @if($errors->has('title'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <textarea class="input-text my-2" id="message" name="message" rows="6" cols="10" placeholder="Your Message"></textarea> @if($errors->has('message'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('message') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row pb-5 w-100 justify-content-center">
                    <div class=" col-lg-2">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Report Now</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
