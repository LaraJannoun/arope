@extends('web.layouts.main')


@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('education-quote.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue text-uppercase">{{ $page->title }}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="row w-100 h-100">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Applicant Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="first_name" placeholder="First Name*" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif


                        <input class="input-text my-2" name="landline_number" placeholder="Landline Number" type="text" value="{{old('landline_number')}}" />
                        @if($errors->has('landline_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('landline_number') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="occupation" placeholder="Occupation*" type="text" value="{{old('occupation')}}" />
                        @if($errors->has('occupation'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('occupation') }}</strong>
                        </span>
                        @endif

                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="father_name" placeholder="Father's Name*" type="text" value="{{old('father_name')}}" />
                        @if($errors->has('father_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('father_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif



                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="last_name" placeholder="Last Name*" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="email" placeholder="Email Address*" type="text" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row w-100 h-100 mt-5">

                    <div class="col-12">
                        <h5 class="text-blue text-uppercase">Plan Details</h5>
                    </div>
                    <div class="col-lg-4">
                        {{-- <input class="input-text my-2" name="date_of_birth" placeholder="Applicant Date Of Birth*"--}}
                        {{-- type="date" value="{{old('date_of_birth') }}"/>--}}

                        <input name="d_o_b" placeholder="Applicant Date Of Birth*" class="input-text my-2 datetimepicker" id="datepicker" type="text" value="{{old('d_o_b') }}" data-options='{"disableMobile":true}' />
                        @if($errors->has('d_o_b'))
                        <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                            <strong>The Applicant Date of Birth is required.</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="contribution_amount" placeholder="Contribution Amount*" type="text" value="{{old('contribution_amount') }}" />

                        @if($errors->has('contribution_amount'))
                        <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                            <strong>{{ $errors->first('contribution_amount') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" min="0" max="10" step="1" name="children_number" placeholder="Number Of Children*" type="number" id="children_number" value="{{old('children_number')}}" />
                        @if($errors->has('children_number'))
                        <span onsubmit="event.preventDefault()" class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The Number of Children is required.</strong>
                        </span>
                        @endif
                        <small class="">*maximum allowed 10 records</small>

                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Contribution Frequency*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input @if(old('contribution_frequency')=="Annual" ) checked @endif type="radio" value='Annual' id="Annual" name="contribution_frequency">
                                    <label class="ml-2" for="Annual">Annual</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('contribution_frequency')=="Semi annual" ) checked @endif type="radio" value='Semi annual' id="semi-annual" name="contribution_frequency">
                                    <label class="ml-2" for="semi-annual">Semi annual</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('contribution_frequency')=="Monthly" ) checked @endif type="radio" value='Monthly' id="Monthly" name="contribution_frequency">
                                    <label class="ml-2" for="Monthly">Monthly</label>
                                </div>
                                <div class="text-custom-color d-inline-flex">
                                    <input @if(old('contribution_frequency')=="Quarterly" ) checked @endif type="radio" value='Quarterly' id="Quarterly" name="contribution_frequency">
                                    <label class="ml-2" for="Quarterly">Quarterly</label>
                                </div>
                            </div>
                        </div>
                        <small>*Additional covers are available upon request</small>
                        @if($errors->has('contribution_frequency'))
                        <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                            <strong>{{ $errors->first('contribution_frequency') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-8">
                        <input class="input-text my-2" name="protection_amount" placeholder="Protection Amount (in case of death)*" type="text" value="{{old('protection_amount') }}" />
                        @if($errors->has('protection_amount'))
                        <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                            <strong>{{ $errors->first('protection_amount') }}</strong>
                        </span>
                        @endif
                        <div class="border-blue py-4 my-2" id="insured_table" @if(old('children_number')> 0) style="display: block" @else style="display: none"@endif >
                            <h5 class="px-4 text-input-color">Table of insured persons*</h5>
                            <small class="px-4">(Maxium allowed 10 records)</small>

                            <div class="px-4">
                                <div class="container" style="max-width: 700px; margin-top:1em">

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="inputFormRow">
                                                <div class="input-group mb-3">
                                                    <input type="text" name="insurer_name[]" class="form-control m-input" placeholder="Child Name" autocomplete="off">
                                                    <input type="text" name="insurer_dob[]" class="form-control m-input datetimepicker datetimepickercustom" placeholder="Date of birth" autocomplete="off">
                                                    <input type="text" name="insurer_saving_age[]" class="form-control m-input" placeholder="Education Saving Payment Age" autocomplete="off">
                                                </div>
                                            </div>


                                            <div id="newRow"></div>
                                        </div>
                                    </div>

                                    @if($errors->has('insurer_name.*'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>Please check your insurer names</strong>
                                    </span>
                                    @endif
                                    @if($errors->has('insurer_dob.*'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>Please check your insurer date of births</strong>
                                    </span>
                                    @endif
                                    @if($errors->has('insurer_saving_age.*'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>Please check your insurer ages</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row py-5 w-100 justify-content-center">
                    <div class="col-lg-12">
                        <small>(Fields marked with * are mandatory)</small>
                        <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                        @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
    $('form input').keydown(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });
    $("#children_number").change(function() {


        $("#newRow").empty();
        var value = +$(this).val();
        if (value === 0) {
            $("#insured_table").hide();
        }
        if (value > 0) {
            $("#insured_table").show();
        }

        var nr = 0;
        while (nr < value - 1) {
            $("#inputFormRow").clone().appendTo("#newRow").find('input').val('');
            nr++;
        }
    });
</script>
@endpush
