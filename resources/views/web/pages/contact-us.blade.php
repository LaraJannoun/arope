@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="contact-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('contact-us.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <h1 class="py-4 font-medium text-blue text-uppercase">{{$page->title}}</h1>
                <p class="pb-3">{!!$page->text!!}</p>
                <div class="row w-100 h-100 pb-4">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="first_name" placeholder="First Name" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="last_name" placeholder="Last Name" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="email" placeholder="Email Address" type="email" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number" type="tel" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <textarea class="input-text my-2" id="message" name="message" rows="3" cols="10" placeholder="Your Message"></textarea>
                        @if($errors->has('message'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('message') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2 datetimepicker" name="time" id="datetimepicker" type="text" value="{{old('time')}}" placeholder="When's a good time of day to call you?" data-options='{"enableTime":true,"dateFormat":"d/m/y H:i","disableMobile":true}' />
                        @if($errors->has('time'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('time') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="subject" placeholder="What would you like to discuss with Arope?" type="tel" value="{{old('subject')}}" />
                        @if($errors->has('subject'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('subject') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row pb-5 w-100 justify-content-center">
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Send message</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="background-lightgrey">
        <div class="container custom-container p-5">
            <h1 class="pb-3 font-medium text-blue text-uppercase">Locate US</h1>
            <div class="shadow-outer-lightgrey">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="contact-map-navigator">
                            <ul>
                                <li class="height-50 text-uppercase background-lightgrey text-black px-3 py-3">Head Office</li>
                                @foreach($maps as $key => $map)
                                @if($map->title == "Head Office")
                                @php $title = preg_replace('/\s+/', '_', $map->title); @endphp

                                <li lat={{$map->lat}} lng={{$map->lng}} title={{$title}} class="map-li background-white text-blue">
                                    <div class="height-50 text-uppercase curcor-pointer px-3 py-3">
                                        <p class="float-left">{{$map->main_title}}</p>
                                        <div class="float-right">
                                            <img class="icon-small" src="{{ asset('assets_web/images/white-circle-arrow-right.svg') }}" alt="go">
                                        </div>
                                    </div>
                                </li>
                                @endif
                                @endforeach
                                <li class="height-50 text-uppercase background-lightgrey text-black px-3 py-3">Branches</li>
                                @foreach($maps as $key => $map)
                                @if($map->title != "Head Office")
                                @php $title = preg_replace('/\s+/', '_', $map->title); @endphp
                                <li lat={{$map->lat}} lng={{$map->lng}} title={{$title}} class="map-li background-white text-blue">
                                    <div class="height-50 text-uppercase curcor-pointer px-3 py-3">
                                        <p class="float-left">{{$map->main_title}}</p>
                                        <div class="float-right">
                                            <img class="icon-small" src="{{ asset('assets_web/images/white-circle-arrow-right.svg') }}" alt="go">
                                        </div>
                                    </div>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9 px-0">
                        <div id="map"></div>
                        @foreach($maps as $key => $map)
                        @php $id = preg_replace('/\s+/', '_', $map->title); @endphp
                        <div id="{{$id}}" class="map-box p-4 @if($map->title != 'Head Office') d-none @endif">
                            <h5 class="text-blue font-bold">{{$map->title}}</h5>
                            <div class="py-2">
                                <p>{!!$map->text!!}</p>
                                <p>{{$map->phone}}</p>
                                <p>{{$map->fax}}
                                </p>
                                <p>{{$map->email}}</p>
                            </div>
                            <div class="pt-3">
                                <a href="{{$map->link}}" class="d-inline-flex">
                                    <p class="text-blue text-uppercase">Get direction</p>
                                    <img class="ml-3 header-icon-small-size" src="{{ asset('assets_web/images/right-arrow-grey.svg') }}" alt="apply">
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap&libraries=&v=weekly" defer></script>
<script>
    const image = "../assets_web/images/pin.svg";

    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        const location = {
            lat: 33.89265880918317,
            lng: 35.48735762865074
        };
        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 14,
            center: location,
        });

        // The marker, positioned at Uluru
        const marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: image
        });
    }
    $('.map-li').on('click', function() {
        // The location of Uluru
        const location = {
            lat: parseFloat($(this).attr('lat')),
            lng: parseFloat($(this).attr('lng')),
        };
        $(".map-box").addClass("d-none");
        var title = $(this).attr('title').replace(/ /g, "_");
        $("#" + title).removeClass("d-none");
        $(".map-li").removeClass("background-blue").removeClass("text-white").addClass("background-white").addClass("text-blue");
        $(this).removeClass("background-white").removeClass("text-blue").addClass("background-blue").addClass("text-white");
        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 14,
            center: location,
        });
        // The marker, positioned at Uluru
        const marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: image
        });
    });
</script>
@endpush