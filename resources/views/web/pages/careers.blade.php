@extends('web.layouts.main')
@section('content')
<div class="font-medium">
    @include('web.layouts.headers.header')
    <div class="modal fade" id="onlineAppSuccess" tabindex="-1" role="dialog" aria-labelledby="onlineAppSuccess" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title text-blue align-self-center" id="onlineAppSuccess">Thank you!</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <h4 class="py-3"> You have successfully completed your online application.</h4>
                    <p>By clicking the submit button, I certify that all the information provided by me on this application is true and complete, and I understand if any false information, omissions, or misrepresentations are discovered, my application may be rejected and , if I am employed, my employment may be terminated at any time.</p>
                </div>
                <div class="d-inline-flex py-4 px-5 justify-content-center">
                    <div id="submit-modal" class="text-center" data-dismiss="modal">
                        <button class="dark-blue-button hvr-grow px-5 logo-width cursor-pointer">SUBMIT</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="banner-container">
        <div id="career-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    @php
    $valid_personal_data_response = request()->session()->get('valid_personal_data');
    $valid_education_data_response = request()->session()->get('valid_educational_data');
    $valid_work_data_response = request()->session()->get('valid_work_data');
    $valid_emp_interest_data_response = request()->session()->get('valid_employment_data');
    $valid_resume_data_response = request()->session()->get('valid_resume_data');

    @endphp
    <div class="container custom-container px-5 pt-5">
        <div class="d-inline-block w-100">
            @foreach($page_sections as $key => $page_section)
            <div image_src={{asset($page_section->image)}} slug={{$page_section->slug}} class="float-left option-title cursor-pointer">
                <h5 id="{{$page_section->slug}}Option" class="options text-uppercase cursor-pointer @if($errors->count() > 0 and $key == 1) active @elseif($key == 0 and $errors->count() == 0) active  @endif ">{{$page_section->header_title}}</h5>
            </div>
            @endforeach
        </div>
        <div class="blue-border-bottom"></div>
    </div>

    @if(isset($page_sections[0]))
    @php $page_details = $page_sections[0]; @endphp
    <div id="{{$page_sections[0]->slug}}" class="show-hide-div @if($errors->count() > 0 ) d-none @endif">
        <div class="container custom-container p-5">
            <div class="row">
                <div class="col-lg-8">
                    <h2 class="text-uppercase text-blue">Launch your Career at AROPE</h2>
                    <p class="py-3 text-custom-color">We look for Dedicated, Competent, Professional, Friendly, Energized, and Service Oriented Candidates. If you think you enjoy these Competencies and your Qualifications fit any of the following Vacancies, we invite you to explore a Career with us and apply below.</p>
                </div>
            </div>
        </div>
        <div class="background-lightgrey mb-5">
            <div class="container custom-container pl-lg-5 pr-lg-0 ">
                <div class="row w-100 m-0">
                    <div class="col-lg-6 vertically-centered p-5">
                        <div class="row justify-content-between">
                            <div class="col-lg-10 pb-5">
                                <p class="font-20">{{$career_section_1->text}}</p>
                            </div>
                            <div class="col-lg-6">
                                <ul class="about-list">
                                    @php
                                    $arr_points = explode("|", $career_section_1->points);
                                    @endphp
                                    @foreach($arr_points as $key => $point)
                                    @if($key < 6) <li class="py-2"><span>{{$point}}
                                        </span></li>
                                        @endif
                                        @endforeach
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="about-list">
                                    @php
                                    $arr_points = explode("|", $career_section_1->points);
                                    @endphp
                                    @foreach($arr_points as $key => $point)
                                    @if($key >= 6) <li class="py-2"><span>{{$point}}
                                        </span></li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- to-right-0 -->
                    <div class="col-lg-6 p-0">
                        <div class="picture-wrapper padding-top-65">
                            <img src="{{ asset($career_section_1->image) }}" alt="about-quality-policy">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(isset($page_sections[1]))
    <div id="{{$page_sections[1]->slug}}" class="show-hide-div @if($errors->count() == 0 ) d-none @endif">
        <div class="container custom-container p-5">
            <div class="row w-100">
                <div class="col-lg-7">
                    <h2 class="text-uppercase text-blue pb-3">CAREERS @ AROPE</h2>
                    <p class="text-custom-color">Thank you for your interest to join AROPE Insurance family.</p>
                    <p class="text-custom-color">To apply for employment opportunities, please check the below current available vacancies.</p>
                </div>
            </div>
        </div>
        <div class="container custom-container px-5">
            <h1 class="font-medium text-blue py-5 text-uppercase">Application Process @AROPE Insurance</h1>
            <div class="row pb-5">
                @foreach($application_processes as $key => $application_process)
                @include('web.components.career-object', ['page' => 'career', 'image_source'=> $application_process->image, 'image_main_title'=> $application_process->title, 'image_text'=> $application_process->text])
                @endforeach
            </div>
            <div class="row w-100">
                <div class="col-lg-7">
                    <h2 class="text-uppercase text-blue pb-3">{{$career_top_message->title}}</h2>
                    <p class="text-custom-color">{!! $career_top_message->text !!}</p>
                </div>
            </div>
            <div class="row pt-3">
                @foreach($career_positions as $key => $career_position)
                @include('web.components.career-position-object', ['image_source'=> $career_position->image, 'image_main_title'=>$career_position->title, 'image_main_subtitle'=>$career_position->country, 'image_text'=>$career_position->first_description, 'image_text_2'=>$career_position->second_description])
                @endforeach
            </div>
            <div class="row py-5 w-100">
                <div class="col-lg-7">
                    <div class="text-custom-color pt-2">{!! $career_bottom_message->text !!}</div>
                </div>
            </div>
        </div>
        <div class="container custom-container px-5">
            <div class="row w-100">
                <div class="col-lg-7">
                    <h2 class="text-uppercase text-blue pb-3">Online Employment Application</h2>
                    <p class="text-custom-color">Please complete the form below to apply for a position with us</p>
                </div>
            </div>
            @if(session('status'))
                <div class="row">
                    <div class="col-12  pt-3">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            @endif
            @if($errors->count() > 0)
                <div class="row">
                    <div class="col-12  pt-3">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            please check for errors
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            @endif
{{--            <div class="row application-form w-100 py-4">--}}
{{--                @include('web.components.career-application-object', ['image_order' => 1, 'title' => 'personal','text'=>'Personal Information', 'onclick'=>'goToPersonal', 'z_index'=>10])--}}
{{--                @include('web.components.career-application-object', ['image_order' => 2, 'title' => 'education', 'text'=>'Education', 'onclick'=>'goToEducation', 'z_index'=>9])--}}
{{--                @include('web.components.career-application-object', ['image_order' => 3, 'title' => 'workExperince', 'text'=>'Work Experience', 'onclick'=>'goToWorkExp', 'z_index'=>8])--}}
{{--                @include('web.components.career-application-object', ['image_order' => 4, 'title' => 'employmentInterests', 'text'=>'Employment Interests', 'onclick'=>'goToEmploymentInt', 'z_index'=>7])--}}
{{--                @include('web.components.career-application-object', ['image_order' => 5, 'title' => 'resume', 'text'=>'Cover Letter & Resume', 'onclick'=>'goToResume', 'z_index'=>6])--}}
{{--            </div>--}}
            <form class="mt-3" id="contact" action="{{Route('careers.store', $locale)}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div>
                    <h3>Personal Information</h3>
                    <div id="personalForm" class="data-form min-height-auto position-relative">
                        <div class="row pb-4 w-100">
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input class="input-text required" name="first_name" placeholder="First Name*" type="text" value="{{old('first_name')}}" />
                                    @if($errors->has('first_name'))
                                        <span class="invalid-feedback text-danger d-block" role="alert">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 my-2">
                                @include('web.components.dropdown', ['type'=>'gender','class' => 'required' ,'placeholder' => 'Gender*', 'form'=>'personal-form', 'data'=>$genders, 'value' => old('gender') ])
                                @if($errors->has('gender'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-lg-4 my-2 align-flex-end">
                                <h5 class="text-blue">Home Address*</h5>
                            </div>
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input class="input-text required" name="family_name" placeholder="Family Name*" type="text" value="{{old('family_name')}}" />
                                    @if($errors->has('family_name'))
                                        <span class="invalid-feedback text-danger d-block" role="alert">
                                            <strong>{{ $errors->first('family_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>
                            <div class="col-lg-4 my-2">
                                <div class="select">
                                    <select onchange="kids()" id="civil_status" name="civil_status" class="required " >
                                        <option value="">Civil Status*</option>
                                        <option @if(old('civil_status') == 'single') selected @endif value="single">Single</option>
                                        <option @if(old('civil_status') == 'married') selected @endif value="married">Married</option>
                                        <option @if(old('civil_status') == 'engaged') selected @endif value="engaged">Engaged</option>
                                        <option @if(old('civil_status') == 'divorced') selected @endif value="divorced">Divorced</option>
                                        <option @if(old('civil_status') == 'widowed') selected @endif value="widowed">Widowed</option>
                                    </select>
                                </div>
                                @if($errors->has('civil_status'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('civil_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input type="text" class="input-text required" name="country" placeholder="Country*" value="{{ old('country') }}">
                                    {{--                                @include('web.components.dropdown', ['type'=>'country_id', 'placeholder' => 'Country', 'form'=>'personal-form', 'data'=>$countries, 'value' => old('country_id') ? old('country_id') : (session()->has('personal_data') && array_key_exists('country_id', session()->get('personal_data')) ? session()->get('personal_data')['country_id'] : '') ])--}}
                                    @if($errors->has('country'))
                                        <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 my-2">
                               <div>
                                   <input class="input-text required" name="father_name" placeholder="Father's Name*" type="text" value="{{old('father_name')}}" />
                                   @if($errors->has('father_name'))
                                       <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('father_name') }}</strong>
                                    </span>
                                   @endif
                               </div>
                            </div>

                            <div class="col-lg-4 my-2 {{old('civil_status') != "married" ? "d-none" : ''}}" id="kids_number_id">
                                @include('web.components.dropdown', ['type'=>'kids_number', 'placeholder' => 'Number of Kids', 'form'=>'personal-form', 'data'=>$kids_number, 'value' => old('kids_number') ? old('kids_number') : ''])
                                @if($errors->has('kids_number'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>{{ $errors->first('kids_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input type="text" class="input-text required" name="city" placeholder="City*" value="{{ old('city') }}">
                                    @if($errors->has('city'))
                                        <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input class="input-text required datetimepicker" name="d_o_b" placeholder="Date Of Birth*" type="date" value="{{old('d_o_b')}}" />
                                    @if($errors->has('d_o_b'))
                                        <span class="invalid-feedback text-danger d-block mt-3 " role="alert">
                                        <strong>{{ $errors->first('d_o_b') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input class="input-text required" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                                    @if($errors->has('mobile_number'))
                                        <span class="invalid-feedback text-danger d-block" role="alert">
                                            <strong>{{ $errors->first('mobile_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input type="text" class="input-text required" name="area" placeholder="Area*" value="{{ old('area') }}">
                                    @if($errors->has('area'))
                                        <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>{{ $errors->first('area') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 my-2">
                                @include('web.components.dropdown', ['type'=>'first_nationality', 'class'=>'required' ,'placeholder' => 'Nationality 1*', 'form'=>'personal-form', 'data'=>$nationalities1, 'value' => old('first_nationality')])
                                @if($errors->has('first_nationality'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                <strong>{{ $errors->first('first_nationality') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input class="input-text required" name="landline" placeholder="Landline" type="text" value="{{old('landline')  }}" />
                                    @if($errors->has('landline'))
                                        <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('landline') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input class="input-text required" name="street" placeholder="Street*" type="text" value="{{old('street')}}" />
                                    @if($errors->has('street'))
                                        <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 my-2">
                                @include('web.components.dropdown', ['type'=>'second_nationality', 'placeholder' => 'Nationality 2', 'form'=>'personal-form', 'data'=>$nationalities2, 'value' => old('second_nationality') ])
                                @if($errors->has('second_nationality'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                <strong>{{ $errors->first('second_nationality') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="col-lg-4 my-2">
                                <div>
                                    <input class="input-text required" name="email_address" placeholder="Email Address*" type="text" value="{{old('email_address')}}" />
                                    @if($errors->has('email_address'))
                                        <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('email_address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div id="space" class="col-lg-4 my-2 "></div>
                            <div class="col-lg-4 my-2">
                              <div>
                                  <input class="input-text required" name="blg_floor" placeholder="Bldg. Floor*" type="text" value="{{old('blg_floor') }}" />
                                  @if($errors->has('blg_floor'))
                                      <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('blg_floor') }}</strong>
                                    </span>
                                  @endif
                              </div>
                            </div>
                        </div>
                    </div>

                    <h3>Education</h3>

                    <div id="educationForm" class="data-form min-height-auto position-relative">
                        <div class="row py-3 w-100">
                            <div class="col-lg-4">
                                @include('web.components.dropdown', ['type'=>'university', 'class' => 'required' ,'placeholder' => 'University', 'form'=>'education-form', 'data'=>$universities, 'value' => old('university') ])
                                @if($errors->has('university'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>{{ $errors->first('university') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div  class="col-lg-4 d-none" id="extra_university">
                                <input class="input-text" name="extra_university" placeholder="University*" type="text"
                                       value="{{old('extra_university')}}"/>
                                @if($errors->has('extra_university'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('extra_university') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        <div class="row py-3 w-100">
                            <div class="col-lg-4">
                                @include('web.components.dropdown', ['type'=>'education_level', 'class' => 'required' ,'placeholder' => 'Education Level*', 'form'=>'education-form', 'data'=>$education_levels, 'value' => old('education_level') ])
                                @if($errors->has('education_level'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                <strong>{{ $errors->first('education_level') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        <div class=" row py-3 w-100">
                            <div class="col-lg-4">
                                @include('web.components.dropdown', ['type'=>'area_of_study', 'class' => 'required' ,'placeholder' => 'Area of Study*', 'form'=>'education-form', 'data'=>$area_of_studies, 'value' => old('area_of_study')])
                                @if($errors->has('area_of_study'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>{{ $errors->first('area_of_study') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row py-3 w-100">
                            <div class="col-lg-4">
                                <div class="select">
                                    <select  onchange="grad()" name="is_graduated" id="graduated" class="required">
                                        <option value="">Graduated*</option>
                                        <option @if(old('is_graduated') == 'yes') selected @endif value="yes">Yes</option>
                                        <option @if(old('is_graduated') == 'no') selected @endif  value="no">No</option>
                                    </select>
                                </div>
                                @if($errors->has('is_graduated'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                        <strong>{{ $errors->first('is_graduated') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row py-3 w-100 {{old('is_graduated') != "yes" ? "d-none" : ''}}" id="graduation_show">
                            <div class="col-lg-4">
                                @include('web.components.dropdown', ['type'=>'graduation_year','placeholder' => 'Year', 'form'=>'education-form', 'data'=>$graduated, 'value' => old('graduation_year') ])
                                @if($errors->has('graduation_year'))
                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                <strong>{{ $errors->first('graduation_year') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>
                        <div class="row pt-3 w-100">
                            <div class="col-lg-4">
                                <a onclick="university()" class="w-100 blue-button text-uppercase text-light"> Add
                                    more universities
                                    <img class="ml-2 mb-1 header-icon-small-size"
                                         src="{{ asset('assets_web/images/right-arrow-grey.svg') }}" alt="apply">
                                </a>
                            </div>
                        </div>
                    </div>

                    <h3>Work Experience</h3>
                    <div id="workExperienceForm" class="data-form min-height-auto position-relative " >

                            <div class="row pb-4 w-100">
                                <div class="col-lg-4 my-2">
                                    <p class="font-bold text-blue mb-2">Work Experience*</p>
                                    <div>
                                        @include('web.components.dropdown', ['type'=>'employment_status','class' => 'required' ,'placeholder' => 'Current Employment Status*', 'form'=>'work-experience-form', 'data'=>$employment_statuses, 'value' => old('employment_status')])
                                        @if($errors->has('employment_status'))
                                            <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                            <strong>{{ $errors->first('employment_status') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 my-2">
                                    <p class="font-bold text-blue mb-2">Current/Last Employer*</p>
                                    <div class="row m-0 w-100">
                                        <div class="col-lg-6 py-2 px-0 pr-1">
                                            <div>
                                                <input class="input-text datetimepicker required" name="work_from_date" placeholder="From dd/mm/yyyy *" type="date" value="{{old('work_from_date') ? old('work_from_date') : (session()->has('personal_data') && array_key_exists('work_from_date', session()->get('personal_data')) ? session()->get('personal_data')['work_from_date'] : '') }}" />
                                                @if($errors->has('work_from_date'))
                                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                                    <strong>{{ $errors->first('work_from_date') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 py-2 px-0 pl-1">
                                            <div>
                                                <input class="input-text datetimepicker required" name="work_to_date" placeholder="To dd/mm/yyyy *" type="date" value="{{old('work_to_date') ? old('work_to_date') : (session()->has('personal_data') && array_key_exists('work_to_date', session()->get('personal_data')) ? session()->get('personal_data')['work_to_date'] : '') }}" />
                                                @if($errors->has('work_to_date'))
                                                    <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                                    <strong>{{ $errors->first('work_to_date') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 my-2 align-flex-end">
                                    <div>
                                        <input class="input-text required" placeholder="Monthly Salary*" type="text" name="monthly_salary" value="{{ old('monthly_salary') }}">
                                        {{--                                    @include('web.components.dropdown', ['type'=>'monthly_salary_id', 'placeholder' => 'Monthly Salary', 'form'=>'work-experience-form', 'data'=>$monthly_salaries, 'value' => old('monthly_salary_id') ? old('monthly_salary_id') : (session()->has('personal_data') && array_key_exists('monthly_salary_id', session()->get('personal_data')) ? session()->get('personal_data')['monthly_salary_id'] : '')])--}}
                                        @if($errors->has('monthly_salary'))
                                            <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                            <strong>{{ $errors->first('monthly_salary') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 my-2">
                                    <div>
                                        <input class="input-text required" type="text" name="experience_year" placeholder="Total Years of experience*" value="{{ old('experience_year') }}">
                                        @if($errors->has('experience_year'))
                                            <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                            <strong>{{ $errors->first('experience_year') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 my-2">
                                    <div>
                                        <input class="input-text required" name="company_name" placeholder="Company Name*" type="text" value="{{old('company_name')}}" />
                                        @if($errors->has('company_name'))
                                            <span class="invalid-feedback text-danger d-block" role="alert">
                                            <strong>{{ $errors->first('company_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 my-2">
                                    <div>
                                        <input class="input-text required" placeholder="Benefits*" type="text" name="benefit" value="{{ old('benefit') }}">
                                        {{--                                    @include('web.components.dropdown', ['type'=>'benefit_id', 'placeholder' => 'Benefits', 'form'=>'work-experience-form', 'data'=>$benefits, 'value' => old('benefit_id') ? old('benefit_id') : (session()->has('personal_data') && array_key_exists('benefit_id', session()->get('personal_data')) ? session()->get('personal_data')['benefit_id'] : '')])--}}
                                        @if($errors->has('benefit'))
                                            <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                            <strong>{{ $errors->first('benefit') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 my-2 offset-lg-4">
                                   <div>
                                       <input class="input-text required" name="job_title" placeholder="Job Title*" type="text" value="{{old('job_title') }}" />
                                       @if($errors->has('job_title'))
                                           <span class="invalid-feedback text-danger d-block" role="alert">
                                            <strong>{{ $errors->first('job_title') }}</strong>
                                        </span>
                                       @endif
                                   </div>
                                </div>
                                <div class="col-lg-4">
                                    <div>
                                        <input class="input-text required" placeholder="Reason of Leaving*" type="text" name="reason_of_leaving" value="{{ old('reason_of_leaving') }}">
                                        {{--                                    @include('web.components.dropdown', ['type'=>'reason_of_leaving_id', 'placeholder' => 'Reason of Leaving', 'form'=>'work-experience-form', 'data'=>$reason_of_leaving, 'value' => old('reason_of_leaving_id') ? old('reason_of_leaving_id') : (session()->has('personal_data') && array_key_exists('reason_of_leaving_id', session()->get('personal_data')) ? session()->get('personal_data')['reason_of_leaving_id'] : '')])--}}
                                        @if($errors->has('reason_of_leaving'))
                                            <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                            <strong>{{ $errors->first('reason_of_leaving') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 my-2 offset-lg-4">
                                   <div>
                                        <textarea class="input-text required" id="job_description" name="job_description" rows="4" cols="10" placeholder="Brief Job Description*">{{ old('job description') }}</textarea>
                                       @if($errors->has('job_description'))
                                           <span class="invalid-feedback text-danger d-block" role="alert">
                                            <strong>{{ $errors->first('job_description') }}</strong>
                                        </span>
                                       @endif
                                   </div>
                                </div>
                            </div>
                    </div>
                    <h3>Work Employment Interests</h3>
                    <div id="employmentInterestsForm" class="data-form min-height-auto position-relative" >

                            <div class="row py-3 w-100">
                                <div class="col-lg-6">
                                    <p class="font-bold text-blue mb-3">Employment Interest</p>
                                    <input class="d-none" name="employment_interests" type="text" />

                                    <div class="">
                                        <div class="col-lg-12">
                                            <div class="select">
                                                <select name="employment_interest" id="" class="required">
                                                    @foreach($employment_interests as $key => $employment_interest)
                                                        @php $value = strtolower(str_replace(" ", "", $employment_interest->title)); @endphp
                                                        <option id="{{$value}}"
                                                                value='{{$employment_interest->title}}'>{{$employment_interest->display_title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    @if($errors->has('employment_interest'))
                                        <span class="invalid-feedback text-danger d-block" role="alert">
                                            <strong>{{ $errors->first('employment_interests') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-lg-5">
                                    <p class="font-bold text-blue">Medical Status:</p>
                                    <div class="row py-2 w-100">
                                        <div class="col-lg-12 my-2">
                                            <div class="input-text d-flex">
                                                <p>Do you suffer from any illness?</p>
                                                <div class="">
                                                    <div class="form-check form-check-inline font-16">
                                                        <input @if(old('illness') == "yes") checked @endif  onchange="medical_status()" class="ml-2 required" type="radio" value='yes' id="illness" name="illness" >
                                                        <label class="ml-2 m-0" for="yes">Yes</label>

                                                        <input @if(old('illness') == "no") checked @endif  onchange="medical_status()" class="ml-2 required" type="radio" value='no' id="illness" name="illness">
                                                        <label class="ml-2 m-0" for="no">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($errors->has('illness'))
                                                <span class="invalid-feedback text-danger d-block" role="alert">
                                                    <strong>{{ $errors->first('illness') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-12 my-2 {{old('illness') != "yes" ? "d-none" : ''}}" id="medical_info">
                                            <div>
                                                <textarea  placeholder="More Info" name="medical_info" type="text" class="input-text" rows="3" cols="10">
                                                {{ old('medical_info') }}
                                                </textarea>
                                                @if($errors->has('medical_info'))
                                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                                    <strong>{{ $errors->first('medical_info') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12 my-2">
                                           <div>
                                               <input class="input-text datetimepicker required" name="start_date" placeholder="Available Start Date" type="date" value="{{old('start_date') }}" />
                                               @if($errors->has('start_date'))
                                                   <span class="invalid-feedback text-danger d-block" role="alert">
                                                    <strong>{{ $errors->first('start_date') }}</strong>
                                                </span>
                                               @endif
                                           </div>
                                        </div>
                                        <div class="col-lg-12 my-2">
                                            @include('web.components.dropdown', ['type'=>'referer', 'class' => 'required' ,'data'=>$referers, 'placeholder' => 'How were you referred to', 'form'=>'employment-int-form', 'value' => old('referer') ])
                                            @if($errors->has('referer'))
                                                <span class="invalid-feedback text-danger d-block mt-3" role="alert">
                                                    <strong>{{ $errors->first('referer') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-12 my-2">
                                            <p class="font-bold text-blue">Are any members of your family employed at AROPE or eBLOM?</p>
                                        </div>
                                        <div class="col-lg-12 my-2">
                                            <div class="input-text py-1">
                                                <div class="row">
                                                    <div class="col-lg-6 font-16 form-inline">
                                                        <input @if(old('family_employed') == "yes") checked @endif  class="ml-2 required" type="radio" value='yes' id="yes" name="family_employed">
                                                        <label  class="ml-2 m-0" for="yes">Yes</label>

                                                        <input @if(old('illness') == "no") checked @endif  class="ml-2 required" type="radio" value='no' id="no" name="family_employed">
                                                        <label class="ml-2 m-0" for="no">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($errors->has('family_employed'))
                                                <span class="invalid-feedback text-danger d-block" role="alert">
                                                    <strong>{{ $errors->first('family_employed') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-lg-12 my-2">
                                            <div>
                                                <input class="input-text required" name="job_details" placeholder="Please give details, name, position, institution(AROPE/BLOM)" type="text" value="{{old('job_details') ? old('job_details') : (session()->has('personal_data') && array_key_exists('job_details', session()->get('personal_data')) ? session()->get('personal_data')['job_details'] : '')  }}" />
                                                @if($errors->has('job_details'))
                                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                                    <strong>{{ $errors->first('job_details') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                    </div>
                    <h3>Cover Letter & Resume</h3>
                    <div id="resumeForm" class="data-form min-height-auto position-relative">

                        <div class="row py-3 w-100">
                            <div class="col-lg-4 my-2">
                                <p class="font-bold text-blue">Cover Letter & Resume</p>
                            </div>
                            <div class="col-lg-8 my-2">
                                <p class="font-bold text-blue">Data Privacy Statement</p>
                            </div>
                            <div class="col-lg-4 my-2">
                                <div class="input-text d-inline-flex">
                                    <input class="file-upload w-100 required" name="cover"
                                           placeholder="Upload Cover Letter (pdf or word file)" type="file"
                                           value="{{old('cover')}}"/>
                                    <img class="float-right header-icon-size"
                                         src="{{asset('assets_web/images/attach.svg')}}">
                                </div>
                                @if($errors->has('cover'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('cover') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="col-lg-7 my-2">
                                <p class="text-custom-color font-book">
                                    We endeavor to ensure that the data submitted in your application remains
                                    confidential and is used only for the purposes stated in the data privacy statement.
                                </p>
                            </div>
                            <div class="col-lg-4 my-2">
                                <div class="input-text d-inline-flex">
                                    <input class="file-upload w-100 required file-resume-upload" type="file" name="resume"
                                           id="resume" value="{{ old('resume') }}"/>
                                    <img class="float-right header-icon-size"
                                         src="{{asset('assets_web/images/attach.svg')}}">
                                </div>
                                @if($errors->has('resume'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('resume') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="col-lg-8 my-2">
                                <div class="text-custom-color form-check form-check-inline">
                                    <input @if(old('agreement') == "true") checked @endif  type="radio" value='true' id="agreement" name="agreement" class="required">
                                    <label class="ml-2 m-0 text-custom-color" for="agreement">Yes I have read and agree with
                                        the data privacy statement and I accept it.</label>
                                </div>
                                @if($errors->has('agreement'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('agreement') }}</strong>
                                </span>
                                @endif
                            </div>

                        </div>

                    </div>

                </div>
            </form>
        </div>
    </div>
    @endif
</div>
@endsection
@push('script')
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
    <script>


        ;(function($){

            /**
             * Store scroll position for and set it after reload
             *
             * @return {boolean} [loacalStorage is available]
             */
            $.fn.scrollPosReaload = function(){
                if (localStorage) {
                    var posReader = localStorage["posStorage"];
                    if (posReader) {
                        $(window).scrollTop(posReader);
                        localStorage.removeItem("posStorage");
                    }
                    $(this).click(function(e) {
                        localStorage["posStorage"] = $(window).scrollTop();
                    });

                    return true;
                }

                return false;
            }

            /* ================================================== */

            $(document).ready(function() {
                // Feel free to set it for any element who trigger the reload
                $('form').scrollPosReaload();
            });

        }(jQuery));

        $('.option-title').click(function() {
            var slug = $(this).attr("slug");
            var image_src = $(this).attr("image_src");

            $(".show-hide-div").addClass("d-none");
            $("#" + slug).removeClass("d-none");

            $(".options").removeClass("active");
            $(this).find("h5").addClass("active");

            $("#career-banner").css('background-image', "url(" + image_src + ")");
        });
       function university(){
           $("#extra_university").removeClass("d-none");
       }
        function medical_status() {
           console.log("hey");
            var selectedValue3 = document.getElementById("illness").value;
            if (selectedValue3 === "yes"){
                $("#medical_info").removeClass("d-none");
            }
        }
        function kids(){
            var selectBox = document.getElementById("civil_status");
            var selectedValue = selectBox.options[selectBox.selectedIndex].value;
            if (selectedValue === "married"){
                $('#space').remove();
                $('#kids_number_id').removeClass('d-none');
            }
        }
        function grad(){
            console.log("hey")
            var selectBox2 = document.getElementById("graduated");
            var selectedValue2 = selectBox2.options[selectBox2.selectedIndex].value;
            if (selectedValue2 === "yes"){
                $('#graduation_show').removeClass('d-none');
            }
        }
        var form = $("#contact");
        if (form) {
            form.validate({
                errorPlacement: function errorPlacement(error, element) {

                    element.parent().after(error);
                }
            });
            form.children("div").steps({
                headerTag: "h3",
                bodyTag: "div",
                transitionEffect: "slideLeft",
                onStepChanging: function(event, currentIndex, newIndex) {
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinishing: function(event, currentIndex) {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function(event, currentIndex) {
                    document.getElementById('contact').submit();
                }
            });
        }
    </script>
@endpush

