@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="report-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div id="faqs" class="py-5 px-5">
        <div class="container custom-container">
            @foreach($faqs_headers as $faq_header)
            <h2 class="py-3 text-uppercase font-medium text-blue">{{$faq_header->title}}</h2>
            <div class="row pb-5">
                @php
                $correspondant_faqs = $faqs->filter(function ($value, $key) use($faq_header) {
                return $value['faqs_header_id'] == $faq_header->id;
                });
                @endphp
                @foreach($correspondant_faqs as $key => $faq)
                <div class="col-lg-12 my-2">
                    <div class="background-white shadow-outer-lightgrey px-5 pt-4 pb-1 w-100">
                        <div class="row pt-1">
                            <div class="col-lg-11 pb-4">
                                <h5 class="text-blue font-medium" data-toggle="collapse" data-target="#faq-{{$key}}">{{$faq->question}}</h5>
                                <div id="faq-{{$key}}" class="collapse">
                                    <p class="pt-3">{{$faq->answer}}</p>
                                </div>
                            </div>
                            <div class="col-lg-1 text-right" data-toggle="collapse" data-target="#faq-{{$key}}">
                                <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-down.svg') }}" alt="go">
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    /* collapsing items for press */
    $('#faqs .collapse').on('show.bs.collapse', function() {
        $(this).parent().next().find("img").attr('src', "{{ asset('assets_web/images/circle-arrow-up.svg') }}");
    });
    $('#faqs .collapse').on('hide.bs.collapse', function() {
        $(this).parent().next().find("img").attr('src', "{{ asset('assets_web/images/circle-arrow-down.svg') }}");
    });
    /*-------------------------------------------------------------------*/
</script>
@endpush