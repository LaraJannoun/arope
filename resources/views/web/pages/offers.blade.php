@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="offers-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>

    <div class="container custom-container p-5">
        <div class="py-4 w-100">
            <h2 class="text-uppercase text-blue">{{$page->title}}</h2>
        </div>
        <div class="row w-100 h-100">
            @foreach($offers as $key => $offer)
            @if($offer->width == '1/2')
            <div class="col-lg-6 px-2 py-2">
                <div class="picture-wrapper padding-top-62">
                    <img class="cover" src="{{ asset($offer->image) }}" alt="offer">
                </div>
            </div>
            @elseif($offer->width == '1')
            <div class="col-lg-12 px-2 py-2">
                <div class="picture-wrapper padding-top-30">
                    <img class="cover" src="{{ asset($offer->image) }}" alt="offer">
                    <div class="offer-box p-2">
                        <div class="white-border text-center py-5">
                            <h2 class="pt-2 text-uppercase text-white font-bold">ACCIDENT?</h2>
                            <h2 class="text-uppercase text-white">Get covered</h2>
                            <div class="px-4 pt-2 pb-3">
                                <button class="second-blue-button text-uppercase w-100">learn more</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="col-lg-4 px-2 py-2">
                <div class="picture-wrapper padding-top-100">
                    <img class="cover" src="{{ asset($offer->image) }}" alt="offer">
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>

@endsection