@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="container custom-container padding-top p-5 mt-5">
        <div class="px-5 pb-5">
            <a class="d-inline-flex" href="javascript:history.back()">
                <div class="float-left mr-2">
                    <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-left.svg') }}" alt="go">
                </div>
                <p class="text-blue">Solutions / {{ucwords(strtolower($page_section))}} </p>
            </a>
        </div>
        <div class="mb-5 px-5">
            @include('web.components.item-details-object', ['page' => 'solutions', 'apply_link' => $featured_item->apply_link, 'slug' => $featured_item->slug, 'main_image'=>$featured_item->image, 'images' => $featured_item_images, 'image_main_title'=>$featured_item->title, 'image_subtitle'=>$featured_item->subtitle, 'image_text'=>$featured_item->text])
        </div>
    </div>
    <div class="mt-5 banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($banner->image)}})">
        <div class="container py-5">
            <div class="text-center pt-5">
                <h1 class="mb-2 font-bold text-uppercase">{{$banner->title}}</h1>
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <h4 class="font-medium">{!!$banner->text!!}</h4>
                    </div>
                </div>
            </div>
            <div class="row text-center justify-content-center  py-4">
                <div class="col-lg-3">
                    <a href="{{route($banner->button_link, $locale)}}">
                        <button class="dark-blue-button hvr-grow font-medium">{{$banner->button_text}}</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
    $(".header-row").addClass("bottom-shadow");
</script>
@endpush
