@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="parent-container padding-top">
        <div class="container py-5">
            <ul>
                @foreach($items as $key => $item)
                <a href="{{route($item->page.'-solution-item-details', [$locale, $item->link])}}?page_section=search">
                    <li class="mb-5">
                        <h5 class="font-bold">{{$item->title_en}}</h5>
                        <p>{!!$item->text_en!!}</p>
                    </li>
                </a>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection