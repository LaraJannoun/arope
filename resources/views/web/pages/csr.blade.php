@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="about-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <div class="container custom-container">
            <div class="py-5 text-center">
                <h1 class="mb-3 font-medium text-blue">{{$page->title}}</h1>
                <p>{{$page->text}}</p>
            </div>
            <div class="row w-100 h-100 pb-5">
                @foreach($csr_items as $key => $csr_item)
                <div class="col-lg-4 my-3">
                    <div class="background-white shadow-outer-lightgrey">
                        <img src="{{ asset($csr_item->image) }}" alt="{{$csr_item->title}}" />

                        <div class="py-4 px-5 min-height-160">
                            <h5 class="pt-2 pb-4 text-blue font-bold text-uppercase">{{$csr_item->title}}</h5>
                            <div>{!!$csr_item->text!!}</div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection