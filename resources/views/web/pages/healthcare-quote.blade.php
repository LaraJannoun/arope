@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('healthcare-quote.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue text-uppercase">{{ $page->title }}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="row w-100 h-100">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-12">
                        <h5 class="text-blue text-uppercase">Applicant Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="first_name" placeholder="First Name*" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif
                        <input name="d_o_b" placeholder="Applicant Date Of Birth*" class="input-text my-2 datetimepicker" id="datepicker" type="text" value="{{old('d_o_b') }}" data-options='{"disableMobile":true, "minDate": "1.01.1958","maxDate": "12.31.2003"}' required />
                        @if($errors->has('d_o_b'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The Applicant Date of Birth is required.</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="email" placeholder="Email Address*" type="text" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>


                    <div class="col-lg-4">
                        <input class="input-text my-2" name="father_name" placeholder="Father's Name*" type="text" value="{{old('father_name')}}" />
                        @if($errors->has('father_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('father_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="landline_number" placeholder="Landline Number" type="text" value="{{old('landline_number')}}" />
                        @if($errors->has('landline_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('landline_number') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="occupation" placeholder="Occupation*" type="text" value="{{old('occupation')}}" />
                        @if($errors->has('occupation'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('occupation') }}</strong>
                        </span>
                        @endif

                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="last_name" placeholder="Last Name*" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif


                    </div>
                </div>

                <div class="row w-100 h-100 mt-3">
                    <div class="col-12">
                        <h5 class="text-blue text-uppercase">Plan Details</h5>
                        <div class="mx-2 mt-2 mb-2">
                            <p>BASIC COVER: IN - HOSPITAL</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">

                            <h5 class="pb-4 px-4 text-input-color">Hospital Network*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='Full Network - All Hospitals' id="full-network" name="hospital_network">
                                    <label class="ml-2" for="full-network">Full Network - All Hospitals</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Restricted Network - Selection of Hospitals' id="restricted-network" name="hospital_network">
                                    <label class="ml-2" for="restricted-network">Restricted Network - Selection of Hospitals</label>
                                </div>
                                @if($errors->has('hospital_class'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('hospital_class') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Hospitalization Class*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='First Class - USD$ 400,000' id="first_class" name="hospital_class">
                                    <label class="ml-2" for="first_class">First Class - USD$ 400,000</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Second Class - USD$ 350,000' id="second_class" name="hospital_class">
                                    <label class="ml-2" for="second_class">Second Class - USD$ 350,000</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Third Class - USD$ 300,000' id="third_class" name="hospital_class">
                                    <label class="ml-2" for="third_class">Third Class - USD$ 300,000</label>
                                </div>
                                @if($errors->has('hospital_class'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>The hospitalization class is required</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Cover Type*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='With NSSF' id="with-nssf" name="cover_type">
                                    <label class="ml-2" for="with-nssf">With NSSF</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Without NSSF' id="withour-nssf" name="cover_type">
                                    <label class="ml-2" for="withour-nssf">Without NSSF</label>
                                </div>
                                @if($errors->has('cover_type'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('cover_type') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row w-100 h-100 mt-3">
                    <div class="col-lg-12">
                        <div class="mb-2">
                            <h5>ADDITIONAL FREE COVER</h5>
                            <p>International Travel Assistance: Covering repatriation of insured family members to the country of residence</p>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-3 px-4 text-input-color">OPTIONAL COVERS</h5>
                            <div class="px-5">
                                <div class="row">
                                    <div class="col">
                                        <div class="text-custom-color custom-control custom-control-inline">
                                            <input onchange="checkSelection()" type="checkbox" value='Out of Hospital Covers' id="out-of-hospital" name="optional_covers[]">
                                            <label class="ml-2" for="out-of-hospital">Out of Hospital Covers</label>
                                        </div>
                                        <div class="text-custom-color custom-control custom-control-inline">
                                            @php
                                            $personal_value = "Personal accidents covering the policyholder being also Insured in case of accidental death or Permanent Total Disability / Permanent Partial Disability due to accident.";
                                            @endphp
                                            <input type="checkbox" value='{{$personal_value}}' id="personal-accident" name="optional_covers[]">
                                            <label class="ml-3" for="personal-accident">
                                                {{$personal_value}}
                                            </label>
                                        </div>
                                        @php
                                        $work_value = "Work related accidents";
                                        @endphp
                                        <div class="text-custom-color custom-control custom-control-inline">
                                            <input type="checkbox" value='{{$work_value}}' id="work-accidents" name="optional_covers[]">
                                            <label class="ml-2" for="work-accidents">{{$work_value}}</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        @php
                                        $amb_value = "Ambulatory (Laboratory & Radiology)";
                                        @endphp
                                        <div class="text-custom-color custom-control custom-control-inline">
                                            <input onclick="checkSelection()" disabled type="checkbox" value='{{$amb_value}}' id="ambulatory" name="optional_covers[]">
                                            <label class="ml-2" for="ambulatory">{{$amb_value}}</label>
                                        </div>
                                        @php
                                        $dr_value = "Dr Consultations";
                                        @endphp
                                        <div class="text-custom-color custom-control custom-control-inline">
                                            <input onclick="checkSelection()" disabled type="checkbox" value='{{$dr_value}}' id="dr-consultation" name="optional_covers[]">
                                            <label class="ml-2" for="dr-consultation">{{$dr_value}}</label>
                                        </div>
                                        @php
                                        $med_value = "Medications";
                                        @endphp
                                        <div class="text-custom-color custom-control custom-control-inline">
                                            <input disabled type="checkbox" value='{{$med_value}}' id="medication" name="optional_covers[]">
                                            <label class="ml-2" for="medication">{{$med_value}}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="border-blue py-4 my-2">
                            <h5 class="px-4 text-input-color">Table of insured persons*</h5>
                            <small class="px-4">(Maxium allowed 10 records)</small>

                            <div class="px-4">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="inputFormRow" class="form-inline">
                                                <div class="input-group mb-3">
                                                    <input type="text" name="insurer_name[]" class="form-control m-input input-text" placeholder="Enter name" autocomplete="off">
                                                    <input type="text" name="insurer_relation[]" class="form-control m-input input-text" placeholder="Enter relation" autocomplete="off">
                                                    <select class="form-control m-input input-text p-2 rounded-0" name="insurer_year[]" id="age_dropdown">
                                                        <option value="">Year of birth</option>
                                                        @for ($x = 2021; $x >=1956 ; $x--)
                                                        <option value="{{$x}}" @if(old('year_of_birth')==$x) selected @endif>
                                                            {{$x}}
                                                        </option>
                                                        @endfor
                                                    </select>
                                                    <input type="text" name="insurer_nssf[]" class="form-control m-input input-text" placeholder="Enter nssf" autocomplete="off">
                                                    <div class="input-group-append">
                                                        <button id="removeRow" type="button" class="btn btn-danger">
                                                            Remove
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="newRow"></div>
                                            <button id="addRow" type="button" class="dark-blue-button">Add Row</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('insurer_name.*'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>Please fill insurer names.</strong>
                        </span>
                        @endif
                        @if($errors->has('insurer_relation.*'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>Please fill insurer relation.</strong>
                        </span>
                        @endif
                        @if($errors->has('insurer_year.*'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                           <strong>Please fill insurer year.</strong>
                        </span>
                        @endif
                        @if($errors->has('insurer_nssf.*'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>Please fill insurer nssf.</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row py-5 w-100 justify-content-center">
                    <div class="col-lg-12">
                        <small>(Fields marked with * are mandatory)</small>
                        <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                        @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('script')
<script type="text/javascript">
    var rowCount = 0;

    $("#addRow").click(function() {
        if (rowCount > 9) { // +1 for header row.
            alert("Maximum allowed 10 insured Persons");
        } else {
            rowCount++;

            $("#inputFormRow").clone().appendTo("#newRow").find('input').val('');
        }
    });


    // remove row
    $(document).on('click', '#removeRow', function() {
        $(this).closest('#inputFormRow').remove();
        rowCount--;
    });
</script>
<script>
    function checkSelection() {
        if (document.getElementById('out-of-hospital').checked) {
            document.getElementById('ambulatory').removeAttribute('disabled');

            document.getElementById('dr-consultation').removeAttribute('disabled');

        }
        if (!document.getElementById('out-of-hospital').checked) {
            document.getElementById('ambulatory').setAttribute('disabled', 'disabled');
            document.getElementById('ambulatory').checked = false;
            document.getElementById('dr-consultation').setAttribute('disabled', 'disabled');
            document.getElementById('dr-consultation').checked = false;
            document.getElementById('medication').setAttribute('disabled', 'disabled');
            document.getElementById('medication').checked = false;

        }

        if (document.getElementById('dr-consultation').checked) {
            document.getElementById('medication').removeAttribute('disabled');
        } else if (!document.getElementById('dr-consultation').checked) {
            document.getElementById('medication').setAttribute('disabled', 'disabled');
            document.getElementById('medication').checked = false;
        }

    }
</script>
@endpush('script')
