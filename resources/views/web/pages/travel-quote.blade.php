@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('travel-quote.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue text-uppercase">{{ $page->title }}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="row w-100 h-100">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-12">
                        <h5 class="text-blue text-uppercase">Applicant Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="first_name" placeholder="First Name*" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif
                        <input name="d_o_b" placeholder="Applicant Date Of Birth*" class="input-text my-2 datetimepicker" id="datepicker" type="text"
                               value="{{old('d_o_b') }}" data-options='{"disableMobile":true}' required/>
                        @if($errors->has('d_o_b'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>The Applicant Date of Birth is required.</strong>
                            </span>
                        @endif
                        <input class="input-text my-2" name="email" placeholder="Email Address*" type="text" value="{{old('email')}}" />
                        @if($errors->has('email'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif


                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="father_name" placeholder="Father's Name*" type="text" value="{{old('father_name')}}" />
                        @if($errors->has('father_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('father_name') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="landline_number" placeholder="Landline Number" type="text" value="{{old('landline_number')}}" />
                        @if($errors->has('landline_number'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('landline_number') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="occupation" placeholder="Occupation*" type="text" value="{{old('occupation')}}" />
                        @if($errors->has('occupation'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('occupation') }}</strong>
                        </span>
                        @endif

                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="last_name" placeholder="Last Name*" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif


                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h5 class="text-blue text-uppercase">Plan Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="px-4 text-input-color">Plan Type*</h5>
                            <p class="px-4 font-10 mt-2">Gold & Platinum Plans are in conformity with Schengen visa requirements</p>
                            <div class="px-5 pt-4">
                                <div class="text-custom-color">
                                    <input type="radio" value='Platinum USD $100,000' id="first" name="plan_type" @if( old('plan_type')=="Platinum USD $100,000" ) checked @endif>
                                    <label class="ml-2" for="first">Platinum USD $100,000</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Gold USD $50,000' id="second" name="plan_type" @if( old('plan_type')=="Gold USD $50,000" ) checked @endif>
                                    <label class="ml-2" for="second">Gold USD $50,000</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Silver USD $25,000' id="third" name="plan_type" @if( old('plan_type')=="Silver USD $25,000" ) checked @endif>
                                    <label class="ml-2" for="third">Silver USD $25,000</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Bronze USD $10,000' id="fourth" name="plan_type" @if( old('plan_type')=="Bronze USD $10,000" ) checked @endif>
                                    <label class="mx-2" for="fourth">Bronze USD $10,000</label>
                                </div>
                                @if($errors->has('plan_type'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('plan_type') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <input class="input-text my-2" name="destination" placeholder="Destination*" type="text" value="{{old('destination')}}" />
                        @if($errors->has('destination'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('destination') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Trip Duration*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 5 days' id="five-days" name="trip_duration" @if( old('trip_duration')=="Up to 5 days" ) checked @endif>
                                    <label class="ml-2" for="five-days">Up to 5 days</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 7 days' id="seven-days" name="trip_duration" @if( old('trip_duration')=="Up to 7 days" ) checked @endif>
                                    <label class="ml-2" for="seven-days">Up to 7 days</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 10 days' id="ten-days" name="trip_duration" @if( old('trip_duration')=="Up to 10 days" ) checked @endif>
                                    <label class="ml-2" for="ten-days">Up to 10 days</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 20 days' id="twenty-days" name="trip_duration" @if( old('trip_duration')=="Up to 20 days" ) checked @endif>
                                    <label class="mx-2" for="twenty-days">Up to 20 days</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 31 days' id="31-days" name="trip_duration" @if( old('trip_duration')=="Up to 31 days" ) checked @endif>
                                    <label class="mx-2" for="31-days">Up to 31 days</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 61 days' id="61-days" name="trip_duration" @if( old('trip_duration')=="Up to 61 days" ) checked @endif>
                                    <label class="mx-2" for="61-days">Up to 61 days</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 90 days' id="90-years" name="trip_duration" @if( old('trip_duration')=="Up to 90 days" ) checked @endif>
                                    <label class="mx-2" for="90-years">Up to 90 days</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 6 months' id="6-months" name="trip_duration" @if( old('trip_duration')=="Up to 6 months" ) checked @endif>
                                    <label class="mx-2" for="6-months">Up to 6 months</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 1 year' id="1-year" name="trip_duration" @if( old('trip_duration')=="Up to 1 year" ) checked @endif>
                                    <label class="mx-2" for="1-year">Up to 1 year</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Up to 2 years' id="2-years" name="trip_duration" @if( old('trip_duration')=="Up to 2 years" ) checked @endif>
                                    <label class="mx-2" for="2-years">Up to 2 years</label>
                                </div>
                                @if($errors->has('trip_duration'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('trip_duration') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Geographical Zone*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='Europe' id="europe" name="geo_zone" @if( old('geo_zone')=="Europe" ) checked @endif>
                                    <label class="ml-2" for="europe">Europe</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Worldwide' id="worldwide" name="geo_zone" @if( old('geo_zone')=="Worldwide" ) checked @endif>
                                    <label class="ml-2" for="worldwide">Worldwide</label>
                                </div>
                                <div class="text-custom-color d-inline-flex">
                                    <input type="radio" value='Worldwide excluding USA and Canada' id="worldwide-usa" name="geo_zone" @if( old('geo_zone')=="Worldwide excluding USA and Canada" ) checked @endif>
                                    <label class="ml-2" for="worldwide-usa">Worldwide excluding USA and Canada</label>
                                </div>
                                @if($errors->has('geo_zone'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>The Geographical Zone is required</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h5 class="text-blue text-uppercase">Table of insured persons</h5>
                    </div>

                    <div class="col-lg-8">
                        <div class="border-blue py-4 my-2">
                            <h5 class="px-4 text-input-color">Table of insured persons</h5>
                            <small class="px-4">(Maxium allowed 5 records)</small>

                            <div class="px-1 mt-2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="inputFormRow" class="form-inline">
                                                <div class="input-group mb-3">
                                                    <input type="text" name="insurer_name[]"
                                                           class="form-control m-input input-text"
                                                           placeholder="Enter name" autocomplete="off">
                                                    <input type="text" name="insurer_relation[]"
                                                           class="form-control m-input input-text"
                                                           placeholder="Enter relation" autocomplete="off">
                                                    <select class="form-control m-input input-text p-2 rounded-0"
                                                            name="insurer_year[]" id="age_dropdown">
                                                        <option value="">year of birth</option>
                                                        @for ($x = 2021; $x >=1956 ; $x--)
                                                            <option value="{{$x}}"
                                                                    @if(old('year_of_birth')==$x) selected @endif>
                                                                {{$x}}
                                                            </option>
                                                        @endfor
                                                    </select>
                                                    <div class="input-group-append">
                                                        <button id="removeRow" type="button" class="btn btn-danger">
                                                            Remove
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="newRow"></div>
                                            <button id="addRow" type="button" class="dark-blue-button">Add Row</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row py-5 w-100 justify-content-center">
                    <div class="col-lg-12">
                        <small>(Fields marked with * are mandatory)</small>
                        <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                        @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('script')

<script type="text/javascript">
    var rowCount = 0;

    $("#addRow").click(function () {
        if (rowCount >= 4) { // +1 for header row.
            alert("Maximum allowed 5 insured Persons");
        }else {
            rowCount++;

            $( "#inputFormRow" ).clone().appendTo( "#newRow" ).find('input').val('');

        }
    });


    // remove row
    $(document).on('click', '#removeRow', function () {
        $(this).closest('#inputFormRow').remove();
        rowCount--;
    });
</script>
@endpush('script')
