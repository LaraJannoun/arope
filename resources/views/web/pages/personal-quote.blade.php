@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('personal-quote.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue text-uppercase">{{ $page->title }}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="row w-100 h-100">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-12">
                        <h5 class="text-blue text-uppercase">Applicant Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="first_name" placeholder="First Name*" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif
                        <input name="d_o_b" placeholder="DOB*" class="input-text my-2 datetimepicker" id="datepicker" type="text" value="{{old('d_o_b') }}" data-options='{"disableMobile":true, "minDate": "1.01.1958","maxDate": "12.31.2003"}' required />
                        <small>Applicant age should be between 18 & 64 Years</small>
                        @if($errors->has('d_o_b'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The Applicant Date of Birth is required.</strong>
                        </span>
                        @endif



                        <input class="input-text my-2" name="email" placeholder="Email Address*" type="text" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="father_name" placeholder="Father's Name*" type="text" value="{{old('father_name')}}" />
                        @if($errors->has('father_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('father_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="landline_number" placeholder="Landline Number" type="text" value="{{old('landline_number')}}" />
                        @if($errors->has('landline_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('landline_number') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="occupation" placeholder="Occupation*" type="text" value="{{old('occupation')}}" />
                        @if($errors->has('occupation'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('occupation') }}</strong>
                        </span>
                        @endif

                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="last_name" placeholder="Last Name*" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif

                    </div>
                </div>

                <div class="row w-100 h-100">
                    <div class="col-12">
                        <h5 class="text-blue text-uppercase mt-4">Plan Details</h5>
                    </div>

                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <label for="" class="mr-2 ml-3 text-input-color px-4">Currency</label>
                            <div class="custom-control custom-control-inline">
                                <input @if(old('currency')=="usd" ) checked @endif type="radio" id="usd" value="usd" name="currency" class="custom-control-input">
                                <label class="custom-control-label text-input-color" for="usd">USD</label>
                            </div>
                            <div class="custom-control  custom-control-inline">
                                <input @if(old('currency')=="lbp" ) checked @endif type="radio" value="lbp" id="lbp" name="currency" class="custom-control-input">
                                <label class="custom-control-label text-input-color" for="lbp">LBP</label>
                            </div>

                        </div>
                        @if($errors->has('currency'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The currency is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Sum Insured*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='$25,000 USD' id="first" name="sum_insured" @if( old('sum_insured')=="$25,000 USD" ) checked @endif>
                                    <label class=" ml-2" for="first">$25,000 USD</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='$50,000 USD' id="second" name="sum_insured" @if( old('sum_insured')=="$50,000 USD" ) checked @endif>
                                    <label class="ml-2" for="second">$50,000 USD</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='$100,000 USD' id="third" name="sum_insured" @if( old('sum_insured')=="$100,000 USD" ) checked @endif>
                                    <label class="ml-2" for="third">$100,000 USD</label>
                                </div>
                                <div class="text-custom-color d-inline-flex">
                                    <input type="radio" value='other' id="other" name="sum_insured" @if( old('sum_insured')=="other" ) checked @endif>
                                    <label class="mx-2" for="other">Other</label>
                                    <input class="input-text py-0" type="text" id="other_sum" name="other_sum" value="{{old('other_sum')}}">
                                </div>
                                @if($errors->has('sum_insured'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('sum_insured') }}</strong>
                                </span>
                                @endif
                                @if($errors->has('other_sum'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>The sum insured is required.</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Cover*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input checked type="radio" value='Accidental Death' id="acc-death" name="cover" @if( old('cover')=="Accidental Death" ) checked @endif>
                                    <label class="ml-2" for="acc-death">Accidental Death</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Accidental Disability' id="acc-disability" name="cover" @if( old('cover')=="Accidental Disability" ) checked @endif>
                                    <label class="ml-2" for="acc-disability">Accidental Disability</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Passive War' id="passive-war" name="cover" @if( old('cover')=="Passive War" ) checked @endif>
                                    <label class="ml-2" for="passive-war">Passive War Risk</label>
                                </div>
                                <small class="text-input-color">*Additional covers are available upon request.</small>

                                @if($errors->has('cover'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('cover') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row py-5 w-100 justify-content-center">
                    <div class="col-lg-12">
                        <small>(Fields marked with * are mandatory)</small>
                        <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                        @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
