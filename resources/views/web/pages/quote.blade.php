@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('quote.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue">{{$page->title}}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="row w-100 h-100 pb-5">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="company_name" placeholder="Professional/Company Name*" type="text" value="{{old('company_name')}}" />
                        @if($errors->has('company_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('company_name') }}</strong>
                        </span>
                        @endif


                        <input class="input-text my-2" name="landline_number" placeholder="Land Line Number*" type="text" value="{{old('landline_number')}}" />
                        @if($errors->has('landline_number'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('landline_number') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="business_type" placeholder="Business Type*" type="text" value="{{old('business_type')}}" />
                        @if($errors->has('business_type'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('business_type') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="email" placeholder="Email Address*" type="text" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="employees_nb" placeholder="# of Employees*" type="text" value="{{old('employees_nb')}}" />
                        @if($errors->has('employees_nb'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong> Employees number is required</strong>
                        </span>
                        @endif

                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="contact_person" placeholder="Contact Person" type="text" value="{{old('contact_person')}}" />
                        @if($errors->has('contact_person'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('contact_person') }}</strong>
                        </span>
                        @endif

                    </div>
                </div>
                <div class="row py-5 w-100 justify-content-center">
                    <div class="col-lg-12">
                        <small>(Fields marked with * are mandatory)</small>
                        <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                        @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection
