@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="background-lightgrey">
        <div class="container custom-container px-5">
            <div class="row w-100 pt-5">
                <div class="col-lg-7">
                    <h2 class="text-uppercase text-blue pb-3">Declare a Motor Claim</h2>
                    <p class="text-custom-color">Please complete the form below to declare your car accident</p>
                </div>
            </div>
            <form id="contact" action="{{Route('motor-claim.submit', $locale)}}" method="POST" class="mt-3" enctype="multipart/form-data">
                @csrf
                @if(session('status'))
                <div class="col-12 p-0 pt-3">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                @endif
                <div>
                    <h3 class="font-bold text-blue">Personal Data</h3>

                    <div id="personalForm" class="data-form min-height-auto position-relative">
                        <p class="font-bold text-blue">Step 1 | Personal Data</p>
                        <div class="row pb-4 w-100">
                            <div class="col-lg-4">
                                <div class="my-3">
                                    <input class="input-text required" name="pas_number" id="pas_number" placeholder="PAS #*" type="number" value="{{old('pas_number')}}" />
                                    @if($errors->has('pas_number'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('pas_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="my-3">
                                    <input class="input-text required" name="plate_number" id="plate_number" placeholder="Plate #*" type="number" value="{{old('plate_number')}}" />
                                    @if($errors->has('plate_number'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('plate_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="my-3">
                                    <input class="input-text required" name="plate_code" id="plate_code" placeholder="Plate Code*" type="text" value="{{old('plate_code')}}" />
                                    @if($errors->has('plate_code'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('plate_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class=" col-lg-4">
                                <div class="my-3">
                                    <input class="input-text required" name="driver_name" id="driver_name" placeholder="Driver Name*" type="text" value="{{old('driver_name')}}" />
                                    @if($errors->has('driver_name'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('driver_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="my-3">
                                    <input class="input-text required" name="mobile_number" id="mobile_number" placeholder="Mobile Number*" type="number" value="{{old('mobile_number')}}" />
                                    @if($errors->has('mobile_number'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="my-3">
                                    <input class="input-text required" name="email" id="email" placeholder="Enter Your Email Address*" type="text" value="{{old('email')}}" />
                                    @if($errors->has('email'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class=" col-lg-4 my-3">
                                <p class="font-bold">Preferred way of communication* </p>
                                @if($errors->has('communication'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('communication') }}</strong>
                                </span>
                                @endif
                                <div class="mt-1 d-flex">
                                    <input type="radio" value='sms' @if(old('communication')=='sms' ) checked @endif id="comm-sms" name="communication" class="required">
                                    <label class="ml-2" for="sms">by SMS</label>

                                    <input class="ml-2" type="radio" value='email' id="comm-email" @if(old('communication')=='email' ) checked @endif name="communication" class="required">
                                    <label class="ml-2" for="email">by e-mail</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="font-bold text-blue">Accident Details</h3>

                    <div id="accidentForm" class="data-form min-height-auto position-relative">
                        <p class="font-bold text-blue">Step 2 | Accident Details</p>
                        <div class="row w-100">
                            <div class="col-lg-4">
                                <div class="my-3">
                                    <input class="input-text datetimepicker required" name="accident_date" placeholder="Accident Date*" type="text" value="{{old('accident_date')}}" />
                                    @if($errors->has('accident_date'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('accident_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="my-3">
                                    <input class="input-text datetimepicker required" data-options='{"enableTime":true,"noCalendar":true,"dateFormat":"H:i","disableMobile":true}' name="accident_time" placeholder="Accident Time*" type="text" value="{{old('accident_time')}}" />
                                    @if($errors->has('accident_time'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('accident_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="my-3">
                                    <input class="input-text required" name="accident_location" placeholder="Accident Location*" type="text" value="{{old('accident_location')}}" />
                                    @if($errors->has('accident_location'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('accident_location') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class=" col-lg-4">
                                <div class="my-3">
                                    <p class="font-bold">Expert</p>
                                    @if($errors->has('expert'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('expert') }}</strong>
                                    </span>
                                    @endif
                                    <div class="mt-1 d-flex">
                                        <input type="radio" value='yes' id="yes" name="expert" class="required" @if(old('expert')=='yes' ) checked @endif>
                                        <label class="ml-2" for="yes">Yes</label>
                                        <input class="ml-3 required" type="radio" value='no' id="no" name="expert" @if(old('expert')=='no' ) checked @endif>
                                        <label class="ml-2" for="no">No</label>
                                    </div>
                                    <input class="input-text" name="expert_name" placeholder="Name Of Expert" type="text" value="{{old('expert_name')}}" />
                                    @if($errors->has('expert_name'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('expert_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="my-3">
                                    <p class="font-bold">Bodily Injuries</p>
                                    @if($errors->has('bodily_injuries'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('bodily_injuries') }}</strong>
                                    </span>
                                    @endif
                                    <div class="mt-1 d-flex">
                                        <input type="radio" value='yes' id="yes" name="bodily_injuries" class="required" @if(old('bodily_injuries')=='yes' ) checked @endif>
                                        <label class="ml-2" for="yes">Yes</label>

                                        <input class="ml-3 required" type="radio" value='no' id="no" name="bodily_injuries" @if(old('bodily_injuries')=='no' ) checked @endif>
                                        <label class="ml-2" for="no">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-lg-4">
                                <div class="my-3">
                                    <p class="font-bold">Accident Description</p>
                                    <div class="mt-1">
                                        <textarea class="input-text required" id="accident_description" name="accident_description" rows="4" cols="10" placeholder="Accident Description"></textarea>
                                        @if($errors->has('accident_description'))
                                        <span class="invalid-feedback text-danger d-block" role="alert">
                                            <strong>{{ $errors->first('accident_description') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="row w-100">
                            <div class="col-lg-12 my-3">
                                <p class="font-bold">Upload Photo* </p>
                            </div>
                            <div class="col-lg-5 my-3">
                                <div class="input-text d-inline-flex mt-2">
                                    <input class="right-license-upload  license-upload w-100 required" name="front_driving_license" type="file" value="{{old('front_driving_license')}}" />
                                    <img class="float-right icon-small" src="{{asset('assets_web/images/upload.svg')}}">
                                </div>
                                @if($errors->has('front_driving_license'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('front_driving_license') }}</strong>
                                </span>
                                @endif
                                <div class="input-text d-inline-flex my-3">
                                    <input class="right-regid-upload regid-upload w-100 required" name="front_car_reg_id" type="file" value="{{old('front_car_reg_id')}}" />
                                    <img class="float-right icon-small" src="{{asset('assets_web/images/upload.svg')}}">
                                </div>
                                @if($errors->has('front_car_reg_id'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('front_car_reg_id') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-lg-5 my-3">
                                <div class="input-text d-inline-flex mt-2">
                                    <input class="left-license-upload license-upload w-100 required" name="back_driving_license" type="file" value="{{old('back_driving_license')}}" />
                                    <img class="float-right icon-small" src="{{asset('assets_web/images/upload.svg')}}">
                                </div>
                                @if($errors->has('back_driving_license'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('back_driving_license') }}</strong>
                                </span>
                                @endif
                                <div class="input-text d-inline-flex my-3">
                                    <input class="left-regid-upload regid-upload w-100 required" name="back_car_reg_id" type="file" value="{{old('back_car_reg_id')}}" />
                                    <img class="float-right icon-small" src="{{asset('assets_web/images/upload.svg')}}">
                                </div>
                                @if($errors->has('back_car_reg_id'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('back_car_reg_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <h3 class="font-bold text-blue">Car Damages</h3>

                    <div id="carDamagesForm" class="data-form min-height-auto position-relative" response=''>
                        <p class="font-bold text-blue">Step 3 | Car Damages</p>
                        <div class="row pb-4 w-100">
                            <div class="col-lg-3">
                                <div class="mt-3">
                                    <div class="picture-wrapper padding-top-65">
                                        <img class="contain" src="{{asset($claim_car_front->image)}}" alt="{{$claim_car_front->title}}">
                                    </div>
                                    <input class="input-text my-3" name="front_car_details" placeholder="Specify Front Accident Locations" type="text" value="{{old('front_car_details')}}" />
                                    @if($errors->has('front_car_details'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('front_car_details') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mt-3">
                                    <div class="picture-wrapper padding-top-65">
                                        <img class="contain" src="{{asset($claim_car_back->image)}}" alt="{{$claim_car_back->title}}">
                                    </div>
                                    <input class="input-text my-3" name="back_car_details" placeholder="Specify Back Accident Locations" type="text" value="{{old('back_car_details')}}" />
                                    @if($errors->has('back_car_details'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('back_car_details') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mt-3">
                                    <div class="picture-wrapper padding-top-65">
                                        <img class="contain" src="{{asset($claim_car_left->image)}}" alt="{{$claim_car_left->title}}">
                                    </div>
                                    <input class="input-text my-3" name="left_car_details" placeholder="Specify Left Accident Locations" type="text" value="{{old('left_car_details')}}" />
                                    @if($errors->has('left_car_details'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('left_car_details') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mt-3">
                                    <div class="picture-wrapper padding-top-65">
                                        <img class="contain" src="{{asset($claim_car_right->image)}}" alt="{{$claim_car_right->title}}">
                                    </div>
                                    <input class="input-text my-3" name="right_car_details" placeholder="Specify Right Accident Locations" type="text" value="{{old('right_car_details')}}" />
                                    @if($errors->has('right_car_details'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('right_car_details') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="font-bold text-blue">Survey & Reparation</h3>
                    <div id="surveyForm" class="data-form min-height-auto position-relative">
                        <p class="font-bold text-blue">Step 4 | Survey & Reparation</p>
                        <div class="row py-3 w-100">
                            <div class="col-lg-4">
                                <div>
                                    <p class="mb-3">Choose your preferred Workshop from AROPE Network</p>
                                    @include('web.components.dropdown', ['type'=>'workshop', 'placeholder' => 'AROPE Network of Workshops', 'form'=>'motor-claim-form', 'class'=>'mt-2 mb-3', 'data' => $workshops])
                                    <!-- @if($errors->has('workshop'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('workshop') }}</strong>
                                    </span>
                                    @endif -->
                                    <?php $workshops_array = $workshops->toArray() ?>
                                    <input class="input-text mt-4" @if(old('workshop') !=null) @if(!in_array(old('workshop'), $workshops_array)) value="{{old('workshop')}}" @endif @endif name="workshop" placeholder="A Workshop of your choice" type="text" />
                                    @if($errors->has('workshop'))
                                    <span class="invalid-feedback text-danger d-block" role="alert">
                                        <strong>{{ $errors->first('workshop') }}</strong>
                                    </span>
                                    @endif
                                    <p class="font-bold mt-4">Call 1219 for support</p>
                                </div>
                            </div>

                            <div class="col-lg-8">
                                <p class="font-bold text-blue mb-3">Legal Disclaimer</p>
                                <div class="d-flex text-custom-color">
                                    <input type="radio" value='yes' id="yes" name="data_privacy_confirm" class="required" @if(old('data_privacy_confirm')=='yes' ) checked @endif>
                                    <label class="ml-3" for="yes">I declare that I have answered all questions as the best of my knowledge and I pledge to provide to AROPE Insurance s.a.l. all possible assistance in relation to this accident. </label>
                                </div>
                                @if($errors->has('data_privacy_confirm'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>{{ $errors->first('data_privacy_confirm') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="row py-5 w-100 justify-content-center">
                            <div class="col-lg-12">
                                <small>(Fields marked with * are mandatory)</small>
                                <div type="checkbox" class="g-recaptcha mb-3 mt-2 required" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                                @if($errors->has('g-recaptcha-response'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                                    <strong>The recaptcha response is required.</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="py-5 px-5">
        <div class="container custom-container">
            <div class="pt-4 pb-4">
                <h1 class="pt-4 font-medium text-blue text-uppercase">Claims procedures</h1>
                <p class="pt-2">Key Consolidated Financial Indicators for 2019 </p>
            </div>
            <div class="row pb-5">
                @foreach($claim_procedures as $key => $claim_procedure)
                @include('web.components.solution-object', ['section' => 'claim', 'page' => 'claim', 'image_order' => $key, 'image_exists' => 'false', 'image_main_title'=>$claim_procedure->title, 'image_text'=>$claim_procedure->text, 'button_text'=>'learn more'])
                @endforeach
            </div>
        </div>
    </div>

</div>
@endsection
@push('script')
<script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js" type="text/javascript" charset="utf-8"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
<script>
    var form = $("#contact");
    if (form) {
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.parent().after(error);
            }
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "div",
            transitionEffect: "slideLeft",
            onStepChanging: function(event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function(event, currentIndex) {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function(event, currentIndex) {
                document.getElementById('contact').submit();
            }
        });
    }
</script>
@endpush