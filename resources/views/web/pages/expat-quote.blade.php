@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('expat-quote.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue text-uppercase">{{ $page->title }}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="row w-100 h-100">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Employer Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="first_name" placeholder="First Name*" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif

                        <input name="d_o_b" placeholder="Applicant Date Of Birth*" class="input-text my-2 datetimepicker" id="datepicker" type="text" value="{{old('d_o_b') }}" data-options='{"disableMobile":true}' required />
                        @if($errors->has('d_o_b'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The Applicant Date of Birth is required.</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="address" placeholder="Detailed Address*" type="text" value="{{old('address')}}" />
                        @if($errors->has('address'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                        @endif

                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="father_name" placeholder="Father's Name*" type="text" value="{{old('father_name')}}" />
                        @if($errors->has('father_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('father_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="landline_number" placeholder="Landline Number" type="text" value="{{old('landline_number')}}" />
                        @if($errors->has('landline_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('landline_number') }}</strong>
                        </span>
                        @endif


                        <input class="input-text my-2" name="email" placeholder="Email Address*" type="text" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif

                    </div>

                    <div class="col-lg-4">

                        <input class="input-text my-2" name="last_name" placeholder="Last Name*" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="occupation" placeholder="Occupation*" type="text" value="{{old('occupation')}}" />
                        @if($errors->has('occupation'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('occupation') }}</strong>
                        </span>
                        @endif

                    </div>
                </div>
                <div class="row w-100 h-100">
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Foreign Worker Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Age Bracket*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='18y-50y' id="first" name="age_bracket">
                                    <label class="ml-2" for="first">18 Y - 50 Y</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='51y-55y' id="second" name="age_bracket">
                                    <label class="ml-2" for="second">51 Y - 55 Y</label>
                                </div>
                            </div>

                        </div>
                        @if($errors->has('age_bracket'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('age_bracket') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Gender*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='Male' id="Male" name="gender">
                                    <label class="ml-2" for="Male">Male</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Female' id="Female" name="gender">
                                    <label class="ml-2" for="Female">Female</label>
                                </div>
                            </div>

                        </div>
                        @if($errors->has('gender'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Occupation*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='Housekeeper' id="housekeeper" name="occupation_field">
                                    <label class="ml-2" for="housekeeper">Housekeeper</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Janitor/ Doorkeeper' id="janitor" name="occupation_field">
                                    <label class="ml-2" for="janitor">Janitor/ Doorkeeper</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Others' id="others" name="occupation_field">
                                    <label class="ml-2" for="others">Others</label>
                                </div>
                            </div>

                        </div>
                        @if($errors->has('occupation_field'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('occupation_field') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row py-5 w-100 justify-content-center">
                    <div class="col-lg-12">
                        <small>(Fields marked with * are mandatory)</small>
                        <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                        @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
