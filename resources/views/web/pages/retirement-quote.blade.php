@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('retirement-quote.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue text-uppercase">{{ $page->title }}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="row w-100 h-100">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Applicant Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="first_name" placeholder="First Name*" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="landline_number" placeholder="Landline Number" type="text" value="{{old('landline_number')}}" />
                        @if($errors->has('landline_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('landline_number') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="occupation" placeholder="Occupation*" type="text" value="{{old('occupation')}}" />
                        @if($errors->has('occupation'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('occupation') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="father_name" placeholder="Father's Name*" type="text" value="{{old('father_name')}}" />
                        @if($errors->has('father_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('father_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif

                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="last_name" placeholder="Last Name*" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="email" placeholder="Email Address*" type="text" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif

                    </div>
                </div>
                <div class="row w-100 h-100">
                    <div class="col-12 mt-4">
                        <h5 class="text-blue text-uppercase">Plan Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input name="d_o_b" placeholder="Applicant Date Of Birth*" class="input-text my-2 datetimepicker" id="datepicker" type="text" value="{{old('d_o_b') }}" data-options='{"disableMobile":true}' required />
                        @if($errors->has('d_o_b'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The Applicant Date of Birth is required.</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="protection_amount" placeholder="Protection Amount*" type="text" value="{{old('protection_amount')}}" />
                        <small>(in case of death)</small>
                        @if($errors->has('protection_amount'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('protection_amount') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="retirement_age" placeholder="Retirement Age*" type="text" value="{{old('retirement_age')}}" />
                        @if($errors->has('retirement_age'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('retirement_age') }}</strong>
                        </span>
                        @endif
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Contribution Frequency*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input type="radio" value='Annual' id="first" name="contribution_frequency">
                                    <label class="ml-2" for="first">Annual</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Semi Annual' id="second" name="contribution_frequency">
                                    <label class="ml-2" for="second">Semi Annual</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Monthly' id="third" name="contribution_frequency">
                                    <label class="ml-2" for="third">Monthly</label>
                                </div>
                                <div class="text-custom-color">
                                    <input type="radio" value='Quarterly' id="fourth" name="contribution_frequency">
                                    <label class="ml-2" for="fourth">Quarterly</label>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('contribution_frequency'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('contribution_frequency') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="retirement_amount" placeholder="Retirement Amount*" type="text" value="{{old('retirement_amount')}}" />
                        @if($errors->has('retirement_amount'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('retirement_amount') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="contribution_amount" placeholder="Contribution Amount*" type="text" value="{{old('contribution_amount')}}" />
                        @if($errors->has('contribution_amount'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('contribution_amount') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row py-5 w-100 justify-content-center">
                    <div class="col-lg-12">
                        <small>(Fields marked with * are mandatory)</small>
                        <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                        @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection
