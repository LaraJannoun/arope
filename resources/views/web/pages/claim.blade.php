@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="background-lightgrey">
        <div class="py-5 px-5">
            <form action="{{Route('claim.submit', $locale)}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="container custom-container">
                    <h1 class="py-4 font-medium text-blue">{{$page->title}}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                    <div class="row w-100 h-100 pb-4">
                        @if(session('status'))
                        <div class="col-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        @endif
                        <div class="col-lg-4">
                            <input class="input-text my-2" name="first_name" placeholder="First Name" type="text" value="{{old('first_name')}}" />
                            @if($errors->has('first_name'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                            @endif

                            <input class="input-text my-2" name="last_name" placeholder="Last Name" type="text" value="{{old('last_name')}}" />
                            @if($errors->has('last_name'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                            @endif

                            <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number" type="text" value="{{old('mobile_number')}}" />
                            @if($errors->has('mobile_number'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('mobile_number') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="col-lg-4">
                            <input class="input-text my-2" name="email" placeholder="Email Address" type="text" value="{{old('email')}}" />
                            @if($errors->has('email'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif

                            @include('web.components.dropdown', ['type'=>'values', 'placeholder' => 'Claim Type', 'form'=>'claim-form', 'class'=>'mt-2 mb-3', 'data' => $types])
                            @if($errors->has('value'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('value') }}</strong>
                            </span>
                            @endif

                            <input class="input-text my-2" name="fields" placeholder="Fields Needed" type="text" value="{{old('fields')}}" />
                            @if($errors->has('fields'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('fields') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="col-lg-4">
                            <textarea class="input-text my-2" id="message" name="message" rows="6" cols="10" placeholder="Your Message"></textarea>
                            @if($errors->has('message'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="row py-5 w-100 justify-content-center">
                        <div class="col-lg-12">
                            <small>(Fields marked with * are mandatory)</small>
                            <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                            @if($errors->has('g-recaptcha-response'))
                                <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                            @endif
                        </div>
                        <div class="col-lg-3">
                            <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="py-5 px-5">
        <div class="container custom-container">
            <div class="pt-4 pb-4">
                <h1 class="pt-4 font-medium text-blue text-uppercase">Claims procedures</h1>
                <p class="pt-2">Key Consolidated Financial Indicators for 2019 </p>
            </div>
            <div class="row pb-5">
                @foreach($claim_procedures as $key => $claim_procedure)
                @include('web.components.solution-object', ['section' => 'claim', 'page' => 'claim', 'image_order' => $key, 'image_exists' => 'false', 'image_main_title'=>$claim_procedure->title, 'image_text'=>$claim_procedure->text, 'button_text'=>'learn more'])
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
