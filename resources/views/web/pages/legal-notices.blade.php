@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="parent-container padding-top">
        <div class="container py-5">
            <h1 class="font-bold mb-5">{{$legalNotices->title}}</h1>
            <div>{!! $legalNotices->text !!}</div>
        </div>
    </div>
</div>
@endsection