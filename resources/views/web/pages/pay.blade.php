@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="pay-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="container custom-container p-5 px-5">
        <div class="py-5">
            <h2 class="font-medium text-blue pb-4 text-uppercase">{{$page->title}}</h2>
            <p>{!!$page->text!!}</p>
        </div>
        <div class="row pb-5">
            @foreach($payment_steps as $key => $payment_step)
            @include('web.components.career-object', ['page' => 'pay', 'image_source'=>$payment_step->image, 'image_main_title'=>$payment_step->title, 'image_text'=>$payment_step->text])
            @endforeach
        </div>
        <div class="row pb-5 w-100 justify-content-center">
            <a href="https://payonline.earope.com/en">
                <div class="col-lg-2">
                    <button class="dark-blue-button hvr-grow text-uppercase w-100">GET started!</button>
            </a>
        </div>
    </div>
</div>
</div>
@endsection
