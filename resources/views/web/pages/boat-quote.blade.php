@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="boat-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('boat-quote.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue text-uppercase">{{ $page->title }}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="row w-100 h-100">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Applicant Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="first_name" placeholder="First Name*" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif


                        <input class="input-text my-2" name="landline_number" placeholder="Landline Number" type="text" value="{{old('landline_number')}}" />
                        @if($errors->has('landline_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('landline_number') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="occupation" placeholder="Occupation*" type="text" value="{{old('occupation')}}" />
                        @if($errors->has('occupation'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('occupation') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="father_name" placeholder="Father's Name*" type="text" value="{{old('father_name')}}" />
                        @if($errors->has('father_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('father_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="last_name" placeholder="Last Name*" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="email" placeholder="Email Address*" type="text" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif

                    </div>
                </div>
                <div class="row w-100 h-100">
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Vessel Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="vessel_name" placeholder="Vessel Name*" type="text" value="{{old('vessel_name')}}" />
                        @if($errors->has('vessel_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('vessel_name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <select class="input-text my-2 rounded-0" name="year_of_make">
                            <option value="">Year Of Make</option>
                            @for($x = 2022; $x>=1970; $x--)
                            <option @if(old('year_of_make')==$x) selected @endif value="{{$x}}">{{$x}}</option>
                            @endfor
                        </select>
                        @if($errors->has('year_of_make'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('year_of_make') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row w-100 h-100">
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Navigation Limit*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input @if(old('navigation_limit')=="Mediterranean sea" ) checked @endif type="radio" value='Mediterranean sea' id="first" name="navigation_limit">
                                    <label class="ml-2" for="first">Mediterranean sea</label>
                                </div>
                                <div class="text-custom-color">
                                    <div class="d-flex flex-row">
                                        <input @if(old('navigation_limit')=="Other" ) checked @endif type="radio" value='Other' id="other" name="navigation_limit" class="mt-1">
                                        <label class="mx-2" for="other">Other</label>
                                        <input class="input-text py-0" type="text" id="other_navigation_limit" name="other_navigation_limit" value="{{old('other_navigation_limit')}}">

                                    </div>
                                    <small>(Enter your navigation limit)</small>
                                </div>
                            </div>

                        </div>
                        @if($errors->has('navigation_limit'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('navigation_limit') }}</strong>
                        </span>
                        @endif
                        @if($errors->has('other_navigation_limit'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('other_navigation_limit') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="row w-100 h-100">
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Plan Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <p class="pb-4 px-4 text-input-color">BASIC COVERS*</p>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input @if(old('basic_cover')=="Third Party Liability" ) checked @endif type="radio" value='Third Party Liability' id="tpl" name="basic_cover">
                                    <label class="ml-2" for="tpl">Third Party Liability</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('basic_cover')=="Loss/Damage caused by Named Perils" ) checked @endif type="radio" value='Loss/Damage caused by Named Perils' id="loss" name="basic_cover">
                                    <label class="ml-2" for="loss">Loss/Damage caused by Named Perils</label>
                                </div>


                            </div>

                        </div>
                        @if($errors->has('basic_cover'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('basic_cover') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <p class="pb-4 px-4 text-input-color">OPTIONAl COVERS*</p>
                            <div class="px-5">


                                <div class="text-custom-color">
                                    <input @if(old('basic_cover')=="Additional Machinery Damage" ) checked @endif type="radio" value='Additional Machinery Damage' id="amd" name="optional_cover">
                                    <label class="ml-2" for="amd">Additional Machinery Damage</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('optional_cover')=="Personal Effect" ) checked @endif type="radio" value='Personal Effect' id="personal_effect" name="optional_cover">
                                    <label class="ml-2" for="personal_effect">Personal Effect</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('basic_cover')=="War strikes, Riots & Civil Commotions" ) checked @endif type="radio" value='War strikes, Riots & Civil Commotions' id="war-strikes" name="optional_cover">
                                    <label class="ml-2" for="war-strikes">War strikes, Riots & Civil Commotions</label>
                                </div>

                            </div>

                        </div>
                        @if($errors->has('optional_cover'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('optional_cover') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row py-5 w-100 justify-content-center">
                    <div class="col-lg-12">
                        <small>(Fields marked with * are mandatory)</small>
                        <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                        @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                    </div>
                </div>
            </div>

        </form>
    </div>


</div>
@endsection
