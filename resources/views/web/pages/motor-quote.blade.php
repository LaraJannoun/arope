@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="quote-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="py-5 px-5">
        <form action="{{Route('motor-quote.submit', $locale)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container custom-container">
                <div class="">
                    <h1 class="py-4 font-medium text-blue text-uppercase">{{ $page->title }}</h1>
                    <div class="pb-3">{!!$page->text!!}</div>
                </div>
                <div class="alert alert-success alert-dismissible fade show d-none" id="new-window-error" role="alert">
                    Please make sure to fill the form opened in your new tab.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row w-100 h-100">
                    @if(session('status'))
                    <div class="col-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Applicant Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="first_name" placeholder="First Name*" type="text" value="{{old('first_name')}}" />
                        @if($errors->has('first_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="landline_number" placeholder="Landline Number" type="text" value="{{old('landline_number')}}" />
                        @if($errors->has('landline_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('landline_number') }}</strong>
                        </span>
                        @endif

                        <input class="input-text my-2" name="occupation" placeholder="Occupation*" type="text" value="{{old('occupation')}}">
                        @if($errors->has('occupation'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('occupation') }}</strong>
                        </span>
                        @endif

                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="father_name" placeholder="Father's Name*" type="text" value="{{old('father_name')}}" />
                        @if($errors->has('father_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('father_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="mobile_number" placeholder="Mobile Number*" type="text" value="{{old('mobile_number')}}" />
                        @if($errors->has('mobile_number'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                        @endif


                    </div>

                    <div class="col-lg-4">
                        <input class="input-text my-2" name="last_name" placeholder="Last Name*" type="text" value="{{old('last_name')}}" />
                        @if($errors->has('last_name'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                        <input class="input-text my-2" name="email" placeholder="Email Address*" type="text" value="{{old('email')}}" />
                        @if($errors->has('email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif

                    </div>
                </div>
                <div class="row w-100 h-100">
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Cover Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Insurance Type*</h5>
                            <div class="px-5">
                                <div class="text-custom-color">
                                    <input onchange="myFunction()" @if(old('insurance_type')=="third_party_liability" ) checked @endif type="radio" value='third_party_liability' id="third_party_liability" name="insurance_type">
                                    <label class="ml-2" for="third_party_liability">Third Party Liability</label>
                                </div>
                                <div class="text-custom-color">
                                    <input onchange="myFunction()" @if(old('insurance_type')=="motor_all_risk" ) checked @endif type="radio" value='motor_all_risk' id="motor_all_risk" name="insurance_type">
                                    <label class="ml-2" for="motor_all_risk">Motor all Risk</label>
                                </div>
                                <div class="text-custom-color">
                                    <input onchange="myFunction()" @if(old('insurance_type')=="orange_card" ) checked @endif type="radio" value='orange_card' id="orange_card" name="insurance_type">
                                    <label class="ml-2" for="orange_card">Orange Card</label>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('insurance_type'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('insurance_type') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row w-100 h-100" id="vehicle_details" @if(in_array(old('insurance_type'), ['third_party_liability','motor_all_risk','orange_card'])) style="display: flex" @else style="display: none" @endif>
                    <div class="col-12 mt-2">
                        <h5 class="text-blue text-uppercase">Vehicle Details</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Vehicle Type*</h5>
                            <div class="px-5">
                                <a href="">
                                    <div class="text-custom-color">
                                        <input @if(old('vehicle_type')=="Touristic Car/Jeep" ) checked @endif type="radio" value='Touristic Car/Jeep' id="touristic_car_jeep" name="vehicle_type">
                                        <label class="ml-2" for="touristic_car_jeep">Touristic Car/Jeep</label>
                                    </div>
                                </a>
                                <div class="text-custom-color">
                                    <input @if(old('vehicle_type')=="Sports Car" ) checked @endif type="radio" value='Sports Car' id="sports_car" name="vehicle_type">
                                    <label class="ml-2" for="sports_car">Sports Car</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('vehicle_type')=="Taxi" ) checked @endif type="radio" value='Taxi' id="taxi" name="vehicle_type">
                                    <label class="ml-2" for="taxi">Taxi</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('vehicle_type')=="Pickup" ) checked @endif type="radio" value='Pickup' id="taxi" name="vehicle_type">
                                    <label class="ml-2" for="pickup">Pickup</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('vehicle_type')=="Cargo Van" ) checked @endif type="radio" value='Cargo Van' id="cargo_van" name="vehicle_type">
                                    <label class="ml-2" for="cargo_van">Cargo Van</label>
                                </div>

                                <div class="text-custom-color">
                                    <input @if(old('vehicle_type')=="Motorcycle" ) checked @endif type="radio" value='Motorcycle' id="motorcycle" name="vehicle_type">
                                    <label class="ml-2" for="motorcycle">Motorcycle</label>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('vehicle_type'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('vehicle_type') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <input class="input-text my-2" name="brand" placeholder="Brand*" type="text" value="{{old('brand')}}" />
                        @if($errors->has('brand'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('brand') }}</strong>
                        </span>
                        @endif
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Usage*</h5>
                            <div class="px-5">
                                <a href="">
                                    <div class="text-custom-color">
                                        <input @if(old('usage')=="Private" ) checked @endif type="radio" value='Private' id="private" name="usage">
                                        <label class="ml-2" for="private">Private</label>
                                    </div>
                                </a>
                                <div class="text-custom-color">
                                    <input @if(old('usage')=="Commercial" ) checked @endif type="radio" value='Commercial' id="commercial" name="usage">
                                    <label class="ml-2" for="commercial">Commercial</label>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('usage'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('usage') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <select class="input-text my-2 rounded-0" name="year_of_make">
                            <option value="">Year Of Make</option>
                            @for($x = 1950; $x<2022; $x++) <option @if(old('year_of_make')==$x) selected @endif value="{{$x}}">{{$x}}</option>
                                @endfor
                        </select>
                        @if($errors->has('year_of_make'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('year_of_make') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row w-100 h-100">
                    <div class="col-12 mt-2" id="plan_title" @if(in_array(old('insurance_type'), ['third_party_liability','motor_all_risk','orange_card'])) style="display: block" @else style="display: none" @endif>
                        <h5 class="text-blue text-uppercase">Plan Details</h5>
                    </div>
                    <div class="col-lg-4" id="third_party_plan" @if(old('insurance_type')=="third_party_liability" ) style="display: block" @else style="display: none" @endif>
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Third Party Liability*</h5>
                            <div class="px-5">

                                <div class="text-custom-color">
                                    <input @if(old('third_party_value')=="Material Damages" ) checked @endif type="radio" value='Material Damages' id="material_damages" name="third_party_value">
                                    <label class="ml-2" for="material_damages">Private</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('third_party_value')=="Bodily Injuries" ) checked @endif type="radio" value='Bodily Injuries' id="bodily_injuries" name="third_party_value">
                                    <label class="ml-2" for="bodily_injuries">Commercial</label>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('third_party_value'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('third_party_value') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4" id="motor_all_risk_plan" @if(old('insurance_type')=="motor_all_risk" ) style="display: block" @else style="display: none" @endif>
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">All Risk*</h5>
                            <div class="px-5">
                                <a href="">
                                    <div class="text-custom-color">
                                        <input @if(old('all_risk_value')=="Motor All Risk" ) checked @endif type="radio" value='Motor All Risk' id="motor_all_risk" name="all_risk_value">
                                        <label class="ml-2" for="motor_all_risk">Motor All Risk</label>
                                    </div>
                                </a>
                                <div class="text-custom-color">
                                    <input @if(old('all_risk_value')=="Total Loss" ) checked @endif type="radio" value='Total Loss' id="total_loss" name="all_risk_value">
                                    <label class="ml-2" for="total_loss">Total Loss</label>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('all_risk_value'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('all_risk_value') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4" id="orange_card_plan" @if(old('insurance_type')=="orange_card" ) style="display: block" @else style="display: none" @endif>
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Orange Card Destination*</h5>
                            <div class="px-5">

                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Syria" ) checked @endif type="radio" value='Syria' id="syria" name="orange_card_destination_value">
                                    <label class="ml-2" for="syria">Syria</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Jordan" ) checked @endif type="radio" value='Jordan' id="jordan" name="orange_card_destination_value">
                                    <label class="ml-2" for="jordan">Jordan</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Iraq" ) checked @endif type="radio" value='Iraq' id="iraq" name="orange_card_destination_value">
                                    <label class="ml-2" for="iraq">Iraq</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Kuwait" ) checked @endif type="radio" value='Kuwait' id="kuwait" name="orange_card_destination_value">
                                    <label class="ml-2" for="kuwait">Kuwait</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Bahrain" ) checked @endif type="radio" value='Bahrain' id="bahrain" name="orange_card_destination_value">
                                    <label class="ml-2" for="bahrain">Bahrain</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="UAE" ) checked @endif type="radio" value='UAE' id="uae" name="orange_card_destination_value">
                                    <label class="ml-2" for="uae">UAE</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Qatar" ) checked @endif type="radio" value='Qatar' id="qatar" name="orange_card_destination_value">
                                    <label class="ml-2" for="qatar">Qatar</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Egypt" ) checked @endif type="radio" value='Egypt' id="egypt" name="orange_card_destination_value">
                                    <label class="ml-2" for="egypt">Egypt</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Libya" ) checked @endif type="radio" value='Libya' id="libya" name="orange_card_destination_value">
                                    <label class="ml-2" for="libya">Libya</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Tunisia" ) checked @endif type="radio" value='Tunisia' id="tunisia" name="orange_card_destination_value">
                                    <label class="ml-2" for="tunisia">Tunisia</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Algeria" ) checked @endif type="radio" value='Algeria' id="algeria" name="orange_card_destination_value">
                                    <label class="ml-2" for="algeria">Algeria</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Morocco" ) checked @endif type="radio" value='Morocco' id="morocco" name="orange_card_destination_value">
                                    <label class="ml-2" for="morocco">Morocco</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Lebanon" ) checked @endif type="radio" value='Lebanon' id="lebanon" name="orange_card_destination_value">
                                    <label class="ml-2" for="lebanon">Lebanon</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Oman" ) checked @endif type="radio" value='Oman' id="oman" name="orange_card_destination_value">
                                    <label class="ml-2" for="oman">Oman</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('orange_card_destination_value')=="Palestine" ) checked @endif type="radio" value='Palestine' id="palestine" name="orange_card_destination_value">
                                    <label class="ml-2" for="palestine">Palestine</label>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('orange_card_destination_value'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('orange_card_destination_value') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-4">
                        <div class="border-blue py-4 my-2">
                            <h5 class="pb-4 px-4 text-input-color">Period Of Insurance*</h5>
                            <div class="px-5">

                                <div class="text-custom-color">
                                    <input @if(old('period')=="1 Month" ) checked @endif type="radio" value='1 Month' id="period" name="period">
                                    <label class="ml-2" for="period">1 Month</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('period')=="3 Months" ) checked @endif type="radio" value='3 Months' id="period" name="period">
                                    <label class="ml-2" for="period">3 Month</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('period')=="6 Months" ) checked @endif type="radio" value='6 Months' id="period" name="period">
                                    <label class="ml-2" for="period">6 Month</label>
                                </div>
                                <div class="text-custom-color">
                                    <input @if(old('period')=="12 Months" ) checked @endif type="radio" value='12 Months' id="period" name="period">
                                    <label class="ml-2" for="period">12 Month</label>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('period'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('period') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="row py-5 w-100 justify-content-center">
                    <div class="col-lg-12">
                        <small>(Fields marked with * are mandatory)</small>
                        <div type="checkbox" class="g-recaptcha mb-3 mt-2" data-sitekey="{{env('RECAPTCHA_SITEKEY')}}"></div>
                        @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>The recaptcha response is required.</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <button class="dark-blue-button hvr-grow text-uppercase w-100" type="submit">Request a quote</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection
@push('script')
<script>
    var external_link = "{{$external_link->link}}";
    $("#third_party_liability").click(function() {
        window.open(external_link, '_blank');
        $("#new-window-error").removeClass("d-none");
    })

    function myFunction() {
        $('#vehicle_details').css('display', 'flex');
        if ($('#third_party_liability').prop('checked')) {
            $('#plan_title').css('display', 'block');
            $('#motor_all_risk_plan').css('display', 'none');
            $('#orange_card_plan').css('display', 'none');
            $('#third_party_plan').css('display', 'block');
        }
        if ($('#motor_all_risk').prop('checked')) {
            $('#plan_title').css('display', 'block');
            $('#third_party_plan').css('display', 'none');
            $('#orange_card_plan').css('display', 'none');
            $('#motor_all_risk_plan').css('display', 'block');
        }
        if ($('#orange_card').prop('checked')) {
            $('#plan_title').css('display', 'block');
            $('#third_party_plan').css('display', 'none');
            $('#motor_all_risk_plan').css('display', 'none');
            $('#orange_card_plan').css('display', 'block');
        }
    }
</script>
@endpush('script')
