@extends('web.layouts.main')

@section('content')
<div class="font-medium">
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="about-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div class="container custom-container p-5">
        <div class="d-inline-block w-100">
            @foreach($page_sections as $key => $page_section)
            <div image_src={{asset($page_section->image)}} slug={{$page_section->slug}} class="float-left option-title">
                <h5 id="{{$page_section->slug}}Option" class="options text-uppercase cursor-pointer @if($key == 0) active @endif">{{$page_section->header_title}}</h5>
            </div>
            @endforeach
        </div>
        <div class="blue-border-bottom">
        </div>
    </div>
    @if(isset($page_sections[0]))
    @php $page_details = $page_sections[0]; @endphp
    <div id="{{$page_sections[0]->slug}}" class="show-hide-div">
        <div class="container custom-container px-5">
            <div class="row d-inline-block w-100 float-left pb-5">
                <div class="col-lg-8">
                    <h2 class="text-uppercase text-blue">{{$page_details->title}}</h2>
                    <h3 class="py-2">{{$page_details->subtitle}}</h3>
                    <p class="py-3 text-custom-color">{{$page_details->text}}</p>
                </div>
            </div>
            <div class="pt-5 pb-2 d-inline-block w-100">
                <div class="float-left">
                    <h2 class="text-uppercase text-blue">Our Milestones</h2>
                </div>
            </div>
        </div>
        <div id="banner-container" class="banner-background text-white" style="background-image:url({{asset('assets_web/images/about-backg-1.png')}})">
            <div class="scrollable-container container py-5">
                <section class="timeline">
                    <ul>
                        @foreach($vertical_timeline as $key => $vertical_year)
                        <li>
                            <div class="time d-inline-block">
                                <time>
                                    <div class="d-inline-flex">
                                        <div class="time-icon icon-large banner-background py-2">
                                            <h5 class="vertically-centered text-white">{{$vertical_year->year}}</h5>
                                        </div>
                                    </div>
                                </time>

                                <div class="text-blue py-2">
                                    @if(str_contains(strtolower($vertical_year->text), 'arope'))
                                    {!! preg_split("/arope/i", $vertical_year->text)[0] !!}
                                    <span class="font-bold">AROPE</span>
                                    {!! preg_split("/arope/i", $vertical_year->text)[1] !!}
                                    @else
                                    {!! $vertical_year->text !!}
                                    @endif
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </section>
            </div>
        </div>

        <div class="container custom-container p-5">
            <div class="row py-4 w-100">
                <div class="col-lg-6">
                    <h2 class="font-medium text-italic text-uppercase text-blue py-3">{{$about_items_section_1->title}}</h2>
                    <p>{{$about_items_section_1->text}}</p>
                </div>
            </div>

            <div class="row pb-5">
                @foreach($about_items_section_3 as $key => $about_items_section)
                @include('web.components.about-object', ['image_order' => $key, 'image_source'=>$about_items_section->image, 'image_main_title'=>$about_items_section->title, 'image_text'=>$about_items_section->text])
                @endforeach
            </div>
        </div>
        <div class="background-lightgrey">
            <div class="container custom-container p-5">
                <h1 class="font-medium text-blue pt-4 text-uppercase">Misson, Vision & Goals</h1>
                <div class="row">
                    @foreach($about_items_section_4 as $key => $about_items_section)
                    @include('web.components.about-object-2', ['image_order' => $key, 'image_source'=>$about_items_section->image, 'image_main_title'=>$about_items_section->title, 'image_text'=>$about_items_section->text])
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container custom-container px-5 pt-5">
            <div class="row py-4 w-100">
                <div class="col-lg-12">
                    <h2 class="text-uppercase text-blue py-3">Our Innovations</h2>
                    <p>AROPE, a trendsetter in the Lebanese Insurance Mart launches at least one new innovative product or service every year</p>
                </div>
            </div>
        </div>
        <div class="container custom-container">
            <section class="horizontal-timeline">
                <ol>
                    @foreach($horizontal_timeline as $key => $horizontal_year)
                    @php $position = 'down';
                    $image_src = 'assets_web/images/year-up.svg';

                    if($key%2 == 0) {
                    $position = 'up';
                    $image_src = 'assets_web/images/year-down.svg';
                    }
                    @endphp
                    <li>
                        @include('web.components.about-object-3', ['type' => $position, 'date'=>$horizontal_year->year, 'image_source'=>$image_src, 'image_main_title'=>$horizontal_year->title, 'image_text'=>$horizontal_year->text])
                    </li>
                    @endforeach
                    <li></li>
                </ol>

                <div class="arrows">
                    <button class="hor-arrow arrow__prev disabled" disabled>
                        <img src="{{ asset('assets_web/images/circle-arrow-left.svg') }}" alt="prev timeline arrow">
                    </button>
                    <button class="hor-arrow arrow__next">
                        <img src="{{ asset('assets_web/images/circle-arrow-right.svg') }}" alt="next timeline arrow">
                    </button>
                </div>
            </section>
        </div>
        <div class="container custom-container p-5">
            <div class="row justify-content-between">
                <div class="col-lg-6">
                    <h2 class="text-uppercase text-blue py-3">{{$quality_policy->title}}</h2>
                    <h4 class="pb-3 font-medium">{{$quality_policy->subtitle}}</h4>
                    @php
                    $arr_points = explode("|", $quality_policy->points);
                    @endphp
                    <ul class="about-list text-custom-color">
                        @foreach($arr_points as $key => $point)
                        <li class="py-1 font-13"><span>{{$point}}</span></li>
                        @endforeach
                    </ul>
                    <p class="py-4 text-custom-color">{{$quality_policy->text}}</p>
                </div>
                <div class="col-lg-5">
                    <div class="picture-wrapper padding-top-90">
                        <img class="object-fit-none" src="{{ asset($quality_policy->image) }}" alt="about-quality-policy">
                    </div>
                </div>
            </div>


        </div>
    </div>
    @endif

    @if(isset($page_sections[1]))
    <div id="{{$page_sections[1]->slug}}" class="show-hide-div d-none">
        <div class="container custom-container p-5">
            <div class="d-inline-block w-100">
                <div class="float-left">
                    <h2 class="text-uppercase text-blue">SHAREHOLDERS</h2>
                </div>
            </div>
            <div class="row py-5 justify-content-between">
                <div class="col-lg-6">
                    <div class="">
                        <img class="contain" src="{{ asset('assets_web/images/temp/shareholders-graph.png') }}" alt="shareholder">
                    </div>
                </div>
                <div class="col-lg-5">
                    @foreach($shareholders as $key => $shareholder)
                    <div class="row @if($key != 0) pt-5 @endif">
                        <div class="col-lg-4">
                            <div class="picture-wrapper padding-top-31">
                                <img class="contain" src="{{ asset($shareholder->image) }}" alt="{{$shareholder->title}}">
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <p class="py-4 text-custom-color">{{$shareholder->text}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="py-5 d-inline-block w-100">
                <div class="float-left pt-5">
                    <h2 class="text-uppercase text-blue pt-5">BOARD OF DIRECTORS & MANAGEMENT TEAM</h2>
                    <h4 class="py-2">Mr. Fateh Bekdache, Chairman & CEO</h4>
                </div>
            </div>
            <div class="row pb-5 justify-content-between">
                <div class="col-lg-6">
                    <div class="background-white shadow-outer-lightgrey p-5">
                        <h4 class="pb-4"><span class="text-blue">{{$board_members->title}}</h4>
                        <div class="row justify-content-between text-custom-color">
                            <div class="col-lg-6">
                                <ul class="about-list">
                                    @php
                                    $arr_board_members = explode("|", $board_members->text);
                                    @endphp
                                    @foreach($arr_board_members as $key => $board_member)
                                    @if($key < 4) <li class="py-1"><span>{{$board_member}}</span></li>
                                        @endif
                                        @endforeach
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="about-list">
                                    @foreach($arr_board_members as $key => $board_member)
                                    @if($key >= 4)
                                    <li class="py-1"><span>{{$board_member}}</span></li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="background-white shadow-outer-lightgrey p-5 text-custom-color">
                        <h4 class="text-blue pb-4">{{$general_management_team->title}}</h4>
                        <ul class="about-list">
                            @php
                            $arr_general_management = explode("|", $general_management_team->text);
                            @endphp
                            @foreach($arr_general_management as $key => $general_management_prsn)
                            @php
                            $full= preg_split("/,/i", $general_management_prsn);
                            $first= $full[0];
                            $rest= ltrim($general_management_prsn, $first);

                            @endphp
                            <li class="py-1"><span class="text-blue">{{$first}}</span><span>{{$rest}}</span></h5>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="background-lightgrey">
            <div class="container custom-container p-5">
                <div class="py-4">
                    <h1 class="font-medium text-blue text-uppercase py-2">REINSURERS</h1>
                    <p class="font-book text-custom-color">AROPE Business is covered by Prominent and Highly-Rated International Reinsurers, including:</p>
                </div>
                <div class="row pb-5">
                    @foreach($reinsureres as $key => $reinsurer)
                    <div class="col-lg-6 py-3">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="picture-wrapper padding-top-100">
                                    <img class="contain" src="{{ asset($reinsurer->image) }}" alt="{{$reinsurer->title}}">
                                </div>
                            </div>
                            <div class="col-lg-6 vertically-centered">
                                <div>
                                    <h5 class="font-bold text-blue py-2">{{$reinsurer->title}}</h5>
                                    <div class="text-custom-color">{!!$reinsurer->text!!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container custom-container p-5">
            <div class="py-5">
                <h2 class="font-medium text-blue text-uppercase py-2">FACTS & FIGURES</h2>
                <h5 class="font-book text-custom-color">Key Consolidated Financial Indicators for 2020</h5>
            </div>
            <div class="row pb-5">
                @foreach($financial_highlights as $key => $financial_highlight)
                <div class="col-lg-2 px-3 hvr-grow mb-4">
                    <div class="text-center p-5 dashed-border">
                        <img class="image-50 height-50" src="{{ asset($financial_highlight->image) }}" alt="{{$financial_highlight->title}}">
                        <h4 class="pt-4 pb-3 text-aqua-blue">{{$financial_highlight->price}}</h4>
                        <p class="text-darkgrey font-16">{{$financial_highlight->title}}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="container custom-container p-5">
            <div class="py-4">
                <h1 class="font-medium text-blue text-uppercase py-2">Annual Report</h1>
            </div>
            <div class="row pb-5">
                @foreach($annual_reports as $key => $annual_report)
                <div class="col-lg-12 my-2">
                    <div class="background-white shadow-outer-lightgrey px-4 py-2 w-100">
                        <div class="d-inline-flex vertically-centered">
                            <img class="icon-small" src="{{ asset('assets_web/images/pdf-icon.svg') }}" alt="pdf">
                            <p class="px-4 text-custom-color">{{$annual_report->title}}</p>
                        </div>
                        <a href="{{ asset($annual_report->attachment) }}" download>
                            <div class="float-right">
                                <img class="icon-small" src="{{ asset('assets_web/images/upload-icon.svg') }}" alt="pdf">
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </div>
    @endif

    @endsection
    @push('script')
    <script>
        $('.option-title').click(function() {
            var slug = $(this).attr("slug");
            var image_src = $(this).attr("image_src");

            $(".show-hide-div").addClass("d-none");
            $("#" + slug).removeClass("d-none");

            $(".options").removeClass("active");
            $(this).find("h5").addClass("active");

            $("#about-banner").css('background-image', "url(" + image_src + ")");
        });
    </script>
    @endpush
