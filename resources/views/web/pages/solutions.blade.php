@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="banner-container">
        <div id="solutions-banner" class="banner-background w-100 h-100 py-5 text-white" style="background-image:url({{asset($page->image)}})">
        </div>
    </div>
    <div id="solutions">
        <div class="container custom-container p-5">
            <div class="py-4 d-inline-block w-100">
                <div class="float-left">
                    <h2 class="text-uppercase text-blue">{{$page->title}}</h2>
                </div>
            </div>
            <div class="d-inline-block w-100">
                @foreach($page_sections as $key => $page_section)
                <div image_src={{asset($page_section->image)}} slug={{$page_section->slug}} class="float-left option-title">
                    <h5 id="{{$page_section->slug}}Option" class="options text-uppercase cursor-pointer @if($key == 0) active @endif">{{$page_section->header_title}}</h5>
                </div>
                @endforeach
            </div>
            <div class="blue-border-bottom"></div>
        </div>
    </div>
    @foreach($page_sections as $key => $page_section)
    @include('web.components.solution-page', ['page'=>'personal', 'key'=>$key, 'page_section'=> $page_sections[$key], 'all_solutions'=>$personal_solutions, 'items'=>$items, 'page_details'=>$page_sections[$key]])
    @endforeach

    @if(count($testimonials) > 0)
    <div class="container custom-small-container py-5">
        <div class="text-center p-5">
            <h2 class="text-uppercase text-blue">Latest from our Clients</h2>
        </div>
        <div class="row justify-content-center">
            @foreach($testimonials as $key => $testimonial)
            <div class="col-lg-2 px-2">
                <div class="picture-wrapper padding-top-90">
                    <img class="contain gray-scale" src="{{ asset($testimonial->image) }}" alt="testimonial">
                </div>
            </div>
            @endforeach
        </div>
        <div class="row justify-content-center pb-5">
            <div class="col-lg-1 vertically-centered">
                <img class="icon-small" src="{{ asset('assets_web/images/left-quot.svg') }}" alt="left-quotation">
            </div>
            <div class="col-lg-10">
                <div class="slider-images-solutions-container">
                    @foreach($testimonials as $key => $testimonial)
                    @include('web.components.slider-object-solution', ['image_order' => $key, 'text'=>$testimonial->text])
                    @endforeach
                </div>
            </div>
            <div class="col-lg-1 vertically-centered align-start">
                <img class="icon-small" src="{{ asset('assets_web/images/right-quot.svg') }}" alt="right-quotation">
            </div>
        </div>
    </div>
    @endif
</div>

@endsection

@push('script')
<script>
    $('.option-title').click(function() {
        var slug = $(this).attr("slug");
        var image_src = $(this).attr("image_src");

        $(".show-hide-div").addClass("d-none");
        $("#" + slug).removeClass("d-none");

        $(".options").removeClass("active");
        $(this).find("h5").addClass("active");

        $("#solutions-banner").css('background-image', "url(" + image_src + ")");
    });
</script>
@endpush