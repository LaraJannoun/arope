@extends('web.layouts.main')

@section('content')
<div>
    @include('web.layouts.headers.header')
    <div class="parent-container padding-top">
        <div class="container py-5">
            <h1 class="font-bold mb-5">{{$disclaimers->title}}</h1>
            <div>{!! $disclaimers->text !!}</div>
        </div>
    </div>
</div>
@endsection