<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>AROPE | {{ $page_title }}</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets_web/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets_web/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets_web/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('assets_web/favicons/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('assets_web/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta charset="utf-8">
    <meta name="title" content="AROPE Insurance">
    <meta name="description" content="">
    <meta name="author" content="AROPE Insurance">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="image" content="">

    <meta property="og:title" content="AROPE Insurance">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="og:card" content="">

    <meta name="twitter:title" content="AROPE Insurance">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <meta name="twitter:card" content="">

    <meta name="fb:title" content="AROPE Insurance">
    <meta name="fb:description" content="">
    <meta name="fb:image" content="">
    <meta name="fb:card" content="">

    <link rel="icon" type="image/png" href="" />
    {{-- Libraries --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/libraries/bootstrap-4.3.1-dist/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/libraries/slick-1.8.1/slick/slick.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/libraries/slick-1.8.1/slick/slick-theme.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/libraries/fontawesome-free-5.15.1-web/css/fontawesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/libraries/fontawesome-free-5.15.1-web/css/all.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/libraries/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/libraries/nouislider/nouislider.min.css') }}" />
    {{-- Fonts --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/fonts/futura/stylesheet.css') }}">
    {{-- Main CSS --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/css/main.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/css/form.css') }}" />

    @if($locale == 'ar')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/css/main-ar.css') }}" />
    @endif
    {{-- Date PICKER CSS--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_web/css/flatpickr.min.css') }}" />
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>
    @yield('content')
    @if(!Route::is('contact-us'))
    <a href="{{route('contact-us', $locale)}}">
        <div class="blue-box rotate-left">
            <i class="far fa-envelope fa-lg mr-2 mt-1 text-white"></i>
            <p class="text-white">CONTACT US</p>
        </div>
    </a>
    @endif
    <a href="https://api.whatsapp.com/send?phone=+96176759999">
        <div class="whatsapp-box text-white">
            <i class="fab fa-whatsapp fa-3x"></i>
        </div>
    </a>
    @include('web.layouts.footers.footer')
    <script src=" {{ asset('assets_web/libraries/jquery-3.5.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets_web/libraries/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets_web/libraries/isotope/isotope.pkgd.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets_web/libraries/slick-1.8.1/slick/slick.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets_web/libraries/nouislider/nouislider.min.js') }}" type="text/javascript"></script>
    @stack('script')
    <script src="{{ asset('assets_web/js/main.js') }}" type="text/javascript"></script>
    {{-- Date PICKER JS--}}
    <script src="{{ asset('assets_web/js/flatpickr.js') }}" type="text/javascript"></script>


</body>

</html>
