<div class="position-relative footer pt-5">
    <div class="container custom-container p-5">
        <div class="text-center">
            <h2 class="font-bold text-blue text-italic">{{ trans('app.footer-title') }}<sup>®</sup></h2>
        </div>
        <div class="row w-100 h-100 py-5 justify-content-between">
            <div class="col-lg-2">
                <div class="">
                    <ul class="font-medium">
                        @foreach($footer_links as $key => $footer_link)
                        @if($key>=0 && $key < 3) @php $url=$footer_link->value == 'external' ? $footer_link->link : route($footer_link->link, $locale);
                            @endphp
                            <a href="{{$url}}" target="_blank">
                                <li class="my-3">
                                    <img class="mr-2 header-icon-small-size" src="{{ asset('assets_web/images/blue-right-arrow.svg') }}" alt="{{$footer_link->title}}">
                                    {{$footer_link->title}}
                                </li>
                            </a>
                            @endif
                            @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="">
                    <ul class="font-medium">
                        @foreach($footer_links as $key => $footer_link)
                        @if($key >= 3 && $key < 6) @php $url=$footer_link->value == 'external' ? $footer_link->link : route($footer_link->link, $locale);
                            @endphp
                            <a href="{{$url}}" target="_blank">
                                <li class="my-3">
                                    <img class="mr-2 header-icon-small-size" src="{{ asset('assets_web/images/blue-right-arrow.svg') }}" alt="{{$footer_link->title}}">
                                    {{$footer_link->title}}
                                </li>
                            </a>
                            @endif
                            @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="">
                    <ul class="font-medium">
                        @foreach($footer_links as $key => $footer_link)
                        @if($key >= 6 && $key < 9) @php $url=$footer_link->value == 'external' ? $footer_link->link : route($footer_link->link, $locale);
                            @endphp
                            <a href="{{$url}}" target="_blank">
                                <li class="my-3">
                                    <img class="mr-2 header-icon-small-size" src="{{ asset('assets_web/images/blue-right-arrow.svg') }}" alt="{{$footer_link->title}}">
                                    {{$footer_link->title}}
                                </li>
                            </a>
                            @endif
                            @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="">
                    <ul class="font-medium">
                        @foreach($footer_links as $key => $footer_link)
                        @if($key >= 9 && $key < 12) @php $url=$footer_link->value == 'external' ? $footer_link->link : route($footer_link->link, $locale);
                            @endphp
                            <a href="{{$url}}" target="_blank">
                                <li class="my-3">
                                    <img class="mr-2 header-icon-small-size" src="{{ asset('assets_web/images/blue-right-arrow.svg') }}" alt="{{$footer_link->title}}">
                                    {{$footer_link->title}}
                                </li>
                            </a>
                            @endif
                            @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="">
                    <ul class="font-medium">
                        @foreach($footer_links as $key => $footer_link)
                        @if($key >= 12 && $key < 15) @php $url=$footer_link->value == 'external' ? $footer_link->link : route($footer_link->link, $locale);
                            @endphp
                            <a href="{{$url}}" target="_blank">
                                <li class="my-3">
                                    <img class="mr-2 header-icon-small-size" src="{{ asset('assets_web/images/blue-right-arrow.svg') }}" alt="{{$footer_link->title}}">
                                    {{$footer_link->title}}
                                </li>
                            </a>
                            @endif
                            @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="row w-100 h-100 py-5 justify-content-between">
            <div class="col-lg-4">
                <h5 class="font-bold text-blue">Stay Up to date</h5>
                <div class="py-4">
                    <form action="{{Route('subscribe.submit', $locale)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if(session('sub_message'))
                        <div class="col-7 p-0">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('sub_message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-lg-7">
                                <input class="footer-input" id="email_address" name="subscription_email" placeholder="Email Address" type="email" />
                            </div>
                            <div class="col-lg-3 vertically-centered justify-content-start p-lg-0">
                                <button class="subscribe px-5" type="submit">SUBSCRIBE</button>
                            </div>
                        </div>
                        @if($errors->has('subscription_email'))
                        <span class="invalid-feedback text-danger d-block" role="alert">
                            <strong>{{ $errors->first('subscription_email') }}</strong>
                        </span>
                        @endif
                    </form>
                </div>
            </div>
            <div class="col-lg-2">
                <h5 class="font-bold text-blue">Follow Us</h5>
                <div class="d-inline-flex py-4">
                    @php
                    $linkedin_link = $social_media->filter(function ($value, $key) {
                    return $value['type'] == 'linkedin';
                    })->first();

                    $facebook_link = $social_media->filter(function ($value, $key) {
                    return $value['type'] == 'facebook';
                    })->first();

                    $instagram_link = $social_media->filter(function ($value, $key) {
                    return $value['type'] == 'instagram';
                    })->first();

                    $twitter_link = $social_media->filter(function ($value, $key) {
                    return $value['type'] == 'twitter';
                    })->first();

                    $youtube_link = $social_media->filter(function ($value, $key) {
                    return $value['type'] == 'youtube';
                    })->first();
                    @endphp
                    @isset($linkedin_link->link)
                        <a href="{{$linkedin_link->link}}" target="_blank">
                            <img class="mr-2 image-50" src="{{ asset('assets_web/images/linkedin.svg') }}" alt="linkedin">
                        </a>
                    @endisset
                    @isset($twitter_link->link)
                        <a href="{{$twitter_link->link}}" target="_blank">
                            <img class="mr-2 image-50" src="{{ asset('assets_web/images/twitter.svg') }}" alt="twitter">
                        </a>
                    @endisset
                    @isset($facebook_link->link)
                        <a href="{{$facebook_link->link}}" target="_blank">
                            <img class="mr-2 image-50" src="{{ asset('assets_web/images/facebook.svg') }}" alt="facebook">
                        </a>
                    @endisset
                    @isset($youtube_link->link)
                        <a href="{{$youtube_link->link}}" target="_blank">
                            <img class="mr-2 image-50" src="{{ asset('assets_web/images/youtube.svg') }}" alt="youtube">
                        </a>
                    @endisset
                </div>
            </div>
            <div class="col-lg-4">
                <h5 class="font-bold text-blue">Claims Hotlines 24/7</h5>
                <div class="row pt-4">
                    <div class="col-lg-7">
                        <ul class="font-medium">
                            @foreach($claim_details as $key => $claim_detail)
                            @if($key < 2) <li class="mb-3 d-inline-flex">
                                <p>{{$claim_detail->title}}:</p>
                                <p class="ml-3">{{$claim_detail->value}}</p>
                                </li>
                                @endif
                                @endforeach
                        </ul>
                    </div>
                    <div class="col-lg-5">
                        <ul class="font-medium">
                            @foreach($claim_details as $key => $claim_detail)
                            @if($key >= 2) <li class="mb-3 d-inline-flex">
                                <p>{{$claim_detail->title}}:</p>
                                <p class="ml-3">{{$claim_detail->value}}</p>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <ul class="font-medium">
                            <li class="d-inline-flex">
                                <p class="text-green">Open Now</p>
                                <p class="ml-3">8:00 AM - 5:00 PM</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-black">
        <div class="container custom-container">
            <div class="row text-white py-2">
                <div class="col-lg-6 text-left">
                    <div class="d-table w-100 h-100">
                        <div class="d-table-cell align-middle">
                            @php $year = date("Y"); @endphp
                            <div>AROPE@ Copyright {{$year}}. All Right Reserved</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="d-table w-100 h-100">
                        <div class="d-table-cell align-middle">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item mx-3"><a target="_blank" class="w-inline-block" href="{{route('privacy-policy', $locale)}}">Privacy Policy</a></li>
                                <li class="list-inline-item mx-3"><a target="_blank" class="w-inline-block" href="{{route('legal-notices', $locale)}}">Legal Notices</a></li>
                                <li class="list-inline-item mx-3"><a target="_blank" class="w-inline-block" href="{{route('disclaimers', $locale)}}">Disclaimers</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
