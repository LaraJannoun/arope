    <div class="header-row-top">
        <nav class="navbar navbar-expand-md navbar-light bg-light-grey">
            <div class="container-fluid py-md-1">
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <p class="text-blue">Open Now</p>
                        </li>
                        <li class="nav-item d-inline-flex">
                            <p class="mx-4">8:00 AM - 5:00 PM</p>
                        </li>
                    </ul>
                </div>
                <div class="mx-auto order-0 d-md-none d-block">
                    <ul class="d-inline-flex">
                        <li class="nav-item active">
                            <p class="text-blue">Open Now</p>
                        </li>
                        <li class="nav-item d-inline-flex mr-5 align-items-baseline">
                            <p class="mx-2">8:00 AM - 5:00 PM</p>
                        </li>
                        <form action="{{Route('search.submit', $locale)}}" method="GET" enctype="multipart/form-data">
                            <li class="nav-item d-inline-flex mr-2 align-items-baseline">
                                <img class="header-icon-small-size" src="{{ asset('assets_web/images/search.svg') }}">
                                <input class="search-input text-blue mr-5 ml-3" type="search" placeholder="SEARCH" name="search">
                            </li>
                        </form>
                        <li class="nav-item d-inline-flex align-items-baseline">
                            <img class="header-icon-small-size" src="{{ asset('assets_web/images/globe.svg') }}">
                            <select id="mobile-language-select" data-placeholder="{{$locale}}" class="language-select text-blue mx-1">
                                <option value="{{route('home', 'en')}}" class="text-blue mx-3" @if($locale=='en' ) selected @endif>ENG</option>
                                <option value="{{route('home', 'ar')}}" class="text-blue mx-3" @if($locale=='ar' ) selected @endif>AR</option>
                            </select>
                        </li>
                    </ul>
                </div>
                <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                    <ul class="navbar-nav ml-auto">
                        @if($login_external_link->link != '')
                        <a href="{{$login_external_link->link}}" target="_blank">
                            <li class="nav-item d-inline-flex">
                                <img class="header-icon-size" src="{{ asset('assets_web/images/login.svg') }}">
                                <p class="text-blue mr-5 ml-3">LOG IN</p>
                            </li>
                        </a>
                        @endif
                        <form action="{{Route('search.submit', $locale)}}" method="GET" enctype="multipart/form-data">
                            <li class="nav-item d-inline-flex">
                                <img type="submit" class="header-icon-size" src="{{ asset('assets_web/images/search.svg') }}">
                                <input value="{{old('search')}}" id="search-input" autocomplete="off" onchange="batata()" onkeypress="this.style.width = ((this.value.length + 1) * 8) + 'px';" class="search-input text-blue mr-2 ml-2" type="search" placeholder="SEARCH" name="search">
                            </li>
                        </form>
                        <li class="nav-item d-inline-flex">
                            <img class="header-icon-size" src="{{ asset('assets_web/images/globe.svg') }}">
                            <select id="language-select" data-placeholder="{{$locale}}" class="language-select text-blue mx-3">
                                <option value="{{route('home', 'en')}}" class="text-blue mx-3" @if($locale=='en' ) selected @endif>ENG</option>
                                <option value="{{route('home', 'ar')}}" class="text-blue mx-3" @if($locale=='ar' ) selected @endif>AR</option>
                            </select>
                        </li>
                    </ul>
                </div>
            </div>

        </nav>
    </div>
    <div class="header-row background-white">
        <nav class="navbar navbar-expand-lg navbar-light background-white">
            <div class="container-fluid py-3">
                <div class="d-flex flex-grow-1">
                    <span class="w-100 d-lg-none d-block">
                        <!-- hidden spacer to center brand on mobile --></span>
                    <a class="navbar-brand" href="{{route('home', $locale)}}">
                        <img class="logo-width" src="{{ asset('assets_web/images/logo.svg') }}" alt="{{ env('APP_NAME') }}">
                    </a>
                    <div class="w-100 text-right">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#secondnav">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>
                <div class="collapse navbar-collapse flex-grow-1 text-right" id="secondnav">
                    <ul class="navbar-nav ml-auto flex-nowrap">
                        <li class="nav-item">
                            <a href="{{route('solutions', $locale)}}" class="nav-link px-0">
                                <button class="dark-blue-button">PERSONAL</button>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('solutions-business', $locale)}}" class="nav-link px-0">
                                <button class="blue-button">BUSINESS</button>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    @push('script')
    <script>
        $(function() {
            // bind change event to select
            $('#language-select').on('change', function() {
                var url = $(this).val(); // get selected value
                if (url) { // require a URL
                    window.location = url;
                }
                return false;
            });
            $('#mobile-language-select').on('change', function() {
                var url = $(this).val(); // get selected value
                if (url) { // require a URL
                    window.location = url;
                }
                return false;
            });
        });
    </script>
    @endpush