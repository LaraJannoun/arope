<div class="row pb-2 d-flex">
    <div class="col-lg-6 px-4">
        <div class="slider-images-item-container pb-4">
            @if(count($images) > 0)
            @foreach($images as $key => $image)
            <img class="rounded-corners" src="{{ asset($image->image) }}" alt="image">
            @endforeach
            @else
            <img class="rounded-corners" src="{{ asset($main_image) }}" alt="image">
            @endif
        </div>
    </div>
    <div class="col-lg-5 px-4 my-auto">
        <div class="w-100 h-100">
            <h1 class="text-blue py-3 text-uppercase">
                {{$image_main_title}}
            </h1>
            @if($image_subtitle != '')
            <h3 class="py-3">
                {{$image_subtitle}}
            </h3>
            @endif
            @if(str_contains($image_text, '%%block%%'))
            <div class="font-medium py-3 text-custom-color">
                {!! preg_split("/%%block%%/i", $image_text)[0] !!}
            </div>
            <div id="more" style="display:none;" class="font-medium py-3 text-custom-color">
                {!! preg_split("/%%block%%/i", $image_text)[1] !!}
            </div>
            <span id="dots">...</span>
            <div class="my-4">
                <p onclick="readmoreless()" id="myBtn" class="style-3">Read More</p>
            </div>
            @else
            <div class="font-medium py-3 text-custom-color">
                {!! $image_text !!}
            </div>
            @endif
            @if($page == 'solutions' && $slug != 'takaful')
            <div class="row pt-4">
                <div class="col-lg-6 pr-2">
                    <a href="{{ route($apply_link, $locale) }}">
                        <button class="white-button hvr-grow font-medium w-100">REQUEST A QUOTE</button>
                    </a>
                </div>
                <div class="col-lg-6 pl-2">
                    <a href="">
                        <button class="dark-blue-button hvr-grow font-medium w-100">GET POLICY</button>
                    </a>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@push('script')
<script>
    function readmoreless() {
        var dots = document.getElementById("dots");
        var moreText = document.getElementById("more");
        var btnText = document.getElementById("myBtn");

        if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "Read more";
            moreText.style.display = "none";
        } else {
            dots.style.display = "none";
            btnText.innerHTML = "Read less";
            moreText.style.display = "inline";
        }
    }
</script>
@endpush('script')
