 <div>
     <div class="py-4">
         <img class="contain" src="{{asset($image_source)}}" alt="{{$image_title}}"></img>
     </div>
     <div class="float-left mr-2 px-3 pb-2">
         <p class="font-bold py-2">{{$image_date}}</p>
         <h5 class="font-bold text-blue py-1">{{$image_title}}</h5>
         <p class="py-1">{{$image_description}}</p>
     </div>
 </div>