<div class="col-lg text-center @if($image_order!= 1) margin-left-minus @endif" style="z-index:{{$z_index}}">
    @php
    $url = 'assets_web/images/arrow.svg';
    $textColorClass = 'text-white';

    $zVal = 10 - intval($image_order);
    if($image_order==1) {
    $url = 'assets_web/images/activeArrow.svg';
    $textColorClass= 'text-white';
    }
    else {
    $url = 'assets_web/images/Arrow.svg';
    $textColorClass= 'text-blue';
    }
    @endphp

    <div id="{{$title}}" class="position-relative" onclick="{{$onclick}}()">
        <img class="arrow-active-drop-shadow" src="{{asset($url)}}" alt="{{$title}}">
        <h5 class="w-100 centered {{$textColorClass}}">{{$text}}</h5>
    </div>
</div>