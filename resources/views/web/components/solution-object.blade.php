<div class=" col-lg-3 py-3 @if($page=='claim' ) min-height-250 @else min-height-350 @endif">
    @if($page =='solutions' || $page =='home')
    <a href="{{route($section.'-solution-item-details', [$locale, $link])}}@if(isset($page_section->header_title))?page_section={{$page_section->header_title}}@else ?page_section={{$page}} @endif">
        @endif
        <div class="@if($page !='home') shadow-outer-lightgrey @endif background-white p-5 @if($page =='claim') min-height-250 @else min-height-350 @endif w-100 h-100">
            @if(isset($image_exists) && $image_exists == 'true')
            <img class="image-50" src="{{ asset($image_source) }}" alt="{{$image_main_title}}">
            @endif
            <h5 class="@if($page =='claim') pb-4 @else py-4 @endif text-blue font-bold">{{$image_main_title}}</h5>
            <p class="font-medium font-13">{!!$image_text!!}</p>
            @if($page != 'claim' && $link != 'takaful')
            <div class="@if($page =='claim') to-bottom-0 py-5 @else pt-5 pb-4 @endif">
                <a href="{{Route($apply_link, $locale)}}" class="d-inline-flex hvr-grow">
                    @php
                    $text = 'APPLY NOW';

                    if(isset($button_text))
                    $text = $button_text;
                    @endphp
                    <p class="text-blue text-uppercase ">{{$text}}</p>
                    <img class="ml-3 header-icon-small-size" src="{{ asset('assets_web/images/right-arrow-grey.svg') }}" alt="click">
                </a>
            </div>
            @endif
        </div>
        @if($page =='solution')
    </a>
    @endif
</div>
