<div class="col-lg-4 px-2">
    <div class="rounded-border text-center py-5">
        <div class="picture-wrapper padding-top-40">
            <img class="contain pb-4" src="{{ asset($image_source) }}" alt="{{$image_main_title}}">
        </div>
        <h5 class="py-1 text-blue">{{$image_main_title}}</h5>
        <h4 class="pb-4 font-light">{{$image_main_subtitle}}</h4>
        <div class="text-left">
            @php
            $full= explode(':', $image_text);
            $first= $full[0];
            $rest= ltrim($image_text, $first);
            @endphp
            <p class="py-1 px-5 font-book font-16"><span class="text-second-blue">{{$first}}</span>{{$rest}}</p>
            @php
            $full2= explode(':', $image_text_2);
            $first2= $full2[0];
            $rest2= ltrim($image_text_2, $first2);
            @endphp
            <p class="pt-3 px-5 font-book font-16"><span class="text-second-blue">{{$first2}}</span>{{$rest2}}</p>
        </div>
        <div class="pt-5">
            <a href="" class="d-inline-flex">
                <p class="text-blue font-16">APPLY NOW</p>
                <img class="ml-3 header-icon-size" src="{{ asset('assets_web/images/right-arrow-grey.svg') }}" alt="apply">
            </a>
        </div>
    </div>
</div>