<div data-title="{{$image_main_title}}" class="slider-object" data-title={{$image_main_title}}>
    <img class="slider-image" src="{{ asset($image_source) }}" alt="{{$image_main_title}}" />
    <div class="left-arrow"></div>
    <div class="right-arrow"></div>
    <div class="text-left centered-left slider-text">
        <div class="p-0 my-4">
            <h1 class="font-bold font-xlarge text-uppercase">{{$image_main_title}}</h1>
        </div>
        @if($image_order == 0)
        <div class="p-0 my-4">
            <a href="{{route('about-us', $locale)}}">
                <button class="max-width-200 primary-button">LEARN MORE</button>
            </a>
        </div>
        @endif
    </div>
</div>
