<div class="mx-3">
    <div class="row pb-5">
        <div class="col-lg-6">
            <div class="text-center centered">
                <h4 class="font-bold text-center">{{$image_title}}</h4>
                <p class="mt-3 text-center">{{$image_description}}</p>
            </div>
        </div>
        <div class="col-lg-6">
            <img class="contain" src="{{asset( $image_source )}}" alt="{{$image_title}}" />
        </div>
    </div>
</div>