<div class="select ">
    <select class=" {{isset($class) ? $class : ''}}" name="{{$type}}" id="{{$type}}_id">
        <option value="">{{$placeholder}}</option>
        @if(isset($data) && $data != '')
        @foreach($data as $key => $v)
        <option @isset($value) @if($v->display_title == $value) selected @endif @endisset value="{{$v->display_title}}">{{$v->display_title}}</option>
        @endforeach
        @endif
    </select>
</div>
