<div class="col-lg-3 px-3 font-medium">
    <div class="background-white shadow-outer-lightgrey text-center py-5 min-heigh-250 h-100">
        <div>
            <img class="custom-career-width" src="{{ asset($image_source) }}" alt="{{$image_main_title}}">
        </div>
        <h3 class="text-darkgrey py-4">{{$image_main_title}}</h3>
        <div class="@if(isset($page) && $page =='pay') px-5 @else px-5 @endif text-custom-color">{!! $image_text!!}</div>
    </div>
</div>