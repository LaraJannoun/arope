<div class="col-lg-4 py-2">
    <div class="background-white shadow-outer-lightgrey text-center pt-5 pb-2 min-height-370">
        <img class="image-width" src="{{ asset($image_source) }}" alt="{{$image_main_title}}">
        <div class="row text-center justify-content-center">
            <div class="col-lg-7">
                @php
                $full= preg_split("/ /i", $image_main_title);
                $first= $full[0];
                $rest= ltrim($image_main_title, $first);

                @endphp
                <h5 class="pt-4 font-medium first-word"><span class="text-blue">{{$first}} </span>{{$rest}}</h5>
            </div>

            <div class="col-lg-10">
                <div class="pt-4 font-medium text-custom-color">{!!$image_text!!}</div>
            </div>
        </div>
    </div>
</div>