<div class="mx-3 my-4 text-left">
    <div class="picture-wrapper padding-top-40">
        <img class="contain" src="{{asset( $image_source )}}" alt="{{$image_title}}" />
    </div>
    <div class="px-5 my-5">
        <h4 class="my-2 font-bold text-center">{{$image_title}}</h4>
        <a href="">
            <button class="primary-button my-3 w-100">Place Order</button>
        </a>
    </div>
</div>