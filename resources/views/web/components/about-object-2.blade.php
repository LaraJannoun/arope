<div class="col-lg-3 py-4">
    <div class="background-white shadow-outer-lightgrey text-center py-5 min-height-320 text-custom-color">
        <img class="image-width" src="{{ asset($image_source) }}" alt="{{$image_main_title}}">
        <h5 class="py-4 font-bold">{{$image_main_title}}</h5>
        <div class="font-medium px-3">{!!$image_text!!}</div>
    </div>
</div>