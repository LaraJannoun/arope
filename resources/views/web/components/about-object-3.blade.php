<div class="horz-time background-white shadow-outer-lightgrey text-center py-4 min-height-250">
    @if($type =='down')
    <time>
        <div class="time-icon d-inline-flex text-center">
            <div style="background-image:url({{asset($image_source)}} );background-size: contain;min-width:100px;height:auto;" class=" w-100 banner-background p-2">
                <p class="text-white font-bold mt-2">{{$date}}</p>
            </div>
        </div>
    </time>
    @endif
    <div class="row text-center justify-content-center">
        <div class="col-lg-11">
            <h3 class="py-4 font-medium text-custom-color">{{$image_main_title}}</h3>
            <div class="font-medium text-custom-color">{!!$image_text!!}</div>
        </div>
    </div>
    @if($type == 'up')
    <time>
        <div class="centered-bottom time-icon d-inline-flex text-center">
            <div style="background-image:url({{asset($image_source)}} );background-size: contain;min-width:100px;height:auto;" class=" w-100 banner-background p-2 ">
                <p class="text-white font-bold px-2 py-2">{{$date}}</p>
            </div>
        </div>
    </time>
    @endif

</div>