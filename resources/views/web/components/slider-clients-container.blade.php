<div class="slider-object-small">
    <div class="position-relative padding-10">
        <img class="slider-image-small" src="{{ asset($image_source) }}" alt="{{$image_main_title}}" />
    </div>
</div>
