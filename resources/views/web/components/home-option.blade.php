<div class="col-md-2 col-sm-3 cursor-pointer">
    <a  class="hvr-grow"
    @if(strpos($image_main_title, 'CONTACT')  !== 0)
      href="{{route($link, $locale)}}"
    @endif
    >
        <button class="primary-button w-100" style="background-color: {{$box_color}}; border: 1px solid {{$box_color}}"
        @if(strpos($image_main_title, 'CONTACT')  === 0) type="button" data-toggle="modal" data-target="#exampleModal" @endif
        >
            <img class="icon-medium my-4" src="{{ asset($image_source) }}" alt="{{$image_main_title}}">
            <p class="px-lg-4">{{$image_main_title}} </p>
        </button>
    </a>
</div>


