    <div id="{{$page_section->slug}}" class="container custom-container px-5 show-hide-div @if($key != 0) d-none @endif">
        <div class="row pb-5">
            @php $solutions = $all_solutions->filter(function ($value, $key) use($page_details){
            return $value['page_section_id'] == $page_details->id;
            });
            @endphp
            @foreach($solutions as $key => $solution)
            @include('web.components.solution-object', ['section' => $page, 'page'=>'solutions', 'apply_link' => $solution->apply_link, 'link' => $solution->link, 'page_section' => $page_section, 'image_order' => $key, 'image_exists' => 'true', 'image_source'=> $solution->image, 'image_main_title'=> $solution->title, 'image_text'=> $solution->text])
            @endforeach
        </div>
        <div class="py-3 d-inline-block w-100">
            <div class="float-left">
                <h2 class="text-uppercase text-blue">Most Buy Insurances</h2>
            </div>
        </div>
        <div class="row w-100 h-100">
            @php $featured_items = $items->filter(function ($value, $key) use($page_details){
            return $value['featured'] == 1 && $value['page_section_id'] == $page_details->id;
            });
            @endphp
            @foreach($featured_items as $key => $featured_item)
            <div class="col-lg-4">
                <div class="picture-wrapper padding-top-100">
                    <img class="contain" src="{{ asset($featured_item->image) }}" alt="{{$featured_item->title}}">
                </div>

                <div class="p-5 to-bottom-0 text-white">
                    <h5 class="py-3 font-bold">{{$featured_item->title}}</h5>
                    <p>{!!$featured_item->text_preview!!}</p>
                    <a href="{{route($page.'-solution-item-details', [$locale, $featured_item->slug])}}?page_section={{$page_sections[0]->header_title}}">
                        <div class="float-right arrow-circle-bottom-right-solutions">
                            <img class="icon-small" src="{{ asset('assets_web/images/circle-arrow-right.svg') }}" alt="go">
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>


    @push('script')
    <script>
        $('.option-title').click(function() {
            var slug = $(this).attr("slug");
            var image_src = $(this).attr("image_src");

            $(".show-hide-div").addClass("d-none");
            $("#" + slug).removeClass("d-none");

            $(".options").removeClass("active");
            $(this).find("h5").addClass("active");

            $("#solutions-banner").css('background-image', "url(" + image_src + ")");
        });
    </script>
    @endpush