<div class="form-group">
	@if(isset($label))
	<label class="form-control-label">{{ $label }} @include('cms.components.inputs.asterix')</label>
	@endif
	@if(isset($hint))
	<div class="mb-2">
		<small><em>{{ $hint }}</em></small>
	</div>
	@endif
	<select class="select2-custom form-control" name="{{ $name }}">
		<option {{ (isset($row->$name) && $row->$name) ? '' : 'selected' }} disabled>{{ $placeholder }}</option>
		@foreach($rows as $select_row)
		<option value="{{ $select_row['id'] }}" @if(old($name)==$select_row['id'] || isset($row->$name) && $row->$name == $select_row['id']) selected @endif>
			@if(isset($showcurrentid) && isset($select_row['id'])) {{$select_row['id']}}: @endif
			@if(isset($select_row['header_title_en']))
			{{ $select_row['header_title_en'] }}
			@else
			{{$select_row['title_en']}}
			@endif
			@if(isset($showparentid) && isset($select_row['page_section_id'])) / {{$select_row['page_section_id']}} @endif
		</option>
		@endforeach
	</select>

	@include('cms.components.inputs.error')
</div>