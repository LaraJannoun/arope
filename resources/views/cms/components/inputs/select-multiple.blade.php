<div class="form-group">
	<label class="form-control-label">{{ $label }} @include('cms.components.inputs.asterix')</label>
	<select class="select2-custom form-control form-custom" name="{{ $name }}[]" multiple="">
		@if(isset($itemEffects) && count($itemEffects) > 0)
			@foreach($rows as $select_row)
				@foreach($itemEffects as $itemEffect)
				<option value="{{ $select_row['id'] }}" @if(old($name)==$select_row['id'] || isset($row->$name) && $row->$name == $select_row['id'] || ($itemEffect->effect_id == $select_row['id'])) selected @endif>{{ $select_row['title'] }}</option>
				@endforeach
			@endforeach
		@else
			@foreach($rows as $select_row)
			<option value="{{ $select_row['id'] }}" @if(old($name)==$select_row['id'] || isset($row->$name) && $row->$name == $select_row['id']) selected @endif>{{ $select_row['title'] }}</option>
			@endforeach
		@endif
	</select>

	@include('cms.components.inputs.error')
</div>