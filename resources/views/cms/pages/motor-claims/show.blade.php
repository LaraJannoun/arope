@extends('cms.layouts.main')

@section('content')
@include('cms.layouts.headers.partials', ['title' => 'View Record'])

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">{{ $page_info['title'] }}</h3>
                        </div>
                        <div class="col-auto text-right">
                            <a href="{{ route('admin.'.$page_info['link'].'.index') }}" class="btn btn-sm btn-primary">Back to list</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled">
                        <li>
                            <h4>{{strtr('pas_number', '_', ' ') }}</h4>
                            <p>{{ $row->pas_number }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('plate_number', '_', ' ') }}</h4>
                            <p>{{ $row->plate_number }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('plate_code', '_', ' ') }}</h4>
                            <p>{{ $row->plate_code }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('driver_name', '_', ' ') }}</h4>
                            <p>{{ $row->driver_name }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('mobile_number', '_', ' ') }}</h4>
                            <p>{{ $row->mobile_number }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('email', '_', ' ') }}</h4>
                            <p>{{ $row->email }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('communication', '_', ' ') }}</h4>
                            <p>{{ $row->communication }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('accident_date', '_', ' ') }}</h4>
                            <p>{{ $row->accident_date }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('accident_time', '_', ' ') }}</h4>
                            <p>{{ $row->accident_time }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('accident_location', '_', ' ') }}</h4>
                            <p>{{ $row->accident_location }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('expert', '_', ' ') }}</h4>
                            <p>{{ $row->expert }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('expert_name', '_', ' ') }}</h4>
                            <p>{{ $row->expert_name }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('bodily_injuries', '_', ' ') }}</h4>
                            <p>{{ $row->bodily_injuries }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('accident_description', '_', ' ') }}</h4>
                            <p>{{ $row->accident_description }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('front_driving_license', '_', ' ') }}</h4>
                            <img width="400" src="@if($row->front_driving_license == '') {{ asset('assets_cms/images/no-image.png') }} @else {{ asset($row->front_driving_license) }} @endif" class="img-thumbnail">
                        </li>
                        <li>
                            <h4>{{strtr('back_driving_license', '_', ' ') }}</h4>
                            <img width="400" src="@if($row->back_driving_license == '') {{ asset('assets_cms/images/no-image.png') }} @else {{ asset($row->back_driving_license) }} @endif" class="img-thumbnail">
                        </li>
                        <li>
                            <h4>{{strtr('front_car_reg_id', '_', ' ') }}</h4>
                            <img width="400" src="@if($row->front_car_reg_id == '') {{ asset('assets_cms/images/no-image.png') }} @else {{ asset($row->front_car_reg_id) }} @endif" class="img-thumbnail">
                        </li>
                        <li>
                            <h4>{{strtr('back_car_reg_id', '_', ' ') }}</h4>
                            <img width="400" src="@if($row->back_car_reg_id == '') {{ asset('assets_cms/images/no-image.png') }} @else {{ asset($row->back_car_reg_id) }} @endif" class="img-thumbnail">
                        </li>
                        <li>
                            <h4>{{strtr('front_car_details', '_', ' ') }}</h4>
                            <p>{{ $row->front_car_details }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('back_car_details', '_', ' ') }}</h4>
                            <p>{{ $row->back_car_details }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('left_car_details', '_', ' ') }}</h4>
                            <p>{{ $row->left_car_details }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('right_car_details', '_', ' ') }}</h4>
                            <p>{{ $row->right_car_details }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('workshop', '_', ' ') }}</h4>
                            <p>{{ $row->workshop }}</p>
                        </li>
                        <li>
                            <h4>{{strtr('data_privacy_confirm', '_', ' ') }}</h4>
                            <p>{{ $row->data_privacy_confirm }}</p>
                        </li>
                        <hr class="my-4">
                        <li>
                            <h4>Created at</h4>
                            <p>{{ date('d M Y - h:i A', strtotime($row->created_at)) }}</p>
                        </li>
                        <hr class="my-4">
                        <li>
                            <h4>Updated at</h4>
                            <p>{{ date('d M Y - h:i A', strtotime($row->updated_at)) }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('cms.layouts.footers.auth')
</div>
@endsection