@extends('cms.layouts.main')

@section('content')
@include('cms.layouts.headers.cards')

<div class="container-fluid mt--7">
    <div class="row mt-5">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">{{ $page_info['title'] }}</h3>
                        </div>
                        <div class="col-auto text-right">
                            <p onclick="downloadDrivingLicences()" class="my-0 btn btn-sm btn-info">Download Driving Licenses</p>
                            <p onclick="downloadCarRegs()" class="my-0 btn btn-sm btn-info">Download Car Registration IDs</p>
                            <a href="{{ route('admin.'.$page_info['link'].'.export') }}" class="btn btn-sm btn-success">Export Data</a>
                        </div>
                    </div>
                </div>

                @if(session('status'))
                <div class="col-12">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                @endif

                <table class="table align-items-center table-flush datatable">

                    <thead class="thead-light">
                        <tr>
                            <th scope="col" class="no-sort">{{strtr('pas_number', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('plate_number', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('plate_code', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('driver_name', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('mobile_number', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('email', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('communication', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('accident_date', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('accident_time', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('accident_location', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('expert', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('expert_name', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('bodily_injuries', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('accident_description', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('front_driving_license', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('back_driving_license', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('front_car_reg_id', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('back_car_reg_id', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('front_car_details', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('back_car_details', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('left_car_details', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('right_car_details', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('workshop', '_', ' ') }}</th>
                            <th scope="col" class="no-sort">{{strtr('data_privacy_confirm', '_', ' ') }}</th>

                            <th scope="col" class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($rows as $row)
                        <tr>
                            <td>{{ $row->pas_number }}</td>
                            <td>{{ $row->plate_number }}</td>
                            <td>{{ $row->plate_code }}</td>
                            <td>{{ $row->driver_name }}</td>
                            <td>{{ $row->mobile_number }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->communication }}</td>
                            <td>{{ $row->accident_date }}</td>
                            <td>{{ $row->accident_time }}</td>
                            <td>{{ $row->accident_location }}</td>
                            <td>{{ $row->expert }}</td>
                            <td>{{ $row->expert_name }}</td>
                            <td>{{ $row->bodily_injuries }}</td>
                            <td>{{ $row->accident_description }}</td>
                            @php
                            $info = pathinfo($row->front_driving_license);
                            $ispdf= false;
                            if (isset($info["extension"]) && $info["extension"] == "pdf") {
                            $ispdf = true;
                            }
                            @endphp
                            <td>
                                <a href="{{ asset($row->front_driving_license) }}" download>
                                    <img src="@if ($ispdf == true)  {{ asset('assets_cms/images/pdf.png') }} @else  {{ asset($row->front_driving_license) }} @endif" class="contain img-thumbnail">
                                </a>
                            </td>
                            @php
                            $info = pathinfo($row->back_driving_license);
                            $ispdf= false;
                            if (isset($info["extension"]) && $info["extension"] == "pdf") {
                            $ispdf = true;
                            }
                            @endphp
                            <td>
                                <a href="{{ asset($row->back_driving_license) }}" download>
                                    <img src="@if ($ispdf == true)  {{ asset('assets_cms/images/pdf.png') }} @else  {{ asset($row->back_driving_license) }} @endif" class="contain img-thumbnail">
                                </a>
                            </td>
                            @php
                            $info = pathinfo($row->front_car_reg_id);
                            $ispdf= false;
                            if (isset($info["extension"]) && $info["extension"] == "pdf") {
                            $ispdf = true;
                            }
                            @endphp
                            <td>
                                <a href="{{ asset($row->front_car_reg_id) }}" download>
                                    <img src="@if ($ispdf == true)  {{ asset('assets_cms/images/pdf.png') }} @else  {{ asset($row->front_car_reg_id) }} @endif" class="contain img-thumbnail">
                                </a>
                            </td>
                            @php
                            $info = pathinfo($row->back_car_reg_id);
                            $ispdf= false;
                            if (isset($info["extension"]) && $info["extension"] == "pdf") {
                            $ispdf = true;
                            }
                            @endphp
                            <td>
                                <a href="{{ asset($row->back_car_reg_id) }}" download>
                                    <img src="@if ($ispdf == true)  {{ asset('assets_cms/images/pdf.png') }} @else  {{ asset($row->back_car_reg_id) }} @endif" class="contain img-thumbnail">
                                </a>
                            </td>
                            <td>{{ $row->front_car_details }}</td>
                            <td>{{ $row->back_car_details }}</td>
                            <td>{{ $row->left_car_details }}</td>
                            <td>{{ $row->right_car_details }}</td>
                            <td>{{ $row->workshop }}</td>
                            <td>{{ $row->data_privacy_confirm }}</td>

                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="{{ route('admin.'.$page_info['link'].'.show', $row) }}">View</a>
                                        <!-- <a class="dropdown-item" href="{{ route('admin.'.$page_info['link'].'.edit', $row) }}">Edit</a> -->
                                        <form action="{{ route('admin.'.$page_info['link'].'.destroy', $row) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <button type="button" class="dropdown-item" onclick="confirm('Are you sure you want to delete this record?') ? this.parentElement.submit() : ''">
                                                Delete
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('cms.layouts.footers.auth')
</div>
@endsection
@push('script')
<script type="text/javascript">
    var rows = {
        !!json_encode($rows) !!
    };

    function downloadDrivingLicences() {
        for (var i = 0; i < rows.length; i++) {
            var a = document.createElement("a");
            a.href = '{{env('
            APP_URL ')}}' + rows[i]['front_driving_license'];
            a.setAttribute("download", "Front Driving License-" + rows[i]['pas_number'] + "-" + rows[i]['plate_number']);
            a.click();
        }
        for (var i = 0; i < rows.length; i++) {
            var a = document.createElement("a");
            a.href = '{{env('
            APP_URL ')}}' + rows[i]['back_driving_license'];
            a.setAttribute("download", "Back Driving License-" + rows[i]['pas_number'] + "-" + rows[i]['plate_number']);
            a.click();
        }
    }

    function downloadCarRegs() {
        for (var i = 0; i < rows.length; i++) {
            var a = document.createElement("a");
            a.href = '{{env('
            APP_URL ')}}' + rows[i]['front_car_reg_id'];
            a.setAttribute("download", "Front Car Reg ID-" + rows[i]['pas_number'] + "-" + rows[i]['plate_number']);
            a.click();
        }
        for (var i = 0; i < rows.length; i++) {
            var a = document.createElement("a");
            a.href = '{{env('
            APP_URL ')}}' + rows[i]['back_car_reg_id'];
            a.setAttribute("download", "Back Car Reg ID-" + rows[i]['pas_number'] + "-" + rows[i]['plate_number']);
            a.click();
        }
    }
</script>
@endpush('script')