@extends('cms.layouts.main')

@section('content')
@include('cms.layouts.headers.partials', ['title' => 'Edit Record'])

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">{{ $page_info['title'] }}</h3>
                        </div>
                        <div class="col-auto text-right">
                            <a href="{{ route('admin.'.$page_info['link'].'.index') }}" class="btn btn-sm btn-primary">Back to list</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('admin.'.$page_info['link'].'.update', $row) }}" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        @method('put')
                        <div class="px-lg-4">

                            {{-- English Title --}}
                            @include('cms.components.inputs.text', ['label' => 'English Title', 'asterix' => true, 'name' => 'title_en'])

                            {{-- Arabic Title --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic Title', 'asterix' => true, 'name' => 'title_ar'])

                            {{-- English Text --}}
                            @include('cms.components.inputs.textarea', ['label' => 'English Text', 'asterix' => true, 'name' => 'text_en', 'quill' => true])

                            {{-- Arabic Text --}}
                            @include('cms.components.inputs.textarea', ['label' => 'Arabic Text', 'asterix' => true, 'name' => 'text_ar', 'quill' => true])

                            {{-- English Year --}}
                            @include('cms.components.inputs.text', ['label' => 'English Year', 'asterix' => true, 'name' => 'year_en'])

                            {{-- Arabic Year --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic Year', 'asterix' => true, 'name' => 'year_ar'])

                            {{-- Type --}}
                            @include('cms.components.inputs.text', ['label' => 'Type', 'asterix' => true, 'note' => 'should be vertical or horizontal', 'name' => 'type'])

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('cms.layouts.footers.auth')
</div>
@endsection