@extends('cms.layouts.main')

@section('content')
@include('cms.layouts.headers.cards')

<div class="container-fluid mt--7">
	<div class="card text-center shadow overflow-hidden mb-5">
		<div class="card-header border-0">
			<h1 class="mb-0">Welcome <b>{{ Auth::guard('admin')->user()->first_name }}</b> to {{ env('APP_NAME') }} dashboard</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-4 mb-4">
			<div class="card card-stats h-100">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-title text-uppercase text-muted mb-0">Published Solutions</h5>
							<span class="h2 font-weight-bold mb-0">{{ $items_published }}</span>
						</div>
						<div class="col-auto">
							<a href="{{ route('admin.business-solutions.index') }}">
								<div class="icon icon-shape bg-primary text-white rounded-circle shadow">
									<i class="fas fa-newspaper"></i>
								</div>
							</a>
						</div>
					</div>
					@if($items_published < 10) <p class="mt-3 mb-0 text-muted text-sm">
						<span class="text-nowrap">Keep Going!</span>
						</p>
						@elseif($items_published >= 10 && $items_published < 25) <p class="mt-3 mb-0 text-muted text-sm">
							<span class="text-nowrap">Great! Don't stop.</span>
							</p>
							@elseif($items_published >= 25)
							<p class="mt-3 mb-0 text-muted text-sm">
								<span class="text-nowrap">Excellent Work!</span>
							</p>
							@endif
				</div>
			</div>
		</div>
		<div class="col-lg-4 mb-4">
			<div class="card card-stats h-100">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-title text-uppercase text-muted mb-0">Unpublished Solutions</h5>
							<span class="h2 font-weight-bold mb-0">{{ $items_unpublished }}</span>
						</div>
						<div class="col-auto">
							<a href="{{ route('admin.business-solutions.index') }}">
								<div class="icon icon-shape bg-danger text-white rounded-circle shadow">
									<i class="fas fa-newspaper"></i>
								</div>
							</a>
						</div>
					</div>
					@if($items_unpublished == 0)
					<p class="mt-3 mb-0 text-muted text-sm">
						<span class="text-nowrap">Great Job!</span>
					</p>
					@elseif($items_published >= 1)
					<p class="mt-3 mb-0 text-muted text-sm">
						<span class="text-nowrap">Attention you have unpublished works!</span>
					</p>
					@endif
				</div>
			</div>
		</div>

		<div class="col-lg-8 mb-4">
			<div class="card card-stats h-100">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-title text-uppercase text-muted mb-3">Logs</h5>
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th scope="col">Admin</th>
											<th scope="col">Action</th>
											<th scope="col">Section</th>
											<th scope="col">At</th>
										</tr>
									</thead>
									<tbody>
										@foreach($logs as $key => $log)
										<tr>
											<td>{{$log->Admin->first_name}}</td>
											<td>{{$log->action}}</td>
											<td>{{$log->section}}</td>
											<td>{{$log->created_at}}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-auto">
							<div class="icon icon-shape bg-danger text-white rounded-circle shadow">
								<i class="fas fa-newspaper"></i>
							</div>
						</div>
					</div>
					<p class="mt-3 mb-0 text-muted text-sm">
						<span class="text-nowrap">Latest Logs Here</span>
					</p>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card">
				<div class="card-header border-0">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0">Forms & Submissions</h3>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table align-items-center table-flush">
						<thead class="thead-light">
							<tr>
								<th scope="col">Forms</th>
								<th class="text-center" scope="col">Number of Submissions</th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">Contact Form</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $contact_form_count }}</span></td>
								<td><a href="{{ route('admin.contact-requests.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Subscribers</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $subscriber_count }}</span></td>
								<td><a href="{{ route('admin.subscribers.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Career Applications</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $career_form_count }}</span></td>
								<td><a href="{{ route('admin.career-applications.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Claims</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $claim_count }}</span></td>
								<td><a href="{{ route('admin.claims.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Motor Claims</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $motor_claim_count }}</span></td>
								<td><a href="{{ route('admin.motor-claims.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Problems</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $problem_count }}</span></td>
								<td><a href="{{ route('admin.problems.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Business Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $business_quote_count }}</span></td>
								<td><a href="{{ route('admin.quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Personal Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $personal_quote_count }}</span></td>
								<td><a href="{{ route('admin.personal-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Travel Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $travel_quote_count }}</span></td>
								<td><a href="{{ route('admin.travel-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Expat Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $expat_quote_count }}</span></td>
								<td><a href="{{ route('admin.expat-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Term Life Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $term_life_quote_count }}</span></td>
								<td><a href="{{ route('admin.term-life-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Healthcare Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $healthcare_quote_count }}</span></td>
								<td><a href="{{ route('admin.healthcare-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Household Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $household_quote_count }}</span></td>
								<td><a href="{{ route('admin.household-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Education Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $education_quote_count }}</span></td>
								<td><a href="{{ route('admin.education-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Retirement Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $retirement_quote_count }}</span></td>
								<td><a href="{{ route('admin.retirement-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Motor Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $motor_quote_count }}</span></td>
								<td><a href="{{ route('admin.motor-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
							<tr>
								<th scope="row">Pleasure Boat Quotes</th>
								<td class="text-center"><span class="badge badge-circle badge-primary">{{ $boat_quote_count }}</span></td>
								<td><a href="{{ route('admin.boat-quotes.index') }}" class="btn btn-sm btn-primary">See all</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@include('cms.layouts.footers.auth')
</div>
@endsection

@push('script')
<script type="text/javascript" src="{{ asset('assets_cms/libraries/chartjs/chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets_cms/libraries/chartjs/chart.extension.js') }}"></script>
@endpush