@extends('cms.layouts.main')

@section('content')
@include('cms.layouts.headers.cards')

<div class="container-fluid mt--7">
    <div class="row mt-5">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">{{ $page_info['title'] }}</h3>
                        </div>
                        <div class="col-auto text-right">
                            <p onclick="downloadCovers()" class="my-0 btn btn-sm btn-info">Download Covers</p>
                            <p onclick="downloadResumes()" class="my-0 btn btn-sm btn-info">Download Resumes</p>
                            <a href="{{ route('admin.'.$page_info['link'].'.export') }}" class="btn btn-sm btn-success">Export Data</a>
                        </div>
                    </div>
                </div>

                @if(session('status'))
                <div class="col-12">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                @endif

                <table class="table align-items-center table-flush datatable">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" class="no-sort">First Name</th>
                            <th scope="col" class="no-sort">Gender</th>
                            <th scope="col" class="no-sort">Family Name</th>
                            <th scope="col" class="no-sort">Father Name</th>
                            <th scope="col" class="no-sort">Email Address</th>
                            <th scope="col" class="no-sort">Civil status</th>
                            <th scope="col" class="no-sort">Country</th>
                            <th scope="col" class="no-sort">Kids Number</th>
                            <th scope="col" class="no-sort">City</th>
                            <th scope="col" class="no-sort">Date of Birth</th>
                            <th scope="col" class="no-sort">Mobile Number</th>
                            <th scope="col" class="no-sort">Area</th>
                            <th scope="col" class="no-sort">First Nationality</th>
                            <th scope="col" class="no-sort">Second Nationality</th>
                            <th scope="col" class="no-sort">Landline</th>
                            <th scope="col" class="no-sort">Street</th>
                            <th scope="col" class="no-sort">Building floor</th>
                            <th scope="col" class="no-sort">University</th>
                            <th scope="col" class="no-sort">Education Level</th>
                            <th scope="col" class="no-sort">Area Of Study</th>
                            <th scope="col" class="no-sort">Is Graduated</th>
                            <th scope="col" class="no-sort">Graduation Year</th>
                            <th scope="col" class="no-sort">Employment Status</th>
                            <th scope="col" class="no-sort">Work From Date</th>
                            <th scope="col" class="no-sort">Work To Date</th>
                            <th scope="col" class="no-sort">Monthly Salary</th>
                            <th scope="col" class="no-sort">Experience Year</th>
                            <th scope="col" class="no-sort">Company Name</th>
                            <th scope="col" class="no-sort">Benefits</th>
                            <th scope="col" class="no-sort">Job Title</th>
                            <th scope="col" class="no-sort">Reason of Leaving</th>
                            <th scope="col" class="no-sort">Job Description</th>
                            <th scope="col" class="no-sort">Employment Interest</th>
                            <th scope="col" class="no-sort">Illness</th>
                            <th scope="col" class="no-sort">More Info</th>
                            <th scope="col" class="no-sort">Start Date</th>
                            <th scope="col" class="no-sort">Referer</th>
                            <th scope="col" class="no-sort">Family Employed</th>
                            <th scope="col" class="no-sort">Job Details</th>
                            <th scope="col" class="no-sort">Cover</th>
                            <th scope="col" class="no-sort">Resume</th>
                            <th scope="col" class="no-sort">Agreement</th>
                            <th scope="col" class="no-sort">Created At</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($rows as $row)
                        <tr>
                            <td>{{ $row->first_name }}</td>
                            <td>{{ $row->gender }}</td>
                            <td>{{ $row->family_name }}</td>
                            <td>{{ $row->father_name }}</td>
                            <td>{{ $row->email_address }}</td>
                            <td>{{ $row->civil_status }}</td>
                            <td>{{ $row->country }}</td>
                            <td>{{ $row->kids_number }}</td>
                            <td>{{ $row->city }}</td>
                            <td>{{ $row->d_o_b }}</td>
                            <td>{{ $row->mobile_number }}</td>
                            <td>{{ $row->area }}</td>
                            <td>{{ $row->first_nationality }}</td>
                            <td>{{ $row->second_nationality }}</td>
                            <td>{{ $row->landline }}</td>
                            <td>{{ $row->street }}</td>
                            <td>{{ $row->blg_floor }}</td>
                            <td>{{ $row->university == null ? $row->extra_university : $row->university }}</td>
                            <td>{{ $row->education_level }}</td>
                            <td>{{ $row->area_of_study }}</td>
                            <td>{{ $row->is_graduated }}</td>
                            <td>{{ $row->graduation_year }}</td>
                            <td>{{ $row->employment_status }}</td>
                            <td>{{ $row->work_from_date }}</td>
                            <td>{{ $row->work_to_date }}</td>
                            <td>{{ $row->monthly_salary }}</td>
                            <td>{{ $row->experience_year }}</td>
                            <td>{{ $row->company_name }}</td>
                            <td>{{ $row->benefit }}</td>
                            <td>{{ $row->job_title }}</td>
                            <td>{{ $row->reason_of_leaving }}</td>
                            <td>{{ $row->job_description }}</td>
                            <td>{{ $row->employment_interest }}</td>
                            <td>{{ $row->illness }}</td>
                            <td>{{ $row->medical_info }}</td>
                            <td>{{ $row->start_date }}</td>
                            <td>{{ $row->referer }}</td>
                            <td>{{ $row->family_employed }}</td>
                            <td>{{ $row->job_details }}</td>

                            @php
                            $info = pathinfo($row->cover);
                            $ispdf= false;
                            if ($info["extension"] == "pdf") {
                            $ispdf = true;
                            }
                            @endphp
                            <td>
                                <a href="{{ asset($row->cover) }}" download>
                                    <img src="@if ($ispdf == true)  {{ asset('assets_cms/images/pdf.png') }} @else  {{ asset($row->cover) }} @endif" class="contain img-thumbnail">
                                </a>
                            </td>
                            @php
                            $info = pathinfo($row->resume);
                            $ispdf= false;
                            if ($info["extension"] == "pdf") {
                            $ispdf = true;
                            }
                            @endphp
                            <td>
                                <a href="{{ asset($row->resume) }}" download>
                                    <img src="@if ($ispdf == true)  {{ asset('assets_cms/images/pdf.png') }} @else  {{ asset($row->resume) }} @endif" class="contain img-thumbnail">
                                </a>
                            </td>
                            <td>{{ $row->agreement }}</td>
                            <td>{{ $row->created_at->diffForHumans() }}</td>

                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="{{ route('admin.'.$page_info['link'].'.show', $row) }}">View</a>
                                        <form action="{{ route('admin.'.$page_info['link'].'.destroy', $row) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <button type="button" class="dropdown-item" onclick="confirm('Are you sure you want to delete this record?') ? this.parentElement.submit() : ''">
                                                Delete
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('cms.layouts.footers.auth')
</div>
@endsection
@push('script')
<script type="text/javascript">
    var rows = {!!json_encode($rows) !!};

    function downloadCovers() {
        for (var i = 0; i < rows.length; i++) {
            var a = document.createElement("a");
            a.href = '{{ env('APP_URL') }}' + rows[i]['cover'];
            a.setAttribute("download", "Cover-"+rows[i]['first_name']+" "+rows[i]['family_name']);
            a.click();
        }
    }

    function downloadResumes() {
        for (var i = 0; i < rows.length; i++) {
            var a = document.createElement("a");
            a.href = '{{ env('APP_URL') }}'+ rows[i]['resume'];
            a.setAttribute("download", "Resume-"+rows[i]['first_name']+" "+rows[i]['family_name']);
            a.click();
        }
    }
</script>
@endpush('script')
