@extends('cms.layouts.main')

@section('content')
@include('cms.layouts.headers.partials', ['title' => 'View Record'])

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">{{ $page_info['title'] }}</h3>
                        </div>
                        <div class="col-auto text-right">
                            <a href="{{ route('admin.'.$page_info['link'].'.index') }}" class="btn btn-sm btn-primary">Back to list</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled">
                        <li>
                            <h4>First Name</h4>
                            <p>{{ $row->first_name }}</p>
                        </li>
                        <li>
                            <h4>Gender</h4>
                            <p>{{ $row->gender }}</p>
                        </li>
                        <li>
                            <h4>Family Name</h4>
                            <p>{{ $row->family_name }}</p>
                        </li>
                        <li>
                            <h4>Father Name</h4>
                            <p>{{ $row->father_name }}</p>
                        </li>
                        <li>
                            <h4>Civil status</h4>
                            <p>{{ $row->civil_status }}</p>
                        </li>
                        <li>
                            <h4>Country</h4>
                            <p>{{ $row->country }}</p>
                        </li>
                        <li>
                            <h4>Kids Number</h4>
                            <p>{{ $row->kids_number }}</p>
                        </li>
                        <li>
                            <h4>City</h4>
                            <p>{{ $row->city }}</p>
                        </li>
                        <li>
                            <h4>Date of Birth</h4>
                            <p>{{ $row->d_o_b }}</p>
                        </li>
                        <li>
                            <h4>Mobile Number</h4>
                            <p>{{ $row->mobile_number }}</p>
                        </li>
                        <li>
                            <h4>Email Address</h4>
                            <p>{{ $row->email_address }}</p>
                        </li>
                        <li>
                            <h4>Area</h4>
                            <p>{{ $row->area }}</p>
                        </li>
                        <li>
                            <h4>First Nationality</h4>
                            <p>{{ $row->first_nationality }}</p>
                        </li>
                        <li>
                            <h4>Second Nationality</h4>
                            <p>{{ $row->second_nationality }}</p>
                        </li>
                        <li>
                            <h4>Landline</h4>
                            <p>{{ $row->landline }}</p>
                        </li>
                        <li>
                            <h4>Street</h4>
                            <p>{{ $row->street }}</p>
                        </li>
                        <li>
                            <h4>Building floor</h4>
                            <p>{{ $row->blg_floor }}</p>
                        </li>
                        <li>
                            <h4>University</h4>
                            <p>{{ $row->university == null ? $row->extra_university : $row->university }}</p>
                        </li>
                        <li>
                            <h4>Education Level</h4>
                            <p>{{ $row->education_level }}</p>
                        </li>
                        <li>
                            <h4>Area Of Study</h4>
                            <p>{{ $row->area_of_study }}</p>
                        </li>
                        <li>
                            <h4>Graduated</h4>
                            <p>{{ $row->is_graduated }}</p>
                        </li>
                        <li>
                            <h4>Year Of Graduation</h4>
                            <p>{{ $row->graduation_year }}</p>
                        </li>
                        <li>
                            <h4>Employment Status</h4>
                            <p>{{ $row->employment_status }}</p>
                        </li>
                        <li>
                            <h4>Work From Date</h4>
                            <p>{{ $row->work_from_date }}</p>
                        </li>
                        <li>
                            <h4>Work To Date</h4>
                            <p>{{ $row->work_to_date }}</p>
                        </li>
                        <li>
                            <h4>Monthly Salary</h4>
                            <p>{{ $row->monthly_salary }}</p>
                        </li>
                        <li>
                            <h4>Experience Year</h4>
                            <p>{{ $row->experience_year }}</p>
                        </li>
                        <li>
                            <h4>Company Name</h4>
                            <p>{{ $row->company_name }}</p>
                        </li>
                        <li>
                            <h4>Benefits</h4>
                            <p>{{ $row->benefit }}</p>
                        </li>
                        <li>
                            <h4>Job Title</h4>
                            <p>{{ $row->job_title }}</p>
                        </li>
                        <li>
                            <h4>Reason of Leaving</h4>
                            <p>{{ $row->reason_of_leaving }}</p>
                        </li>
                        <li>
                            <h4>Job Description</h4>
                            <p>{{ $row->job_description }}</p>
                        </li>
                        <li>
                            <h4>Employment Interest</h4>
                            <p>{{ $row->employment_interest }}</p>
                        </li>
                        <li>
                            <h4>Illness</h4>
                            <p>{{ $row->illness }}</p>
                        </li>
                        <li>
                            <h4>More Info Date</h4>
                            <p>{{ $row->medical_info }}</p>
                        </li>
                        <li>
                            <h4>Start Date</h4>
                            <p>{{ $row->start_date }}</p>
                        </li>
                        <li>
                            <h4>Referer</h4>
                            <p>{{ $row->referer }}</p>
                        </li>
                        <li>
                            <h4>Family Employed</h4>
                            <p>{{ $row->family_employed }}</p>
                        </li>
                        <li>
                            <h4>Job Details</h4>
                            <p>{{ $row->job_details }}</p>
                        </li>
                        <hr class="my-4">
                        <li>
                            @php
                            $info = pathinfo($row->cover);
                            $ispdf= false;
                            if ($info["extension"] == "pdf") {
                            $ispdf = true;
                            }
                            @endphp
                            <h4>Cover</h4>
                            <img width="100" src="@if ($ispdf == true)  {{ asset('assets_cms/images/pdf.png') }} @else  {{ asset($row->cover) }} @endif" class="contain img-thumbnail">
                        </li>
                        <hr class="my-4">
                        <li>
                            @php
                            $info = pathinfo($row->resume);
                            $ispdf= false;
                            if ($info["extension"] == "pdf") {
                            $ispdf = true;
                            }
                            @endphp
                            <h4>Resume</h4>
                            <img width="100" src="@if ($ispdf == true)  {{ asset('assets_cms/images/pdf.png') }} @else  {{ asset($row->resume) }} @endif" class="contain img-thumbnail">
                        </li>
                        <hr class="my-4">
                        <li>
                            <h4>Agreement</h4>
                            <p>{{ $row->agreement }}</p>
                        </li>
                        <hr class="my-4">
                        <li>
                            <h4>Created at</h4>
                            <p>{{ date('d M Y - h:i A', strtotime($row->created_at)) }}</p>
                        </li>
                        <hr class="my-4">
                        <li>
                            <h4>Updated at</h4>
                            <p>{{ date('d M Y - h:i A', strtotime($row->updated_at)) }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('cms.layouts.footers.auth')
</div>
@endsection
