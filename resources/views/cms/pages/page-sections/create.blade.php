@extends('cms.layouts.main')

@section('content')
@include('cms.layouts.headers.partials', ['title' => 'Add Record'])

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">{{ $page_info['title'] }}</h3>
                        </div>
                        <div class="col-auto text-right">
                            <a href="{{ route('admin.'.$page_info['link'].'.index') }}" class="btn btn-sm btn-primary">Back to list</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('admin.'.$page_info['link'].'.store') }}" enctype="multipart/form-data" autocomplete="off">
                        @csrf

                        <div class="px-lg-4">
                            {{-- Slug --}}
                            @include('cms.components.inputs.text', ['label' => 'Slug', 'asterix' => true, 'name' => 'slug'])

                            {{-- Image --}}
                            @include('cms.components.inputs.image', ['label' => 'Image', 'asterix' => true, 'name' => 'image'])

                            {{-- English Header Title --}}
                            @include('cms.components.inputs.text', ['label' => 'English Header Title', 'asterix' => true, 'name' => 'header_title_en'])

                            {{-- Arabic Header Title --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic Header Title', 'asterix' => true, 'name' => 'header_title_ar'])

                            {{-- Page --}}
                            @include('cms.components.inputs.select-single', ['label' => 'Page', 'asterix' => true, 'name' => 'page_id', 'placeholder' => 'Please select a page', 'rows' => $pages])

                            {{-- English Title --}}
                            @include('cms.components.inputs.text', ['label' => 'English Title', 'asterix' => false, 'name' => 'title_en'])

                            {{-- Arabic Title --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic Title', 'asterix' => false, 'name' => 'title_ar'])

                            {{-- English Text --}}
                            @include('cms.components.inputs.textarea', ['label' => 'English Text', 'asterix' => false, 'name' => 'text_en', 'quill' => true])

                            {{-- Arabic Text --}}
                            @include('cms.components.inputs.textarea', ['label' => 'Arabic Text', 'asterix' => false, 'name' => 'text_ar', 'quill' => true])

                            {{-- English SubTitle --}}
                            @include('cms.components.inputs.text', ['label' => 'English SubTitle', 'asterix' => false, 'name' => 'subtitle_en'])

                            {{-- Arabic SubTitle --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic SubTitle', 'asterix' => false, 'name' => 'subtitle_ar'])

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('cms.layouts.footers.auth')
</div>
@endsection