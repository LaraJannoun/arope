@extends('cms.layouts.main')

@section('content')
@include('cms.layouts.headers.cards')

<div class="container-fluid mt--7">
    <div class="row mt-5">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">{{ $page_info['title'] }}</h3>
                        </div>
                        <div class="col-auto text-right">
                            <a href="{{ route('admin.'.$page_info['link'].'.create') }}" class="btn btn-sm btn-primary">Add</a>
                            <a href="{{ route('admin.'.$page_info['link'].'.order') }}" class="btn btn-sm btn-warning">Order</a>
                        </div>
                    </div>
                </div>

                @if(session('status'))
                <div class="col-12">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                @endif

                <table class="table align-items-center table-flush datatable">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" class="no-sort">Lat</th>
                            <th scope="col" class="no-sort">Lng</th>
                            <th scope="col" class="no-sort">English Main Title</th>
                            <th scope="col" class="no-sort">Arabic Main Title</th>
                            <th scope="col" class="no-sort">English Title</th>
                            <th scope="col" class="no-sort">Arabic Title</th>
                            <th scope="col" class="no-sort">English Text</th>
                            <th scope="col" class="no-sort">Arabic Text</th>
                            <th scope="col" class="no-sort">English Phone</th>
                            <th scope="col" class="no-sort">Arabic Phone</th>
                            <th scope="col" class="no-sort">English Fax</th>
                            <th scope="col" class="no-sort">Arabic Fax</th>
                            <th scope="col" class="no-sort">English Email</th>
                            <th scope="col" class="no-sort">Arabic Email</th>
                            <th scope="col" class="no-sort">Link</th>
                            <th scope="col" class="no-sort">Publish</th>
                            <th scope="col" class="no-sort"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($rows as $row)
                        <tr>
                            <td>{{ $row->lat }}</td>
                            <td>{{ $row->lng }}</td>
                            <td>{{ $row->main_title_en }}</td>
                            <td>{{ $row->main_title_ar }}</td>
                            <td>{{ $row->title_en }}</td>
                            <td>{{ $row->title_ar }}</td>
                            <td>{{ substr($row->text_en, 0, 20) }}...</td>
                            <td>{{ substr($row->text_ar, 0, 20) }}...</td>
                            <td>{{ $row->phone_en }}</td>
                            <td>{{ $row->phone_ar }}</td>
                            <td>{{ $row->fax_en }}</td>
                            <td>{{ $row->fax_ar }}</td>
                            <td>{{ $row->email_en }}</td>
                            <td>{{ $row->email_ar }}</td>
                            <td>{{ $row->link }}</td>
                            <td class="adjust-element">
                                <label class="custom-toggle mb-0">
                                    <input class="publish-js" type="checkbox" value="{{ $row->id }}" @if($row->publish) {{ "checked" }} @endif>
                                    <span class="custom-toggle-slider rounded-circle"></span>
                                </label>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="{{ route('admin.'.$page_info['link'].'.show', $row) }}">View</a>
                                        <a class="dropdown-item" href="{{ route('admin.'.$page_info['link'].'.edit', $row) }}">Edit</a>
                                        <form action="{{ route('admin.'.$page_info['link'].'.destroy', $row) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <button type="button" class="dropdown-item" onclick="confirm('Are you sure you want to delete this record?') ? this.parentElement.submit() : ''">
                                                Delete
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('cms.layouts.footers.auth')
</div>
@endsection
@push('script')
<script type="text/javascript">
    $('.publish-js').change(function() {
        var $this = $(this);
        $.ajax({
            type: "post",
            url: "{{ route('admin.'.$page_info['link'].'.publish') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id": $this.val()
            }
        });
    });
</script>
@endpush