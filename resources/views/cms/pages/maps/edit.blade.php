@extends('cms.layouts.main')

@section('content')
@include('cms.layouts.headers.partials', ['title' => 'Edit Record'])

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">{{ $page_info['title'] }}</h3>
                        </div>
                        <div class="col-auto text-right">
                            <a href="{{ route('admin.'.$page_info['link'].'.index') }}" class="btn btn-sm btn-primary">Back to list</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('admin.'.$page_info['link'].'.update', $row) }}" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        @method('put')
                        <div class="px-lg-4">
                            {{-- Lat--}}
                            @include('cms.components.inputs.text', ['label' => 'Lat', 'asterix' => true, 'name' => 'lat'])

                            {{-- Lng--}}
                            @include('cms.components.inputs.text', ['label' => 'Lng', 'asterix' => true, 'name' => 'lng'])

                            {{-- English Main Title --}}
                            @include('cms.components.inputs.text', ['label' => 'English Main Title', 'asterix' => true, 'name' => 'main_title_en'])

                            {{-- Arabic Main Title --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic Main Title', 'asterix' => true, 'name' => 'main_title_ar'])

                            {{-- English Title --}}
                            @include('cms.components.inputs.text', ['label' => 'English Title', 'asterix' => true, 'name' => 'title_en'])

                            {{-- Arabic Title --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic Title', 'asterix' => true, 'name' => 'title_ar'])

                            {{-- English Text --}}
                            @include('cms.components.inputs.textarea', ['label' => 'English Text', 'asterix' => true, 'name' => 'text_en', 'quill' => true])

                            {{-- Arabic Text --}}
                            @include('cms.components.inputs.textarea', ['label' => 'Arabic Text', 'asterix' => true, 'name' => 'text_ar', 'quill' => true])

                            {{-- English Phone --}}
                            @include('cms.components.inputs.text', ['label' => 'English Phone', 'asterix' => true, 'name' => 'phone_en'])

                            {{-- Arabic Phone --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic Phone', 'asterix' => true, 'name' => 'phone_ar'])

                            {{-- English Fax --}}
                            @include('cms.components.inputs.text', ['label' => 'English Fax', 'asterix' => true, 'name' => 'fax_en'])

                            {{-- Arabic Fax --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic Fax', 'asterix' => true, 'name' => 'fax_ar'])

                            {{-- English Email --}}
                            @include('cms.components.inputs.text', ['label' => 'English Email', 'asterix' => true, 'name' => 'email_en'])

                            {{-- Arabic Email --}}
                            @include('cms.components.inputs.text', ['label' => 'Arabic Email', 'asterix' => true, 'name' => 'email_ar'])

                            {{-- Link --}}
                            @include('cms.components.inputs.text', ['label' => 'Link', 'asterix' => true, 'name' => 'link'])

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('cms.layouts.footers.auth')
</div>
@endsection