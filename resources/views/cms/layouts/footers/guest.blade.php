<footer class="py-5 footer-bottom">
    <div class="container">
        @include('cms.layouts.footers.nav')
    </div>
</footer>