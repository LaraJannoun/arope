<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        {{-- Toggler --}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        {{-- Brand --}}
        <a class="navbar-brand pt-0" href="{{ route('admin.dashboard') }}">
            <img src="{{ asset('assets_cms/images/logo.png') }}" class="navbar-brand-img" alt="Tedmob Logo">
        </a>
        {{-- User --}}
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ asset('assets_cms/images/default_avatar.png') }}">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="{{ route('admin.profile.edit') }}" class="dropdown-item">
                        <i class="fas fa-user-circle"></i>
                        <span>My profile</span>
                    </a>

                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
        {{-- Collapse --}}
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            {{-- Collapse header --}}
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('admin.dashboard') }}">
                            <img src="{{ asset('assets_cms/images/logo.png') }}">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            {{-- Navigation --}}
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.dashboard') ? 'active' : '' }}" href="{{ route('admin.dashboard') }}">
                        <i class="fas fa-drafting-compass text-primary"></i>Dashboard
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.admins.*') ? 'active' : '' }}" href="{{ route('admin.admins.index') }}">
                        <i class="fas fa-users-cog text-primary"></i>Admins Management
                    </a>
                </li>
            </ul>
            <hr class="my-3">
            <h6 class="navbar-heading text-muted">GENERAL MANAGEMENT</h6>
            <ul class="navbar-nav mb-3">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.fixed-sections.*') ? 'active' : '' }}" href="{{ route('admin.fixed-sections.index') }}">
                        <i class="fas fa-columns"></i>Fixed & Informative Sections
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.banners.*') ? 'active' : '' }}" href="{{ route('admin.banners.index') }}">
                        <i class="fas fa-image"></i>Banners
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#pages" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.pages.*') ? 'true' : 'false' }}" aria-controls="pages">
                        <i class="fas fa-book"></i> <span class="nav-link-text">Pages & Sections</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.page-sections.*') || Route::is('admin.pages.*') ? 'show' : '' }}" id="pages">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.pages.*') && !Route::is('admin.pages.*') ? 'active' : '' }}" href="{{ route('admin.pages.index') }}">
                                    <i class="fas fa-tasks"></i>Pages/Banners
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.page-sections.*') && !Route::is('admin.page-sections.*') ? 'active' : '' }}" href="{{ route('admin.page-sections.index') }}">
                                    <i class="fas fa-tasks"></i>Page Sections
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <h6 class="navbar-heading text-muted">Platform Content</h6>
            <ul class="navbar-nav mb-3">
                <li class="nav-item">
                    <a class="nav-link" href="#home" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.home.*') ? 'true' : 'false' }}" aria-controls="home">
                        <i class="fas fa-book-open"></i>
                        <span class="nav-link-text">Home</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.slider-items.*') || Route::is('admin.home-sliders.*') || Route::is('admin.financial-highlights.*')  || Route::is('admin.home-links.*') || Route::is('admin.home-services.*')  ? 'show' : '' }}" id="home">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.home-sliders.*') ? 'active' : '' }}" href="{{ route('admin.home-sliders.index') }}">
                                    <i class="far fa-images"></i>Home Sliders
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.slider-items.*') ? 'active' : '' }}" href="{{ route('admin.slider-items.index') }}">
                                    <i class="far fa-images"></i>Slider Items
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.financial-highlights.*') ? 'active' : '' }}" href="{{ route('admin.financial-highlights.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Financial Highlights
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.home-services.*') ? 'active' : '' }}" href="{{ route('admin.home-services.index') }}">
                                    <i class="fas fa-stream"></i>Services
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.home-links.*') ? 'active' : '' }}" href="{{ route('admin.home-links.index') }}">
                                    <i class="fas fa-pen-alt"></i>Links
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#solutions" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.personal-solutions.*') ? 'true' : 'false' }}" aria-controls="solutions">
                        <i class="fas fa-users-cog"></i> <span class="nav-link-text">Solutions</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.personal-solutions.*') || Route::is('admin.business-solutions.*') || Route::is('admin.items.*') || Route::is('admin.item-images.*')? 'show' : '' }}" id="solutions">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.personal-solution.*') && !Route::is('admin.personal-solutions.*') ? 'active' : '' }}" href="{{ route('admin.personal-solutions.index') }}">
                                    <i class="fas fa-users-cog"></i>Personal Solutions
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.business-solutions.*') && !Route::is('admin.business-solutions.*') ? 'active' : '' }}" href="{{ route('admin.business-solutions.index') }}">
                                    <i class="fas fa-business-time"></i>Business Solutions
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.items.*') ? 'active' : '' }}" href="{{ route('admin.items.index') }}">
                                    <i class="fas fa-pen-alt"></i>Items
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.item-images.*') ? 'active' : '' }}" href="{{ route('admin.item-images.index') }}">
                                    <i class="far fa-images"></i>
                                    Item Images
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#team" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.team.*') ? 'true' : 'false' }}" aria-controls="team">
                        <i class="far fa-comments"></i> <span class="nav-link-text">Testimonials</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.testimonials.*') ? 'show' : '' }}" id="team">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.testimonials.*') && !Route::is('admin.testimonials.*') ? 'active' : '' }}" href="{{ route('admin.testimonials.index') }}">
                                    <i class="far fa-comments"></i>Testimonials
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#about-us" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.about-items.*') ? 'true' : 'false' }}" aria-controls="about-us">
                        <i class="fas fa-user-tag"></i> <span class="nav-link-text">About Us</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.about-items.*') || Route::is('admin.about-timelines.*') || Route::is('admin.quality-policy.*') || Route::is('admin.team.*') || Route::is('admin.financial-highlights.*') || Route::is('admin.annual-reports.*') || Route::is('admin.reinsurers.*') ? 'show' : '' }}" id="about-us">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.about-items.*') && !Route::is('admin.about-items.*') ? 'active' : '' }}" href="{{ route('admin.about-items.index') }}">
                                    <i class="fas fa-stream"></i>Items
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.about-timelines.*') ? 'active' : '' }}" href="{{ route('admin.about-timelines.index') }}">
                                    <i class="fas fa-hourglass-start"></i>Timelines
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.quality-policy.*') ? 'active' : '' }}" href="{{ route('admin.quality-policy.index') }}">
                                    <i class="fas fa-balance-scale"></i>Quality Policy
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.team.*') && !Route::is('admin.team.*') ? 'active' : '' }}" href="{{ route('admin.team.index') }}">
                                    <i class="fas fa-users"></i>Shareholders & Teams
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.reinsurers.*') && !Route::is('admin.reinsurers.*') ? 'active' : '' }}" href="{{ route('admin.reinsurers.index') }}">
                                    <i class="fas fa-user-tie"></i>Reinsurers
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.financial-highlights.*') ? 'active' : '' }}" href="{{ route('admin.financial-highlights.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Financial Highlights
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link ml-3 {{ Route::is('admin.annual-reports.*') ? 'active' : '' }}" href="{{ route('admin.annual-reports.index') }}">
                                    <i class="fas fa-download"></i>Annual Reports
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#careers" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.careers.*') ? 'true' : 'false' }}" aria-controls="careers">
                        <i class="fas fa-project-diagram"></i>
                        <span class="nav-link-text">Careers</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.career-items.*') || Route::is('admin.career-lists.*')  || Route::is('admin.career-positions.*') ? 'show' : '' }}" id="careers">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.career-items.*') ? 'active' : '' }}" href="{{ route('admin.career-items.index') }}">
                                    <i class="fas fa-stream"></i>Items
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.career-lists.*') ? 'active' : '' }}" href="{{ route('admin.career-lists.index') }}">
                                    <i class="fas fa-tasks"></i>Predefined Lists
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.career-positions.*') ? 'active' : '' }}" href="{{ route('admin.career-positions.index') }}">
                                    <i class="fas fa-project-diagram"></i>Positions
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#media-room" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.media-room.*') ? 'true' : 'false' }}" aria-controls="media-room">
                        <i class="fas fa-hashtag"></i>
                        <span class="nav-link-text">Media Room</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.publication-items.*') || Route::is('admin.publication-headers.*') || Route::is('admin.news.*') || Route::is('admin.events.*')  || Route::is('admin.press-headers.*')  || Route::is('admin.press-items.*')  || Route::is('admin.blogs.*') ? 'show' : '' }}" id="media-room">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.news.*') ? 'active' : '' }}" href="{{ route('admin.news.index') }}">
                                    <i class="far fa-newspaper"></i>News
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.news-images.*') ? 'active' : '' }}" href="{{ route('admin.news-images.index') }}">
                                    <i class="far fa-images"></i>
                                    News Images
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.events.*') ? 'active' : '' }}" href="{{ route('admin.events.index') }}">
                                    <i class="far fa-calendar-plus"></i>Events
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.event-images.*') ? 'active' : '' }}" href="{{ route('admin.event-images.index') }}">
                                    <i class="far fa-images"></i>
                                    Event Images
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.press-headers.*') ? 'active' : '' }}" href="{{ route('admin.press-headers.index') }}">
                                    <i class="far fa-calendar"></i>Press Headers
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.press-items.*') ? 'active' : '' }}" href="{{ route('admin.press-items.index') }}">
                                    <i class="far fa-bookmark"></i>Press
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.blogs.*') ? 'active' : '' }}" href="{{ route('admin.blogs.index') }}">
                                    <i class="fab fa-blogger-b"></i>Blogs
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.blog-images.*') ? 'active' : '' }}" href="{{ route('admin.blog-images.index') }}">
                                    <i class="far fa-images"></i>
                                    Blog Images
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.publication-headers.*') ? 'active' : '' }}" href="{{ route('admin.publication-headers.index') }}">
                                    <i class="far fa-calendar"></i>Publication Headers
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.publication-items.*') ? 'active' : '' }}" href="{{ route('admin.publication-items.index') }}">
                                    <i class="far fa-bookmark"></i>Publication
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#csr" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.csr.*') ? 'true' : 'false' }}" aria-controls="csr">
                        <i class="fas fa-balance-scale"></i>
                        <span class="nav-link-text">CSR</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.csr-items.*') ? 'show' : '' }}" id="csr">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.csr-items.*') ? 'active' : '' }}" href="{{ route('admin.csr-items.index') }}">
                                    <i class="fas fa-stream"></i>
                                    Items
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#problem-types" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.problem-types.*') ? 'true' : 'false' }}" aria-controls="problem-types">
                        <i class="fas fa-exclamation"></i>
                        <span class="nav-link-text">Report A Problem</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.problem-types.*') ? 'show' : '' }}" id="problem-types">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.problem-types.*') ? 'active' : '' }}" href="{{ route('admin.problem-types.index') }}">
                                    <i class="fas fa-exclamation"></i>
                                    Problem Types
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#offers" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.offers.*') ? 'true' : 'false' }}" aria-controls="offers">
                        <i class="far fa-images"></i>
                        <span class="nav-link-text">Offers</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.all-offers.*') ? 'show' : '' }}" id="offers">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.all-offers.*') ? 'active' : '' }}" href="{{ route('admin.all-offers.index') }}">
                                    <i class="far fa-images"></i>
                                    Images
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#payment-steps" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.payment-steps.*') ? 'true' : 'false' }}" aria-controls="payment-steps">
                        <i class="fas fa-money-check-alt"></i>
                        <span class="nav-link-text">Pay Online</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.payment-steps.*') ? 'show' : '' }}" id="payment-steps">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.payment-steps.*') ? 'active' : '' }}" href="{{ route('admin.payment-steps.index') }}">
                                    <i class="fas fa-money-check-alt"></i>
                                    Payment Steps
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#claims" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.claims.*') ? 'true' : 'false' }}" aria-controls="claims">
                        <i class="fas fa-exclamation-circle"></i> <span class="nav-link-text">Claims</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.claim-procedures.*') || Route::is('admin.claim-types.*') ? 'show' : '' }}" id="claims">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.claim-procedures.*') ? 'active' : '' }}" href="{{ route('admin.claim-procedures.index') }}">
                                    <i class="fas fa-stream"></i>
                                    Claims Procedures
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.claim-types.*') ? 'active' : '' }}" href="{{ route('admin.claim-types.index') }}">
                                    <i class="fas fa-exclamation-circle"></i>
                                    Types & Workshops
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#faqs" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.faqs.*') ? 'true' : 'false' }}" aria-controls="faqs">
                        <i class="far fa-question-circle"></i>
                        <span class="nav-link-text">FAQs</span>
                    </a>
                    <div class="collapse {{ Route::is('admin.faqs-headers.*') || Route::is('admin.all-faqs.*') ? 'show' : '' }}" id="faqs">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.faqs-headers.*') ? 'active' : '' }}" href="{{ route('admin.faqs-headers.index') }}">
                                    <i class="fas fa-stream"></i>
                                    FAQs - Headers
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.all-faqs.*') ? 'active' : '' }}" href="{{ route('admin.all-faqs.index') }}">
                                    <i class="far fa-question-circle"></i>
                                    FAQs
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <h6 class="navbar-heading text-muted">Contact Us</h6>
            <ul class="navbar-nav mb-3">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.maps.*') ? 'active' : '' }}" href="{{ route('admin.maps.index') }}">
                        <i class="fas fa-map-marked-alt"></i>
                        Maps
                    </a>
                </li>
            </ul>
            <h6 class="navbar-heading text-muted">Footer</h6>
            <ul class="navbar-nav mb-3">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.footer-details.*') ? 'active' : '' }}" href="{{ route('admin.footer-details.index') }}">
                        <i class="fas fa-info-circle"></i>
                        Details
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.social-media.*') ? 'active' : '' }}" href="{{ route('admin.social-media.index') }}">
                        <i class="fas fa-hashtag"></i>
                        Social Media
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.external-links.*') ? 'active' : '' }}" href="{{ route('admin.external-links.index') }}">
                        <i class="fas fa-hashtag"></i>
                        External Links
                    </a>
                </li>
            </ul>
            <h6 class="navbar-heading text-muted">Forms & Submissions</h6>
            <ul class="navbar-nav mb-3">
                <li class="nav-item">
                    <a class="nav-link" href="#quotes" data-toggle="collapse" role="button" aria-expanded="{{ Route::is('admin.quotes.*') ? 'true' : 'false' }}" aria-controls="quotes">
                        <i class="far fa-question-circle"></i>
                        <span class="nav-link-text">Quotes</span>
                    </a>
                    <div class="collapse {{Route::is('admin.boat-quotes.*') || Route::is('admin.expat-quotes.*') ||  Route::is('admin.motor-quotes.*') || Route::is('admin.healthcare-quotes.*') || Route::is('admin.household-quotes.*') || Route::is('admin.retirement-quotes.*') || Route::is('admin.education-quotes.*') || Route::is('admin.quotes.*') || Route::is('admin.personal-quotes.*') ||  Route::is('admin.travel-quotes.*') || Route::is('admin.term-life-quotes.*')? 'show' : '' }}" id="quotes">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.quotes.*') ? 'active' : '' }}" href="{{ route('admin.quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Business Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.personal-quotes.*') ? 'active' : '' }}" href="{{ route('admin.personal-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Personal Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.travel-quotes.*') ? 'active' : '' }}" href="{{ route('admin.travel-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Travel Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.term-life-quotes.*') ? 'active' : '' }}" href="{{ route('admin.term-life-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Term Life Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.education-quotes.*') ? 'active' : '' }}" href="{{ route('admin.education-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Education Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.retirement-quotes.*') ? 'active' : '' }}" href="{{ route('admin.retirement-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Retirement Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.household-quotes.*') ? 'active' : '' }}" href="{{ route('admin.household-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Household Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.healthcare-quotes.*') ? 'active' : '' }}" href="{{ route('admin.healthcare-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Healthcare Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.motor-quotes.*') ? 'active' : '' }}" href="{{ route('admin.motor-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Motor Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.expat-quotes.*') ? 'active' : '' }}" href="{{ route('admin.expat-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Expat Quotes
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('admin.boat-quotes.*') ? 'active' : '' }}" href="{{ route('admin.boat-quotes.index') }}">
                                    <i class="fas fa-dollar-sign"></i>Boat Quotes
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.problems.*') ? 'active' : '' }}" href="{{ route('admin.problems.index') }}">
                        <i class="fas fa-exclamation"></i>Problems
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.claims.*') ? 'active' : '' }}" href="{{ route('admin.claims.index') }}">
                        <i class="fas fa-exclamation-circle"></i>Claims
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.motor-claims.*') ? 'active' : '' }}" href="{{ route('admin.motor-claims.index') }}">
                        <i class="fas fa-exclamation-circle"></i>Motor Claims
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.career-applications.*') ? 'active' : '' }}" href="{{ route('admin.career-applications.index') }}">
                        <i class="fas fa-laptop"></i>Career Applications
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.contact-requests.*') ? 'active' : '' }}" href="{{ route('admin.contact-requests.index') }}">
                        <i class="far fa-edit"></i>Contact Requests
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.subscribers.*') ? 'active' : '' }}" href="{{ route('admin.subscribers.index') }}">
                        <i class="fas fa-at"></i>
                        Subscribers
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>