<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\TravelQuote;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class TravelQuoteController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Travel Quote',
            'link' => 'travel-quotes',
            'table_name' => 'travel_quotes'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = TravelQuote::select([
            'id',
            'first_name',
            'landline_number',
            'occupation',
            'father_name',
            'mobile_number',
            'year_of_birth',
            'last_name',
            'email',
            'plan_type',
            'trip_duration',
            'geo_zone',
            'destination',
            'insurer_name',
            'insurer_relation',
            'insurer_year'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = TravelQuote::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new TravelQuote;
        $row->first_name = $request->first_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->father_name = $request->father_name;
        $row->mobile_number = $request->mobile_number;
        $row->year_of_birth = $request->year_of_birth;
        $row->last_name = $request->last_name;
        $row->email = $request->email;
        $row->plan_type = $request->plan_type;
        $row->trip_duration = $request->trip_duration;
        $row->geo_zone = $request->geo_zone;
        $row->destination = $request->destination;
        $row->insurer_name = $request->insurer_name;
        $row->insurer_relation = $request->insurer_relation;
        $row->insurer_year = $request->insurer_year;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = TravelQuote::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = TravelQuote::findOrFail($id);
        $row->first_name = $request->first_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->father_name = $request->father_name;
        $row->mobile_number = $request->mobile_number;
        $row->year_of_birth = $request->year_of_birth;
        $row->last_name = $request->last_name;
        $row->email = $request->email;
        $row->plan_type = $request->plan_type;
        $row->trip_duration = $request->trip_duration;
        $row->geo_zone = $request->geo_zone;
        $row->destination = $request->destination;
        $row->insurer_name = $request->insurer_name;
        $row->insurer_relation = $request->insurer_relation;
        $row->insurer_year = $request->insurer_year;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        TravelQuote::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new TravelQuotesExport, 'travel_quotes_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT travel QUOTES
*/
class TravelQuotesExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return TravelQuote::select([
            'id',
            'first_name',
            'father_name',
            'last_name',
            'email',
            'mobile_number',
            'landline_number',
            'occupation',
            'plan_type',
            'trip_duration',
            'geo_zone',
            'destination',
            'insurer_name',
            'insurer_relation',
            'insurer_year'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'First Name',
            'Father Name',
            'Last Name',
            'Email',
            'Mobile Number',
            'Landline number',
            'Occupation',
            'Plan Type',
            'Trip Duration',
            'Geographical Zone',
            'Destination',
            'Insurer Names',
            'Insurer Relations',
            'Insurer Years'
        ];
    }
}
