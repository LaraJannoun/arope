<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Pages',
            'link' => 'pages',
            'table_name' => 'pages'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = Page::select([
            'id',
            'slug',
            'image',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'publish'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = Page::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new Page;
        $image_path = null;
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }
        $row->image = $image_path;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->text_en = $request->text_en;
        $row->text_ar = $request->text_ar;
        $row->slug = $request->slug;
        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = Page::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = Page::findOrFail($id);
        $image_path = $row['image'];
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }

        $row->image = $image_path;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->text_en = $request->text_en;
        $row->text_ar = $request->text_ar;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        Page::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }


    /**
     * Publish a specified row
     *
     */
    public function publish(Request $request)
    {
        $id = $request['id'];
        $row = Page::findOrFail($id);
        $row->publish = !$row->publish;
        $row->save();

        $page_info = $this->page_info();
        parent::add_log('publish', $page_info['link']);
    }

    /**
     * Show the form for ordering all rows
     *
     */
    public function order()
    {
        $page_info = $this->page_info();

        $rows = Page::select([
            'id',
            'slug'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.order', compact('page_info', 'rows'));
    }

    /**
     * Update the order for all rows in the database
     *
     */
    public function orderSubmit(Request $request)
    {
        $page_info = $this->page_info();

        foreach ($request->id as $key => $id) {
            $row = Page::findOrFail($id);
            $row->pos = $request->pos[$key];
            $row->save();
        }
        parent::add_log('order', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Records successfully ordered.');
    }
}
