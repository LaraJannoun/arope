<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Faqs;
use App\Models\FaqsHeader;

class FaqsController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Faqs',
            'link' => 'all-faqs',
            'table_name' => 'faqs'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = Faqs::select([
            'id',
            'faqs_header_id',
            'question_en',
            'answer_en',
            'question_ar',
            'answer_ar',
            'publish'
        ])->orderBy('pos')->get();

        $faqs_headers = FaqsHeader::select([
            'id',
            'title_en',
            'title_ar'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows', 'faqs_headers'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = Faqs::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        $faqs_headers = FaqsHeader::select([
            'id',
            'title_en',
            'title_ar'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info', 'faqs_headers'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new Faqs;
        $row->question_en = $request->question_en;
        $row->answer_en = $request->answer_en;
        $row->question_ar = $request->question_ar;
        $row->answer_ar = $request->answer_ar;
        $row->faqs_header_id = $request->faqs_header_id;
        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();
        $faqs_headers = FaqsHeader::select([
            'id',
            'title_en',
            'title_ar'
        ])->orderBy('pos')->get();

        $row = Faqs::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row', 'faqs_headers'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = Faqs::findOrFail($id);
        $row->question_en = $request->question_en;
        $row->answer_en = $request->answer_en;
        $row->question_ar = $request->question_ar;
        $row->answer_ar = $request->answer_ar;
        $row->faqs_header_id = $request->faqs_header_id;
        $row->save();
        parent::add_log('update', $page_info['link']);
        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        Faqs::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);
        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }


    /**
     * Publish a specified row
     *
     */
    public function publish(Request $request)
    {
        $page_info = $this->page_info();

        $id = $request['id'];
        $row = Faqs::findOrFail($id);
        $row->publish = !$row->publish;
        $row->save();
        parent::add_log('publish', $page_info['link']);
    }

    /**
     * Show the form for ordering all rows
     *
     */
    public function order()
    {
        $page_info = $this->page_info();

        $rows = Faqs::select([
            'id',
            'question_en'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.order', compact('page_info', 'rows'));
    }

    /**
     * Update the order for all rows in the database
     *
     */
    public function orderSubmit(Request $request)
    {
        $page_info = $this->page_info();

        foreach ($request->id as $key => $id) {
            $row = Faqs::findOrFail($id);
            $row->pos = $request->pos[$key];
            $row->save();
        }
        parent::add_log('order', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Records successfully ordered.');
    }
}
