<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\ContactRequest;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class ContactRequestController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Contact Requests',
            'link' => 'contact-requests',
            'table_name' => 'contact_requests'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = ContactRequest::select([
            'id',
            'first_name',
            'last_name',
            'email',
            'mobile_number',
            'message',
            'time',
            'subject'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = ContactRequest::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new ContactRequest;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->email = $request->email;
        $row->mobile_number = $request->mobile_number;
        $row->message = $request->message;
        $row->time = $request->time;
        $row->subject = $request->subject;
        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = ContactRequest::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = ContactRequest::findOrFail($id);
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->email = $request->email;
        $row->mobile_number = $request->mobile_number;
        $row->message = $request->message;
        $row->time = $request->time;
        $row->subject = $request->subject;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        ContactRequest::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new ContactRequestsExport, 'contact_requests_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT ContactRequests
*/
class ContactRequestsExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return ContactRequest::select([
            'id',
            'first_name',
            'last_name',
            'email',
            'mobile_number',
            'message',
            'time',
            'subject'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'First_name',
            'Last_name',
            'Email',
            'Mobile Number',
            'message',
            'Time',
            'Subject'
        ];
    }
}
