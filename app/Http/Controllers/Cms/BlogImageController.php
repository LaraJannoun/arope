<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\BlogImage;
use App\Models\Blog;

use Storage;

class BlogImageController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Blog Images',
            'link' => 'blog-images',
            'table_name' => 'blog_images'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = BlogImage::select([
            'id',
            'image',
            'blog_id'
        ])->get();

        $blogs = Blog::select([
            'id',
            'slug',
            'title_en',
            'text_en',
            'title_ar',
            'text_ar',
            'date_ar',
            'date_en',
            'link',
            'page_section_id'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows', 'blogs'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = BlogImage::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();

        $blogs = Blog::select([
            'id',
            'slug',
            'title_en',
            'text_en',
            'title_ar',
            'text_ar',
            'date_ar',
            'date_en',
            'link',
            'page_section_id'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info', 'blogs'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $image_path = null;
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }

        $row = new BlogImage;
        $row->blog_id = $request->blog_id;
        $row->image = $image_path;
        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $blogs = Blog::select([
            'id',
            'slug',
            'title_en',
            'text_en',
            'title_ar',
            'text_ar',
            'date_ar',
            'date_en',
            'link',
            'page_section_id'
        ])->orderBy('pos')->get();

        $row = BlogImage::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row', 'blogs'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = BlogImage::findOrFail($id);

        // Check if the image exists
        $image_path = $row['image'];
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }

        $row->image = $image_path;
        $row->blog_id = $request->blog_id;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        BlogImage::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    /**
     * Remove Brochure function
     *
     */
    public function imageRemove($id)
    {
        $page_info = $this->page_info();

        $row = BlogImage::findOrFail($id);
        $row->image = null;
        $row->save();
        parent::add_log('imageRemove', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }
}
