<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\News;
use App\Models\PageSection;

class NewsController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'News',
            'link' => 'news',
            'table_name' => 'news'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = News::select([
            'id',
            'slug',
            'image',
            'link',
            'page_section_id',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'text_preview_en',
            'text_preview_ar',
            'date_en',
            'date_ar',
            'publish'
        ])->orderBy('pos')->get();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'header_title_en',
            'header_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'subtitle_en',
            'subtitle_ar'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows', 'page_sections'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = News::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'header_title_en',
            'header_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'subtitle_en',
            'subtitle_ar'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info', 'page_sections'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new News;
        $image_path = null;
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }
        $row->image = $image_path;
        $row->link = $request->link;
        $row->page_section_id = $request->page_section_id;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->text_en = $request->text_en;
        $row->text_ar = $request->text_ar;
        $row->date_en = $request->date_en;
        $row->date_ar = $request->date_ar;
        $row->text_preview_en = $request->text_preview_en;
        $row->text_preview_ar = $request->text_preview_ar;
        $row->slug = $request->slug;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'header_title_en',
            'header_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'subtitle_en',
            'subtitle_ar'
        ])->orderBy('pos')->get();

        $row = News::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row', 'page_sections'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = News::findOrFail($id);
        $image_path = $row['image'];
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }

        $row->image = $image_path;
        $row->link = $request->link;
        $row->page_section_id = $request->page_section_id;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->text_en = $request->text_en;
        $row->text_ar = $request->text_ar;
        $row->date_en = $request->date_en;
        $row->date_ar = $request->date_ar;
        $row->text_preview_en = $request->text_preview_en;
        $row->text_preview_ar = $request->text_preview_ar;
        $row->slug = $request->slug;

        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        News::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }


    /**
     * Publish a specified row
     *
     */
    public function publish(Request $request)
    {
        $id = $request['id'];
        $row = News::findOrFail($id);
        $row->publish = !$row->publish;
        $row->save();

        $page_info = $this->page_info();
        parent::add_log('publish', $page_info['link']);
    }

    /**
     * Show the form for ordering all rows
     *
     */
    public function order()
    {
        $page_info = $this->page_info();

        $rows = News::select([
            'id',
            'image'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.order', compact('page_info', 'rows'));
    }

    /**
     * Update the order for all rows in the database
     *
     */
    public function orderSubmit(Request $request)
    {
        $page_info = $this->page_info();

        foreach ($request->id as $key => $id) {
            $row = News::findOrFail($id);
            $row->pos = $request->pos[$key];
            $row->save();
        }
        parent::add_log('order', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Records successfully ordered.');
    }
}
