<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\HouseholdQuote;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class HouseholdQuoteController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Household Quotes',
            'link' => 'household-quotes',
            'table_name' => 'household_quotes'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = HouseholdQuote::select([
            'id',
            'first_name',
            'father_name',
            'landline_number',
            'occupation',
            'mobile_number',
            'email',
            'last_name',
            'property_address',
            'property_value',
            'property_type',
            'content_value'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = HouseholdQuote::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new HouseholdQuote;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->property_type = $request->property_type;
        $row->property_value = $request->property_value;
        $row->property_address = $request->property_address;
        $row->content_value = $request->content_value;


        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = HouseholdQuote::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = HouseholdQuote::findOrFail($id);
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->property_type = $request->property_type;
        $row->property_value = $request->property_value;
        $row->property_address = $request->property_address;
        $row->content_value = $request->content_value;

        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        HouseholdQuote::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new HouseholdQuotesExport, 'household_quotes_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT household QUOTES
*/
class HouseholdQuotesExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return HouseholdQuote::select([
            'id',
            'first_name',
            'father_name',
            'last_name',
            'email',
            'mobile_number',
            'landline_number',
            'occupation',
            'property_address',
            'property_value',
            'property_type'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'First Name',
            'Father Name',
            'Last Name',
            'Email',
            'Mobile Number',
            'Landline number',
            'Occupation',
            'Property Address',
            'Property Value',
            'Property Type'
        ];
    }
}
