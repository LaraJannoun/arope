<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Team;
use App\Models\PageSection;

class TeamController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Shareholders & Teams',
            'link' => 'team',
            'table_name' => 'team'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = Team::select([
            'id',
            'slug',
            'page_section_id',
            'image',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
        ])->get();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'header_title_en',
            'header_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'subtitle_en',
            'subtitle_ar'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows', 'page_sections'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = Team::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'header_title_en',
            'header_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'subtitle_en',
            'subtitle_ar'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info', 'page_sections'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new Team;
        $image_path = null;
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }
        $row->image = $image_path;
        $row->slug = $request->slug;
        $row->page_section_id = $request->page_section_id;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->text_en = $request->text_en;
        $row->text_ar = $request->text_ar;
        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'header_title_en',
            'header_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'subtitle_en',
            'subtitle_ar'
        ])->orderBy('pos')->get();
        $row = Team::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row', 'page_sections'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = Team::findOrFail($id);
        $image_path = $row['image'];
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }

        $row->image = $image_path;
        $row->page_section_id = $request->page_section_id;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->text_en = $request->text_en;
        $row->text_ar = $request->text_ar;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        Team::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }
}
