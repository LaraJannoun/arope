<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\MotorClaim;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class MotorClaimController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Motor Claims',
            'link' => 'motor-claims',
            'table_name' => 'motor_claims'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = MotorClaim::select('*')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = MotorClaim::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new MotorClaim;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = MotorClaim::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = MotorClaim::findOrFail($id);

        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        MotorClaim::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new MotorClaimApplicationsExport, 'motor_claims_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT motor claims
*/
class MotorClaimApplicationsExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return MotorClaim::select([
            'id',
            'pas_number',
            'plate_number',
            'plate_code',
            'driver_name',
            'mobile_number',
            'email',
            'communication',
            'accident_date',
            'accident_time',
            'accident_location',
            'expert',
            'expert_name',
            'bodily_injuries',
            'accident_description',
            'front_driving_license',
            'back_driving_license',
            'front_car_reg_id',
            'back_car_reg_id',
            'front_car_details',
            'back_car_details',
            'left_car_details',
            'right_car_details',
            'workshop',
            'data_privacy_confirm'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'Pas number',
            'Plate number',
            'Plate code',
            'Driver name',
            'Mobile number',
            'Email',
            'Communication',
            'Accident date',
            'Accident time',
            'Accident location',
            'Expert',
            'Expert name',
            'Bodily injuries',
            'Accident Description',
            'Front Driving license',
            'Back Driving license',
            'Front Car registration id',
            'Back Car registration id',
            'Front car details',
            'Back car details',
            'Left car details',
            'Right car details',
            'Workshop',
            'Data privacy confirm'
        ];
    }
}
