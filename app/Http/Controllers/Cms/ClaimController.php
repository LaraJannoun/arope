<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Claim;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class ClaimController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Claims',
            'link' => 'claims',
            'table_name' => 'claims'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = Claim::select([
            'id',
            'claim_type_id',
            'first_name',
            'last_name',
            'email',
            'fields',
            'mobile_number',
            'message'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = Claim::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new Claim;
        $row->claim_type_id = $request->claim_type_id;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->email = $request->email;
        $row->fields = $request->fields;
        $row->mobile_number = $request->mobile_number;
        $row->message = $request->message;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = Claim::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = Claim::findOrFail($id);
        $row->claim_type_id = $request->claim_type_id;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->email = $request->email;
        $row->fields = $request->fields;
        $row->mobile_number = $request->mobile_number;
        $row->message = $request->message;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        Claim::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new ClaimApplicationsExport, 'claims_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT claims
*/
class ClaimApplicationsExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return Claim::select([
            'id',
            'claim_type_id',
            'first_name',
            'last_name',
            'email',
            'fields',
            'mobile_number',
            'message'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'id',
            'claim_type_id',
            'first_name',
            'last_name',
            'email',
            'fields',
            'mobile_number',
            'message'
        ];
    }
}
