<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\HealthcareQuote;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class HealthcareQuoteController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Healthcare Quotes',
            'link' => 'healthcare-quotes',
            'table_name' => 'healthcare_quotes'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = HealthcareQuote::select([
            'id',
            'first_name',
            'father_name',
            'landline_number',
            'occupation',
            'mobile_number',
            'email',
            'hospital_network',
            'hospital_class',
            'last_name',
            'cover_type',
            'insurer_name',
            'insurer_relation',
            'insurer_year',
            'insurer_nssf',
            'optional_covers',
            'd_o_b'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = HealthcareQuote::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new HealthcareQuote;
        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->hospital_network = $request->hospital_network;
        $row->hospital_class = $request->hospital_class;
        $row->last_name = $request->last_name;
        $row->cover_type = $request->cover_type;
        $row->insurer_name = json_encode($request->insurer_name);
        $row->insurer_relation = json_encode($request->insurer_relation);
        $row->insurer_year = json_encode($request->insurer_year);
        $row->insurer_nssf = json_encode($request->insurer_nssf);
        $row->optional_covers = json_encode($request->optional_covers);
        $row->d_o_b = $request->d_o_b;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = HealthcareQuote::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = HealthcareQuote::findOrFail($id);
        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->hospital_network = $request->hospital_network;
        $row->hospital_class = $request->hospital_class;
        $row->last_name = $request->last_name;
        $row->cover_type = $request->cover_type;
        $row->insurer_name = json_encode($request->insurer_name);
        $row->insurer_relation = json_encode($request->insurer_relation);
        $row->insurer_year = json_encode($request->insurer_year);
        $row->insurer_nssf = json_encode($request->insurer_nssf);
        $row->optional_covers = json_encode($request->optional_covers);
        $row->d_o_b = $request->d_o_b;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        HealthcareQuote::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new HealthcareQuotesExport, 'healthcare_quotes_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT healthcare QUOTES
*/
class HealthcareQuotesExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return HealthcareQuote::select([
            'id',
            'first_name',
            'father_name',
            'last_name',
            'email',
            'mobile_number',
            'landline_number',
            'd_o_b',
            'occupation',
            'hospital_network',
            'hospital_class',
            'cover_type',
            'insurer_name',
            'insurer_relation',
            'insurer_year',
            'insurer_nssf',
            'optional_covers'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'First Name',
            'Father Name',
            'Last Name',
            'Email',
            'Mobile Number',
            'Landline number',
            'DOB',
            'Occupation',
            'Hospital Networking',
            'Hospital Class',
            'Cover Type',
            'Insurer Names',
            'Insurer Relations',
            'Insurer Years',
            'Insurers NSSF',
            'Optional Covers'
        ];
    }
}
