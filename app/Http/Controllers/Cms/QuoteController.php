<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Quote;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class QuoteController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Quotes',
            'link' => 'quotes',
            'table_name' => 'quotes'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = Quote::select([
            'id',
            'company_name',
            'contact_person',
            'mobile_number',
            'email',
            'business_type',
            'employees_nb',
            'message',
            'landline_number'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = Quote::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new Quote;
        $row->company_name = $request->company_name;
        $row->contact_person = $request->contact_person;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->business_type = $request->business_type;
        $row->employees_nb = $request->employees_nb;
        $row->message = $request->message;
        $row->landline_number = $request->landline_number;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = Quote::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = Quote::findOrFail($id);
        $row->company_name = $request->company_name;
        $row->contact_person = $request->contact_person;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->business_type = $request->business_type;
        $row->employees_nb = $request->employees_nb;
        $row->message = $request->message;
        $row->landline_number = $request->landline_number;

        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        Quote::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new QuotesExport, 'business_quotes_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT Business QUOTES
*/
class QuotesExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return Quote::select([
            'id',
            'company_name',
            'contact_person',
            'mobile_number',
            'landline_number',
            'email',
            'business_type',
            'employees_nb',
            'message',
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'Company Name',
            'Contact Person',
            'Mobile Number',
            'Landline Number',
            'Email',
            'Business Type',
            'Employees Number',
            'Message',
        ];
    }
}
