<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\EducationQuote;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class EducationQuoteController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Education Quotes',
            'link' => 'education-quotes',
            'table_name' => 'education_quotes'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = EducationQuote::select([
            'id',
            'first_name',
            'father_name',
            'landline_number',
            'occupation',
            'mobile_number',
            'email',
            'children_number',
            'protection_amount',
            'last_name',
            'd_o_b',
            'contribution_amount',
            'contribution_frequency',
            'insurer_dob',
            'insurer_name',
            'insurer_saving_age'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = EducationQuote::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new EducationQuote;

        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->children_number = $request->children_number;
        $row->protection_amount = $request->protection_amount;
        $row->last_name = $request->last_name;
        $row->d_o_b = $request->d_o_b;
        $row->contribution_amount = $request->contribution_amount;
        $row->contribution_frequency = $request->contribution_frequency;
        $row->insurer_name = json_encode($request->insurer_name);
        $row->insurer_dob = json_encode($request->insurer_dob);
        $row->insurer_saving_age = json_encode($request->insurer_saving_age);
        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = EducationQuote::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = EducationQuote::findOrFail($id);

        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->children_number = $request->children_number;
        $row->protection_amount = $request->protection_amount;
        $row->last_name = $request->last_name;
        $row->d_o_b = $request->d_o_b;
        $row->contribution_amount = $request->contribution_amount;
        $row->contribution_frequency = $request->contribution_frequency;
        $row->insurer_name = json_encode($request->insurer_name);
        $row->insurer_dob = json_encode($request->insurer_dob);
        $row->insurer_saving_age = json_encode($request->insurer_saving_age);
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        EducationQuote::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new EducationQuotesExport, 'education_quotes_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT Education Quote
*/
class EducationQuotesExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return EducationQuote::select([
            'id',
            'first_name',
            'father_name',
            'last_name',
            'email',
            'mobile_number',
            'landline_number',
            'occupation',
            'children_number',
            'protection_amount',
            'd_o_b',
            'contribution_amount',
            'contribution_frequency',
            'insurer_name',
            'insurer_dob',
            'insurer_saving_age'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'First Name',
            'Father Name',
            'Last Name',
            'Email',
            'Mobile Number',
            'Landline number',
            'Occupation',
            'Number of Children',
            'Protection Amount',
            'Date of Birth',
            'Contribution Amount',
            'Contribution Frequency',
            'Insurer Names',
            'Insurer DOBs',
            'Insurer Saving Ages'
        ];
    }
}
