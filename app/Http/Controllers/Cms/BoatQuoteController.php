<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\BoatQuote;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class BoatQuoteController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Boat Quotes',
            'link' => 'boat-quotes',
            'table_name' => 'boat_quotes'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = BoatQuote::select([
            'id',
            'first_name',
            'father_name',
            'landline_number',
            'occupation',
            'mobile_number',
            'email',
            'last_name',
            'navigation_limit',
            'other_navigation_limit',
            'year_of_make',
            'basic_cover',
            'vessel_name',
            'optional_cover'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = BoatQuote::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new BoatQuote;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->navigation_limit = $request->navigation_limit;
        $row->year_of_make = $request->year_of_make;
        $row->basic_cover = $request->basic_cover;
        $row->vessel_name = $request->vessel_name;
        $row->other_navigation_limit = $request->other_navigation_limit;
        $row->optional_cover = $request->optional_cover;

        $row->save();
        parent::add_log('inser', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = BoatQuote::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = BoatQuote::findOrFail($id);
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->navigation_limit = $request->navigation_limit;
        $row->year_of_make = $request->year_of_make;
        $row->basic_cover = $request->basic_cover;
        $row->vessel_name = $request->vessel_name;
        $row->other_navigation_limit = $request->other_navigation_limit;
        $row->optional_cover = $request->optional_cover;

        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        BoatQuote::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);
        return Excel::download(new BoatQuotesExport, 'boat_quotes_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT BOAT QUOTES
*/
class BoatQuotesExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return BoatQuote::select([
            'id',
            'first_name',
            'father_name',
            'last_name',
            'email',
            'mobile_number',
            'landline_number',
            'occupation',
            'vessel_name',
            'navigation_limit',
            'other_navigation_limit',
            'year_of_make',
            'basic_cover',
            'optional_cover'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'First Name',
            'Father Name',
            'Last Name',
            'Email',
            'Mobile Number',
            'Landline number',
            'Occupation',
            'Vessel Name',
            'Navigation Limit',
            'Other Navigation Limit',
            'Year Of Make',
            'Basic Cover',
            'Optional Cover'
        ];
    }
}
