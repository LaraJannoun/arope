<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Map;

class MapController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Maps',
            'link' => 'maps',
            'table_name' => 'maps'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = Map::select([
            'id',
            'lat',
            'lng',
            'main_title_en',
            'main_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'phone_en',
            'phone_ar',
            'fax_en',
            'fax_ar',
            'email_en',
            'email_ar',
            'link',
            'publish'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = Map::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new Map;
        $row->lat = $request->lat;
        $row->lng = $request->lng;
        $row->main_title_en = $request->main_title_en;
        $row->main_title_ar = $request->main_title_ar;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->text_en = $request->text_en;
        $row->text_ar = $request->text_ar;
        $row->phone_en = $request->phone_en;
        $row->phone_ar = $request->phone_ar;
        $row->fax_en = $request->fax_en;
        $row->fax_ar = $request->fax_ar;
        $row->email_en = $request->email_en;
        $row->email_ar = $request->email_ar;
        $row->link = $request->link;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = Map::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = Map::findOrFail($id);
        $row->lat = $request->lat;
        $row->lng = $request->lng;
        $row->main_title_en = $request->main_title_en;
        $row->main_title_ar = $request->main_title_ar;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->text_en = $request->text_en;
        $row->text_ar = $request->text_ar;
        $row->phone_en = $request->phone_en;
        $row->phone_ar = $request->phone_ar;
        $row->fax_en = $request->fax_en;
        $row->fax_ar = $request->fax_ar;
        $row->email_en = $request->email_en;
        $row->email_ar = $request->email_ar;
        $row->link = $request->link;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        Map::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }


    /**
     * Publish a specified row
     *
     */
    public function publish(Request $request)
    {
        $id = $request['id'];
        $row = Map::findOrFail($id);
        $row->publish = !$row->publish;
        $row->save();

        $page_info = $this->page_info();
        parent::add_log('publish', $page_info['link']);
    }

    /**
     * Show the form for ordering all rows
     *
     */
    public function order()
    {
        $page_info = $this->page_info();

        $rows = Map::select([
            'id',
            'title_en'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.order', compact('page_info', 'rows'));
    }

    /**
     * Update the order for all rows in the database
     *
     */
    public function orderSubmit(Request $request)
    {
        $page_info = $this->page_info();

        foreach ($request->id as $key => $id) {
            $row = Map::findOrFail($id);
            $row->pos = $request->pos[$key];
            $row->save();
        }
        parent::add_log('order', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Records successfully ordered.');
    }
}
