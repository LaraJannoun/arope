<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\CareerApplication;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;
use Mail;

class CareerApplicationController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Career Applications',
            'link' => 'career-applications',
            'table_name' => 'career_applications'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = CareerApplication::select([
            'id',
            'first_name',
            'family_name',
            'father_name',
            'd_o_b',
            'first_nationality' ,
            'second_nationality' ,
            'gender' ,
            'civil_status',
            'kids_number' ,
            'mobile_number' ,
            'landline' ,
            'email_address',
            'country' ,
            'city' ,
            'area' ,
            'street' ,
            'blg_floor',

            'university',
            'extra_university' ,
            'education_level',
            'area_of_study' ,
            'is_graduated' ,
            'graduation_year',

            'employment_status' ,
            'work_from_date',
            'work_to_date',
            'monthly_salary' ,
            'experience_year' ,
            'company_name' ,
            'benefit',
            'job_title' ,
            'reason_of_leaving' ,
            'job_description',

            'employment_interest',
            'illness',
            'start_date',
            'medical_info' ,
            'referer',
            'family_employed',
            'job_details',

            'resume' ,
            'cover' ,
            'agreement',
            'created_at'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = CareerApplication::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        CareerApplication::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new CareerApplicationsExport, 'career_applictions_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT career applications
*/
class CareerApplicationsExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return CareerApplication::select([
            'id',
            'first_name',
            'family_name',
            'father_name',
            'd_o_b',
            'first_nationality' ,
            'second_nationality' ,
            'gender' ,
            'civil_status',
            'kids_number' ,
            'mobile_number' ,
            'landline' ,
            'email_address',
            'country' ,
            'city' ,
            'area' ,
            'street' ,
            'blg_floor',

            'university',
            'extra_university' ,
            'education_level',
            'area_of_study' ,
            'is_graduated' ,
            'graduation_year',

            'employment_status' ,
            'work_from_date',
            'work_to_date',
            'monthly_salary' ,
            'experience_year' ,
            'company_name' ,
            'benefit',
            'job_title' ,
            'reason_of_leaving' ,
            'job_description',

            'employment_interest',
            'illness',
            'start_date',
            'medical_info' ,
            'referer',
            'family_employed',
            'job_details',

            'agreement',
            'created_at'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'First Name',
            'Family Name',
            'Father Name',
            'D O B',
            'First Nationality' ,
            'Second Nationality' ,
            'Gender' ,
            'Civil Status',
            'Kids Number' ,
            'Mobile Number' ,
            'Landline' ,
            'Email Address',
            'Country' ,
            'City' ,
            'Area' ,
            'Street' ,
            'Blg Floor',

            'University',
            'Extra University' ,
            'Education Level',
            'Area Of Study' ,
            'Is Graduated' ,
            'Graduation Year',

            'Employment Status' ,
            'Work From Date',
            'Work To Date',
            'Monthly Salary' ,
            'Experience Year' ,
            'Company Name' ,
            'Benefit',
            'Job Title' ,
            'Reason Of Leaving' ,
            'Job Description',

            'Employment Interest',
            'Illness',
            'Start Date',
            'Medical Info' ,
            'Referer',
            'Family Employed',
            'Job Details',


            'Agreement',
            'Created At'
        ];
    }
}
