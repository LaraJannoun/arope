<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Models\BoatQuote;
use Illuminate\Support\Carbon;
use App\Models\ContactRequest;
use App\Models\BusinessSolution;
use App\Models\CareerApplication;
use App\Models\Claim;
use App\Models\EducationQuote;
use App\Models\ExpatQuote;
use App\Models\HealthcareQuote;
use App\Models\HouseholdQuote;
use App\Models\Log;
use App\Models\MotorClaim;
use App\Models\MotorQuote;
use App\Models\PersonalQuote;
use App\Models\Problem;
use App\Models\Quote;
use App\Models\RetirementQuote;
use App\Models\Subscriber;
use App\Models\TermLifeQuote;
use App\Models\TravelQuote;

class DashboardController extends Controller
{

    public function index()
    {
        /**
         * General Constant
         *
         */

        $today = Carbon::now();

        /**
         * Items Widgets
         *
         */

        // Get published items
        $items_published = BusinessSolution::where('publish', 1)->count();
        // Get unpublished items
        $items_unpublished = BusinessSolution::where('publish', 0)->count();
        // Get last article date
        $last_article = BusinessSolution::select('created_at')->where('publish', 1)->orderBy('created_at', 'desc')->first();
        $items_interval_days = null;
        if ($last_article) {
            $last_article_created_at = $last_article->created_at;
            $items_interval_days = $last_article_created_at->diff($today)->format('%a');
        }

        $logs = Log::select('*')->get();


        /**
         * Form & Submissions
         *
         */

        $contact_form_count = ContactRequest::count();
        $subscriber_count = Subscriber::count();
        $career_form_count = CareerApplication::count();

        $claim_count = Claim::count();
        $motor_claim_count = MotorClaim::count();
        $problem_count = Problem::count();
        $business_quote_count = Quote::count();
        $personal_quote_count = PersonalQuote::count();
        $travel_quote_count = TravelQuote::count();
        $expat_quote_count = ExpatQuote::count();
        $term_life_quote_count = TermLifeQuote::count();
        $healthcare_quote_count = HealthcareQuote::count();
        $household_quote_count = HouseholdQuote::count();
        $education_quote_count = EducationQuote::count();
        $retirement_quote_count = RetirementQuote::count();
        $motor_quote_count = MotorQuote::count();
        $boat_quote_count = BoatQuote::count();

        return view('cms.pages.dashboard', compact(
            'items_published',
            'items_unpublished',
            'items_interval_days',
            'contact_form_count',
            'subscriber_count',
            'career_form_count',
            'claim_count',
            'motor_claim_count',
            'problem_count',
            'personal_quote_count',
            'travel_quote_count',
            'expat_quote_count',
            'term_life_quote_count',
            'healthcare_quote_count',
            'household_quote_count',
            'education_quote_count',
            'retirement_quote_count',
            'motor_quote_count',
            'boat_quote_count',
            'business_quote_count',
            'logs'
        ));
    }
}
