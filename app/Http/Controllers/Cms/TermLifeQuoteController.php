<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\TermLifeQuote;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class TermLifeQuoteController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Term Life Quotes',
            'link' => 'term-life-quotes',
            'table_name' => 'term_life_quotes'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = TermLifeQuote::select([
            'id',
            'first_name',
            'father_name',
            'last_name',
            'occupation',
            'email',
            'mobile_number',
            'landline_number',
            'year_of_birth',
            'sum_insured',
            'other_sum_insured',
            'policy_duration',
            'cover',
            'currency'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = TermLifeQuote::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new TermLifeQuote;
        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->last_name = $request->last_name;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->landline_number = $request->landline_number;
        $row->year_of_birth = $request->year_of_birth;
        $row->email = $request->email;
        $row->sum_insured = $request->sum_insured;
        $row->other_sum_insured = $request->other_sum_insured ? $request->other_sum_insured : '';
        $row->cover = $request->cover;
        $row->policy_duration = $request->policy_duration;
        $row->currency = $request->currency;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = TermLifeQuote::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = TermLifeQuote::findOrFail($id);
        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->last_name = $request->last_name;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->landline_number = $request->landline_number;
        $row->year_of_birth = $request->year_of_birth;
        $row->email = $request->email;
        $row->sum_insured = $request->sum_insured;
        $row->other_sum_insured = $request->other_sum_insured ? $request->other_sum_insured : '';
        $row->cover = $request->cover;
        $row->policy_duration = $request->policy_duration;
        $row->currency = $request->currency;

        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        TermLifeQuote::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new TermLifeQuotesExport, 'termlife_quotes_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT termlife QUOTES
*/
class TermLifeQuotesExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return TermLifeQuote::select([
            'id',
            'first_name',
            'father_name',
            'last_name',
            'email',
            'mobile_number',
            'landline_number',
            'occupation',
            'year_of_birth',
            'sum_insured',
            'other_sum_insured',
            'policy_duration',
            'cover', 'currency'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'First Name',
            'Father Name',
            'Last Name',
            'Email',
            'Mobile Number',
            'Landline number',
            'Occupation',
            'Year Of Birth',
            'Sum Insured',
            'Other Sum Insured',
            'Policy Duration',
            'Cover',
            'Currency'
        ];
    }
}
