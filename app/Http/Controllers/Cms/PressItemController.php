<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\PressItem;
use App\Models\PressHeader;

class PressItemController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Press Items',
            'link' => 'press-items',
            'table_name' => 'press_items'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = PressItem::select([
            'id',
            'slug',
            'attachment',
            'link',
            'press_header_id',
            'title_en',
            'title_ar',
            'date_en',
            'date_ar',
            'publish'
        ])->orderBy('pos')->get();

        $press_headers =
            PressHeader::select([
                'id',
                'title_en',
                'title_ar'
            ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows', 'press_headers'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = PressItem::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        $press_headers =
            PressHeader::select([
                'id',
                'title_en',
                'title_ar'
            ])->orderBy('pos')->get();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info', 'press_headers'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new PressItem;
        $image_path = null;
        if ($request->attachment) {
            $this->validate($request, [
                'attachment' => 'required|mimes:pdf'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->attachment);
        }
        $row->attachment = $image_path;
        $row->link = $request->link;
        $row->press_header_id = $request->press_header_id;
        $row->slug = $request->slug;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->date_en = $request->date_en;
        $row->date_ar = $request->date_ar;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();
        $press_headers =
            PressHeader::select([
                'id',
                'title_en',
                'title_ar'
            ])->orderBy('pos')->get();
        $row = PressItem::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row', 'press_headers'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = PressItem::findOrFail($id);
        $image_path = $row['attachment'];
        if ($request->attachment) {
            $this->validate($request, [
                'attachment' => 'required|mimes:pdf'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->attachment);
        }

        $row->attachment = $image_path;
        $row->link = $request->link;
        $row->press_header_id = $request->press_header_id;
        $row->title_en = $request->title_en;
        $row->title_ar = $request->title_ar;
        $row->date_en = $request->date_en;
        $row->date_ar = $request->date_ar;

        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        PressItem::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }


    /**
     * Publish a specified row
     *
     */
    public function publish(Request $request)
    {
        $id = $request['id'];
        $row = PressItem::findOrFail($id);
        $row->publish = !$row->publish;
        $row->save();

        $page_info = $this->page_info();
        parent::add_log('publish', $page_info['link']);
    }

    /**
     * Show the form for ordering all rows
     *
     */
    public function order()
    {
        $page_info = $this->page_info();

        $rows = PressItem::select([
            'id',
            'slug'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.order', compact('page_info', 'rows'));
    }

    /**
     * Update the order for all rows in the database
     *
     */
    public function orderSubmit(Request $request)
    {
        $page_info = $this->page_info();

        foreach ($request->id as $key => $id) {
            $row = PressItem::findOrFail($id);
            $row->pos = $request->pos[$key];
            $row->save();
        }
        parent::add_log('order', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Records successfully ordered.');
    }
}
