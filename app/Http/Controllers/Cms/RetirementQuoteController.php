<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\RetirementQuote;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Carbon;

use Excel;

class RetirementQuoteController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Retirement Quotes',
            'link' => 'retirement-quotes',
            'table_name' => 'retirement_quotes'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = RetirementQuote::select([
            'id',
            'first_name',
            'father_name',
            'landline_number',
            'occupation',
            'mobile_number',
            'email',
            'last_name',
            'contribution_frequency',
            'retirement_age',
            'retirement_amount',
            'protection_amount',
            'd_o_b',
            'contribution_amount'
        ])->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = RetirementQuote::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new RetirementQuote;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->contribution_frequency = $request->contribution_frequency;
        $row->retirement_age = $request->retirement_age;
        $row->retirement_amount = $request->retirement_amount;
        $row->d_o_b = $request->d_o_b;
        $row->protection_amount = $request->protection_amount;
        $row->contribution_amount = $request->contribution_amount;

        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = RetirementQuote::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = RetirementQuote::findOrFail($id);
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->contribution_frequency = $request->contribution_frequency;
        $row->retirement_age = $request->retirement_age;
        $row->retirement_amount = $request->retirement_amount;
        $row->d_o_b = $request->d_o_b;
        $row->protection_amount = $request->protection_amount;
        $row->contribution_amount = $request->contribution_amount;

        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        RetirementQuote::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    public function export()
    {
        $page_info = $this->page_info();
        parent::add_log('export', $page_info['link']);

        return Excel::download(new RetirementQuotesExport, 'retirement_quotes_export_' . Carbon::now()->format('Y_m_d') . '.xls');
    }
}

/*
* EXPORT retirement QUOTES
*/
class RetirementQuotesExport implements FromCollection, WithHeadings
{

    function collection()
    {
        return RetirementQuote::select([
            'id',
            'first_name',
            'father_name',
            'last_name',
            'email',
            'mobile_number',
            'landline_number',
            'occupation',
            'contribution_frequency',
            'retirement_age',
            'retirement_amount',
            'protection_amount',
            'd_o_b',
            'contribution_amount'
        ])->get();
    }

    public function headings(): array
    {
        return [
            'Id',
            'First Name',
            'Father Name',
            'Last Name',
            'Email',
            'Mobile Number',
            'Landline number',
            'Occupation',
            'Contribution Frequency',
            'Retirement Age',
            'Retirement Amount',
            'Protection Amount',
            'Date Of Birth',
            'Contribution Amount'
        ];
    }
}
