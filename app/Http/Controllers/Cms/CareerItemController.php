<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\CareerItem;

class CareerItemController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Career Items',
            'link' => 'career-items',
            'table_name' => 'career_items'
        ];
        return $page_info;
    }

    /**
     * Display a Iteming of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = CareerItem::select([
            'id',
            'slug',
            'image',
            'title_en',
            'text_en',
            'title_ar',
            'text_ar',
            'points_en',
            'points_ar',
            'publish',
        ])->orderBy('slug')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows'));
    }

    /**
     * Display a Iteming of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = CareerItem::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();
        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $row = new CareerItem;
        $image_path = null;
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }
        $row->image = $image_path;
        $row->title_en = $request->title_en;
        $row->text_en = $request->text_en;
        $row->title_ar = $request->title_ar;
        $row->text_ar = $request->text_ar;
        $row->points_en = $request->points_en;
        $row->points_ar = $request->points_ar;
        $row->slug = $request->slug;
        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $row = CareerItem::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = CareerItem::findOrFail($id);
        $row->title_en = $request->title_en;
        $row->text_en = $request->text_en;
        $row->title_ar = $request->title_ar;
        $row->text_ar = $request->text_ar;
        $row->points_en = $request->points_en;
        $row->points_ar = $request->points_ar;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        CareerItem::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }


    /**
     * Publish a specified row
     *
     */
    public function publish(Request $request)
    {
        $page_info = $this->page_info();

        $id = $request['id'];
        $row = CareerItem::findOrFail($id);
        $row->publish = !$row->publish;
        $row->save();
        parent::add_log('publish', $page_info['link']);
    }
}
