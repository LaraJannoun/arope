<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\ItemImage;
use App\Models\Item;
use App\Models\PageSection;

use Storage;

class ItemImageController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Item Images',
            'link' => 'item-images',
            'table_name' => 'item_images'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = ItemImage::select([
            'id',
            'image',
            'item_id',
            'page_section_id'
        ])->get();

        $items = Item::select([
            'id',
            'slug',
            'page_section_id',
            'image',
            'title_en',
            'title_ar',
            'text_preview_en',
            'text_preview_ar',
            'text_en',
            'title_ar',
            'link',
            'apply_link',
            'featured'
        ])->get();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'header_title_en',
            'header_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'subtitle_en',
            'subtitle_ar'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows', 'items', 'page_sections'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = ItemImage::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();

        $items = Item::select([
            'id',
            'slug',
            'page_section_id',
            'image',
            'title_en',
            'title_ar',
            'text_preview_en',
            'text_preview_ar',
            'text_en',
            'title_ar',
            'link',
            'apply_link',
            'featured'
        ])->get();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'header_title_en',
            'header_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'subtitle_en',
            'subtitle_ar'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info', 'items', 'page_sections'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $image_path = null;
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }

        $row = new ItemImage;
        $row->item_id = $request->item_id;
        $row->page_section_id = $request->page_section_id;

        $row->image = $image_path;
        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $items = Item::select([
            'id',
            'slug',
            'page_section_id',
            'image',
            'title_en',
            'title_ar',
            'text_preview_en',
            'text_preview_ar',
            'text_en',
            'title_ar',
            'link',
            'apply_link',
        ])->get();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'header_title_en',
            'header_title_ar',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'subtitle_en',
            'subtitle_ar'
        ])->orderBy('pos')->get();

        $row = ItemImage::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row', 'items', 'page_sections'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = ItemImage::findOrFail($id);

        // Check if the image exists
        $image_path = $row['image'];
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }

        $row->image = $image_path;
        $row->item_id = $request->item_id;
        $row->page_section_id = $request->page_section_id;

        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        ItemImage::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    /**
     * Remove Brochure function
     *
     */
    public function imageRemove($id)
    {
        $page_info = $this->page_info();

        $row = ItemImage::findOrFail($id);
        $row->image = null;
        $row->save();
        parent::add_log('imageRemove', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }
}
