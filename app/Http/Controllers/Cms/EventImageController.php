<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\EventImage;
use App\Models\Event;

use Storage;

class EventImageController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'title' => 'Event Images',
            'link' => 'event-images',
            'table_name' => 'event_images'
        ];
        return $page_info;
    }

    /**
     * Display a listing of the Table
     *
     */
    public function index()
    {
        $page_info = $this->page_info();

        $rows = EventImage::select([
            'id',
            'image',
            'event_id'
        ])->get();

        $events = Event::select([
            'id',
            'slug',
            'image',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'date_en',
            'date_ar',
            'link',
            'page_section_id'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.index', compact('page_info', 'rows', 'events'));
    }

    /**
     * Display a listing of the specified row
     *
     */
    public function show($id)
    {
        $page_info = $this->page_info();

        $row = EventImage::findOrFail($id);

        return view('cms.pages.' . $page_info['link'] . '.show', compact('page_info', 'row'));
    }

    /**
     * Show the form for creating a new row
     *
     */
    public function create()
    {
        $page_info = $this->page_info();

        $events = Event::select([
            'id',
            'slug',
            'image',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'date_en',
            'date_ar',
            'link',
            'page_section_id'
        ])->orderBy('pos')->get();

        return view('cms.pages.' . $page_info['link'] . '.create', compact('page_info', 'events'));
    }

    /**
     * Store a newly created row in the database
     *
     */
    public function store(Request $request)
    {
        $page_info = $this->page_info();

        $image_path = null;
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }

        $row = new EventImage;
        $row->event_id = $request->event_id;
        $row->image = $image_path;
        $row->save();
        parent::add_log('insert', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully created.');
    }

    /**
     * Show the form for editing the specified row
     *
     */
    public function edit($id)
    {
        $page_info = $this->page_info();

        $events = Event::select([
            'id',
            'slug',
            'image',
            'title_en',
            'title_ar',
            'text_en',
            'text_ar',
            'date_en',
            'date_ar',
            'link',
            'page_section_id'
        ])->orderBy('pos')->get();

        $row = EventImage::findOrFail($id);
        return view('cms.pages.' . $page_info['link'] . '.edit', compact('page_info', 'row', 'events'));
    }

    /**
     * Update the specified row in the database
     *
     */
    public function update(Request $request, $id)
    {
        $page_info = $this->page_info();

        $row = EventImage::findOrFail($id);

        // Check if the image exists
        $image_path = $row['image'];
        if ($request->image) {
            $this->validate($request, [
                'image' => 'required|mimes:png,jpg,jpeg,svg|max:2000'

            ]);
            $image_path = parent::store_file($page_info['link'], $request->image);
        }

        $row->image = $image_path;
        $row->event_id = $request->event_id;
        $row->save();
        parent::add_log('update', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully updated.');
    }

    /**
     * Remove the specified row from the database
     *
     */
    public function destroy($id)
    {
        $page_info = $this->page_info();

        EventImage::findOrFail($id)->delete();
        parent::add_log('delete', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }

    /**
     * Remove Brochure function
     *
     */
    public function imageRemove($id)
    {
        $page_info = $this->page_info();

        $row = EventImage::findOrFail($id);
        $row->image = null;
        $row->save();
        parent::add_log('imageRemove', $page_info['link']);

        return redirect()->route('admin.' . $page_info['link'] . '.index')->withStatus('Record successfully deleted.');
    }
}
