<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ClaimType;
use App\Models\ClaimProcedure;
use App\Models\Page;
use App\Models\Claim;

use Illuminate\Http\Request;
use Mail;

class ClaimController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Claim';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'claim')->first();

        $types = ClaimType::select([
            'id',
            'title',
            'display_title_' . $locale . ' as display_title',
        ])->where('title', 'type')->get();

        $claim_procedures = ClaimProcedure::select([
            'id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->get();

        return view('web/pages/claim', compact(
            'page',
            'page_title',
            'types',
            'claim_procedures'
        ));
    }

    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'value' => 'required',
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email',
            'message' => 'required',
            'mobile_number' => 'required|numeric',
            'fields' => 'required',
            'g-recaptcha-response' => 'required'
        ]);

        $row = new Claim;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->fields = $request->fields;
        $row->email = $request->email;
        $row->mobile_number = $request->mobile_number;
        $row->message = $request->message;
        $row->claim_type_id = $request->value;
        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.claimHere',
                [
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'fields' => $request->fields,
                    'mobile_number' => $request->mobile_number,
                    'claim_type_id' => $request->value,
                    'email' => $request->email,
                    'message' => $request->message,
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Claim Here Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('claim', $locale)->withStatus('Claim successfully sent.');
    }
}
