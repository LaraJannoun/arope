<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\PersonalQuote;
use App\Models\Page;

use Illuminate\Http\Request;
use Mail;

class PersonalQuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Personal quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'personal-quote')->first();

        return view('web/pages/personal-quote', compact(
            'page_title',
            'page'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'father_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'occupation' => 'required|min:2',
            'email' => 'required|email',
            'mobile_number' => 'required|numeric',
            'd_o_b' => 'required',
            'sum_insured' => 'required',
            'other_sum' => 'required_if:sum_insured,other',
            'cover' => 'required',
            'currency' => 'required',
            'g-recaptcha-response' => 'required'


        ]);

        $row = new PersonalQuote;
        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->last_name = $request->last_name;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->landline_number = $request->landline_number;
        $row->year_of_birth = $request->d_o_b;
        $row->email = $request->email;
        $row->sum_insured = $request->sum_insured;
        $row->other_sum = $request->other_sum;
        $row->cover = $request->cover;
        $row->currency = $request->currency;

        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'A personal quote is requested from Arope Website',
                    'param' => [
                        'first_name',
                        'father_name',
                        'last_name',
                        'email',
                        'mobile_number',
                        'landline_number',
                        'occupation',
                        'd_o_b',
                        'sum_insured',
                        'cover',
                        'currency'
                    ]
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Request A Personal Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('personal-quote', $locale)->withStatus('Quote successfully requested.');
    }
}
