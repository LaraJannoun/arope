<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\FixedSection;

class DisclaimerController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Disclaimers';
        $disclaimers = FixedSection::select([
            'id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('slug', 'disclaimers')
            ->first();

        return view('web/pages/disclaimers', compact(
            'page_title',
            'disclaimers'
        ));
    }
}
