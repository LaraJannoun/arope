<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\RetirementQuote;
use App\Models\Page;

use Illuminate\Http\Request;
use Mail;

class RetirementQuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'AOL Retirement quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'retirement-quote')->first();

        return view('web/pages/retirement-quote', compact(
            'page_title',
            'page'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'father_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email',
            'mobile_number' => 'required|numeric',
            'occupation' => 'required',
            'contribution_frequency' => 'required',
            'retirement_age' => 'required|numeric',
            'retirement_amount' => 'required|numeric',
            'd_o_b' => 'required',
            'protection_amount' => 'required|numeric',
            'contribution_amount' => 'required|numeric',
            'g-recaptcha-response' => 'required'
        ]);

        $row = new RetirementQuote;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->contribution_frequency = $request->contribution_frequency;
        $row->retirement_age = $request->retirement_age;
        $row->retirement_amount = $request->retirement_amount;
        $row->d_o_b = $request->d_o_b;
        $row->protection_amount = $request->protection_amount;
        $row->contribution_amount = $request->contribution_amount;

        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'A retirement quote is requested from Arope Website',
                    'param' => [
                        'first_name',
                        'father_name',
                        'last_name',
                        'email',
                        'mobile_number',
                        'landline_number',
                        'occupation',
                        'contribution_frequency',
                        'retirement_age',
                        'retirement_amount',
                        'protection_amount',
                        'd_o_b',
                        'contribution_amount'
                    ]
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Request A Retirement Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('retirement-quote', $locale)->withStatus('Quote successfully requested.');
    }
}
