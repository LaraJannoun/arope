<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\PageSection;
use App\Models\BusinessSolution;
use App\Models\Item;
use App\Models\Testimonial;
use App\Models\ItemImage;
use App\Models\Banner;

use Illuminate\Http\Request;

class SolutionsBusinessController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Business Solutions';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'business-solutions')->first();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'subtitle_' . $locale . ' as subtitle',
            'header_title_' . $locale . ' as header_title'
        ])->where('publish', 1)->where('page_id', $page->id)->get();

        $business_solutions = BusinessSolution::select([
            'id',
            'image',
            'link',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'home_featured',
            'page_section_id',
            'apply_link'
        ])->where('publish', 1)->orderBy('pos')->get();

        $items = Item::select([
            'id',
            'slug',
            'page_section_id',
            'image',
            'title_' . $locale . ' as title',
            'text_preview_' . $locale . ' as text_preview',
            'text_' . $locale . ' as text',
            'link',
            'apply_link',
            'featured'
        ])->where('publish', 1)->get();

        $testimonials = Testimonial::select([
            'id',
            'image',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->orderBy('pos')->get();

        return view('web/pages/solutions-business', compact(
            'page_title',
            'page',
            'page_sections',
            'business_solutions',
            'items',
            'testimonials'
        ));
    }


    public function single($locale, $slug)
    {

        $page_section = request('page_section');

        $page_title = 'Solution Details - ' . $slug;
        $featured_item = Item::select([
            'id',
            'slug',
            'image',
            'link',
            'apply_link',
            'page_section_id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'subtitle_' . $locale . ' as subtitle',
            'text_preview_' . $locale . ' as text_preview'
        ])->where('publish', 1)->where('slug', $slug)->first();

        $featured_item_images = ItemImage::select([
            'id',
            'image',
            'item_id',
            'page_section_id'
        ])->where('item_id', $featured_item->id)->get();

        $banner = Banner::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'button_link',
            'button_text_' . $locale . ' as button_text',
        ])->where('slug', 'solutions-banner')->first();

        return view('web/pages/solution-item-details', compact(
            'page_title',
            'page_section',
            'slug',
            'featured_item',
            'featured_item_images',
            'banner'
        ));
    }
}
