<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\PageSection;
use App\Models\CareerItem;
use App\Models\CareerPosition;
use App\Models\CareerList;
use App\Models\CareerApplication;

use Illuminate\Http\Request;
use Validator;
use Mail;


class CareersController extends Controller
{

    public function page_info()
    {
        $page_info = [
            'link' => 'Careers',
        ];
        return $page_info;
    }

    public function index($locale)
    {

        $page_title = 'Careers';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'careers')->first();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'subtitle_' . $locale . ' as subtitle',
            'header_title_' . $locale . ' as header_title'
        ])->where('publish', 1)->where('page_id', $page->id)->get();


        $career_items = CareerItem::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'points_' . $locale . ' as points'
        ])->where('publish', 1)->get();


        $career_section_1 = $career_items->filter(function ($value, $key) {
            return $value['slug'] == 'careers-hr';
        })->first();

        $application_processes = $career_items->filter(function ($value, $key) {
            return $value['slug'] == 'application-process';
        });

        $career_top_message = $career_items->filter(function ($value, $key) {
            return $value['slug'] == 'career-top-message';
        })->first();

        $career_bottom_message = $career_items->filter(function ($value, $key) {
            return $value['slug'] == 'career-bottom-message';
        })->first();


        $career_positions = CareerPosition::select([
            'id',
            'slug',
            'image',
            'position',
            'country',
            'first_description',
            'second_description',
            'publish',
        ])->orderBy('pos')->get();

        $career_lists = CareerList::select([
            'id',
            'title',
            'display_title_' . $locale . ' as display_title',
            'type',
        ])->get();

        $genders = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'gender';
        });

        $nationalities1 = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'nationality';
        });

        $nationalities2 = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'nationality';
        });

        $countries = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'country';
        });

        $cities = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'city';
        });

        $areas = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'area';
        });

        $kids_number = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'kids_number';
        });

        $universities = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'university';
        });

        $education_levels = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'education_level';
        });

        $area_of_studies = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'area_of_study';
        });

        $graduated = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'graduated';
        });

        $employment_statuses = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'employment_status';
        });

        $experience_years = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'experience_years';
        });

        $monthly_salaries = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'monthly_salary';
        });

        $benefits = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'benefit';
        });

        $reason_of_leaving = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'reason_of_leaving';
        });

        $referers = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'referer';
        });

        $employment_interests = $career_lists->filter(function ($value, $key) {
            return $value['type'] == 'employment_interest';
        });

        return view('web/pages/careers', compact(
            'page_title',
            'page',
            'page_sections',
            'career_section_1',
            'application_processes',
            'career_positions',
            'career_top_message',
            'career_bottom_message',
            'genders',
            'nationalities1',
            'nationalities2',
            'countries',
            'cities',
            'areas',
            'kids_number',
            'universities',
            'education_levels',
            'area_of_studies',
            'graduated',
            'employment_statuses',
            'experience_years',
            'monthly_salaries',
            'benefits',
            'referers',
            'reason_of_leaving',
            'employment_interests'
        ));
    }

    /**
     * Submit request
     *
     */
    /*
    public function validatePersonalInfo(Request $request, $locale)
    {
        request()->session()->forget('personal_data');
        request()->session()->forget('valid_personal_data');
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|min:2',
            'family_name' => 'required|min:2',
            'gender_id' => 'required',
            'civil_status' => 'required',
            'country_id' => 'required',
            'father_name' => 'required',
            'kids_number_id' => 'required',
            'city_id' => 'required',
            'd_o_b' => 'required',
            'mobile_number' => 'required|numeric',
            'area_id' => 'required',
            'first_nationality_id' => 'required',
            'second_nationality_id' => 'required',
            'landline' => 'required',
            'street' => 'required',
            'email_address' => 'required|email',
            'blg_floor' => 'required'
        ]);

        $data = [];
        $data['first_name'] = $request->first_name;
        $data['family_name'] = $request->family_name;
        $data['father_name'] = $request->father_name;
        $data['d_o_b'] = $request->d_o_b;
        $data['first_nationality_id'] = $request->first_nationality_id;
        $data['second_nationality_id'] = $request->second_nationality_id;
        $data['gender_id'] = $request->gender_id;
        $data['civil_status'] = $request->civil_status;
        $data['kids_number_id'] = $request->kids_number_id;
        $data['mobile_number'] = $request->mobile_number;
        $data['landline'] = $request->landline;
        $data['email_address'] = $request->email_address;
        $data['country_id'] = $request->country_id;
        $data['city_id'] = $request->city_id;
        $data['area_id'] = $request->area_id;
        $data['street'] = $request->street;
        $data['blg_floor'] = $request->blg_floor;


        request()->session()->put('personal_data', $data);
        request()->session()->put('valid_personal_data', $validator->fails() ? false : true);

        return redirect()->route('careers', $locale);
    }

    public function validateEducationalData(Request $request, $locale)
    {
        request()->session()->forget('educational_data');
        request()->session()->forget('valid_educational_data');

        $validator = Validator::make($request->all(), [
            'university_id' => 'required',
            'extra_university' => 'required_without:university_id',
            'education_level_id' => 'required',
            'area_of_study_id' => 'required',
            'graduated_id' => 'required'
        ]);

        $data = [];
        $data['university_id'] = $request->university_id;
        $data['extra_university'] = $request->extra_university;
        $data['education_level_id'] = $request->education_level_id;
        $data['area_of_study_id'] = $request->area_of_study_id;
        $data['graduated_id'] = $request->graduated_id;

        request()->session()->put('educational_data', $data);
        request()->session()->put('valid_educational_data', $validator->fails() ? false : true);

        return redirect()->route('careers', $locale);
    }

    public function validateWorkData(Request $request, $locale)
    {
        request()->session()->forget('work_data');
        request()->session()->forget('valid_work_data');

        $validator = Validator::make($request->all(), [
            'employment_status_id' => 'required',
            'work_from_date' => 'required',
            'work_to_date' => 'required',
            'monthly_salary_id' => 'required',
            'experience_year_id' => 'required',
            'company_name' => 'required',
            'benefit_id' => 'required',
            'job_title' => 'required',
            'reason_of_leaving_id' => 'required',
            'job_description' => 'required'
        ]);

        $data = [];
        $data['employment_status_id'] = $request->employment_status_id;
        $data['work_from_date'] = $request->work_from_date;
        $data['work_to_date'] = $request->work_to_date;
        $data['monthly_salary_id'] = $request->monthly_salary_id;
        $data['experience_year_id'] = $request->experience_year_id;
        $data['company_name'] = $request->company_name;
        $data['benefit_id'] = $request->benefit_id;
        $data['job_title'] = $request->job_title;
        $data['reason_of_leaving_id'] = $request->reason_of_leaving_id;
        $data['job_description'] = $request->job_description;

        request()->session()->put('work_data', $data);
        request()->session()->put('valid_work_data', $validator->fails() ? false : true);

        return redirect()->route('careers', $locale);
    }

    public function validateEmploymentData(Request $request, $locale)
    {
        request()->session()->forget('employment_data');
        request()->session()->forget('valid_employment_data');

        $validator = Validator::make($request->all(), [
            'employment_interest' => 'required',
            'illness' => 'required',
            'start_date' => 'required',
            'referer_id' => 'required',
            'family_employed' => 'required',
            'job_details' => 'required',
        ]);

        $data = [];
        $data['employment_interest'] = $request->employment_interest;
        $data['illness'] = $request->illness;
        $data['start_date'] = $request->start_date;
        $data['referer_id'] = $request->referer_id;
        $data['family_employed'] = $request->family_employed;
        $data['job_details'] = $request->job_details;

        request()->session()->put('employment_data', $data);
        request()->session()->put('valid_employment_data', $validator->fails() ? false : true);

        return redirect()->route('careers', $locale);
    }

    public function validateResumeData(Request $request, $locale)
    {
        $page_info = $this->page_info();

        request()->session()->forget('resume_data');
        request()->session()->forget('valid_resume_data');

        $validator = Validator::make($request->all(), [
            'resume' => 'required|file|mimes:png,jpg,jpeg,pdf',
            'cover' => 'required|file|mimes:png,jpg,jpeg,pdf',
            'agreement' => 'required'
        ]);

        $data = [];
        $data['agreement'] = $request->agreement;

        request()->session()->put('resume_data', $data);
        request()->session()->put('valid_resume_data', $validator->fails() ? false : true);

        if (!$validator->fails()) {
            $personalData = request()->session()->get('personal_data', $data);
            $educationalData = request()->session()->get('educational_data', $data);
            $workData = request()->session()->get('work_data', $data);
            $employmentData = request()->session()->get('employment_data', $data);

            $row = new CareerApplication;
            $row->first_name = $personalData['first_name'];
            $row->gender_id = $personalData['gender_id'];
            $row->family_name = $personalData['family_name'];
            $row->father_name = $personalData['father_name'];
            $row->civil_status = $personalData['civil_status'];
            $row->country_id = $personalData['country_id'];
            $row->kids_number_id = $personalData['kids_number_id'];
            $row->city_id = $personalData['city_id'];
            $row->d_o_b = $personalData['d_o_b'];
            $row->mobile_number = $personalData['mobile_number'];
            $row->area_id = $personalData['area_id'];
            $row->first_nationality_id = $personalData['first_nationality_id'];
            $row->landline = $personalData['landline'];
            $row->street = $personalData['street'];
            $row->second_nationality_id = $personalData['second_nationality_id'];
            $row->email_address = $personalData['email_address'];
            $row->blg_floor = $personalData['blg_floor'];

            $row->university_id = $educationalData['university_id'];
            $row->extra_university = $educationalData['extra_university'];

            $row->education_level_id = $educationalData['education_level_id'];
            $row->area_of_study_id = $educationalData['area_of_study_id'];
            $row->graduated_id = $educationalData['graduated_id'];

            $row->employment_status_id = $workData['employment_status_id'];
            $row->work_from_date = $workData['work_from_date'];
            $row->work_to_date = $workData['work_to_date'];
            $row->monthly_salary_id = $workData['monthly_salary_id'];
            $row->experience_year_id = $workData['experience_year_id'];
            $row->company_name = $workData['company_name'];
            $row->benefit_id = $workData['benefit_id'];
            $row->job_title = $workData['job_title'];
            $row->reason_of_leaving_id = $workData['reason_of_leaving_id'];
            $row->job_description = $workData['job_description'];

            $row->employment_interest = $employmentData['employment_interest'];
            $row->illness = $employmentData['illness'];
            $row->start_date = $employmentData['start_date'];
            $row->referer_id = $employmentData['referer_id'];
            $row->family_employed = $employmentData['family_employed'];
            $row->job_details = $employmentData['job_details'];

            $row->cover = parent::store_file($page_info['link'], $request->cover);

            $row->resume = parent::store_file($page_info['link'], $request->resume);

            $row->agreement = true;
            $row->ip = $request->ip();

            $row->save();
        }

        return redirect()->route('careers', $locale);
    }
    */


    public function store(Request $request, $locale){
        $page_info = $this->page_info();
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'family_name' => 'required|min:2',
            'father_name' => 'required',
            'd_o_b' => 'required',
            'first_nationality' => 'required',
//            'second_nationality' => 'required',
            'gender' => 'required',
            'civil_status' => 'required',
//            'kids_number' => 'required_if,civil_status,married',
            'mobile_number' => 'required|numeric',
//            'landline' => 'required',
            'email_address' => 'required|email',
            'country' => 'required',
            'city' => 'required',
            'area' => 'required',
            'street' => 'required',
            'blg_floor' => 'required',

            'university' => 'required_without:extra_university',
            'extra_university' => 'required_without:university',
            'education_level' => 'required',
            'area_of_study' => 'required',
            'is_graduated' => 'required',
            'graduation_year' => 'required_if:is_graduated,yes',

            'employment_status' => 'required',
            'work_from_date' => 'required',
            'work_to_date' => 'required',
            'monthly_salary' => 'required',
            'experience_year' => 'required',
            'company_name' => 'required',
            'benefit' => 'required',
            'job_title' => 'required',
            'reason_of_leaving' => 'required',
            'job_description' => 'required',

            'employment_interest' => 'required',
            'illness' => 'required',
            'start_date' => 'required',
            'medical_info' => 'required_if:illness,yes',
            'referer' => 'required',
            'family_employed' => 'required',
            'job_details' => 'required',

            'resume' => 'required|file|mimes:png,jpg,jpeg,pdf',
            'cover' => 'required|file|mimes:png,jpg,jpeg,pdf',
            'agreement' => 'required'

        ]);

        if($request->civil_status == "married")
        {
            $this->validate($request, [
                'kids_number' => 'required',
            ]);
        }

        $row = new CareerApplication;
        $row->first_name = $request->first_name;
        $row->family_name = $request->family_name;
        $row->father_name = $request->father_name;
        $row->d_o_b = $request->d_o_b;
        $row->first_nationality = $request->first_nationality;
        $row->second_nationality = $request->second_nationality;
        $row->gender = $request->gender;
        $row->civil_status = $request->civil_status;
        $row->kids_number = $request->kids_number;
        $row->mobile_number = $request->mobile_number;
        $row->landline = $request->landline;
        $row->email_address = $request->email_address;
        $row->country = $request->country;
        $row->city = $request->city;
        $row->area = $request->area;
        $row->street = $request->street;
        $row->blg_floor = $request->blg_floor;

        $row->university = $request->university;
        $row->extra_university = $request->extra_university;
        $row->education_level = $request->education_level;
        $row->area_of_study = $request->area_of_study;
        $row->is_graduated = $request->is_graduated;
        $row->graduation_year = $request->graduation_year;

        $row->employment_status = $request->employment_status;
        $row->work_from_date = $request->work_from_date;
        $row->work_to_date = $request->work_to_date;
        $row->monthly_salary = $request->monthly_salary;
        $row->experience_year = $request->experience_year;
        $row->company_name = $request->company_name;
        $row->benefit = $request->benefit;
        $row->job_title = $request->job_title;
        $row->reason_of_leaving = $request->reason_of_leaving;
        $row->job_description = $request->job_description;

        $row->employment_interest = $request->employment_interest;
        $row->illness = $request->illness;
        $row->start_date = $request->start_date;
        $row->referer = $request->referer;
        $row->family_employed = $request->family_employed;
        $row->job_details = $request->job_details;
        $row->medical_info = $request->medical_info;

        $row->cover = parent::store_file($page_info['link'], $request->cover);

        $row->resume = parent::store_file($page_info['link'], $request->resume);

        $row->agreement = $request->agreement;
        $row->ip = request()->ip();

        $row->save();

        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'ONLINE EMPLOYMENT APPLICATION',
                    'param' => [

                    'first_name',
                    'family_name',
                    'father_name',
                    'd_o_b',
                    'first_nationality' ,
                    'second_nationality' ,
                    'gender' ,
                    'civil_status',
                    'kids_number' ,
                    'mobile_number' ,
                    'landline' ,
                    'email_address',
                    'country' ,
                    'city' ,
                    'area' ,
                    'street' ,
                    'blg_floor',

                    'university',
                    'extra_university' ,
                    'education_level',
                    'area_of_study' ,
                    'is_graduated' ,
                    'graduation_year',

                    'employment_status' ,
                    'work_from_date',
                    'work_to_date',
                    'monthly_salary' ,
                    'experience_year' ,
                    'company_name' ,
                    'benefit',
                    'job_title' ,
                    'reason_of_leaving' ,
                    'job_description',

                    'employment_interest',
                    'illness',
                    'start_date',
                    'medical_info' ,
                    'referer',
                    'family_employed',
                    'job_details',

                    'agreement',
                    ],
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'));
                    $message->cc(env('MAIL_DM_ADDRESS'));
                    $message->subject(env('APP_NAME') . " - Online Employment Application");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('careers', $locale)->withStatus('Quote successfully requested.');

    }
}
