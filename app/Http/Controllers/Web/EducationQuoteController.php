<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\EducationQuote;
use App\Models\Page;

use Illuminate\Http\Request;
use Mail;

class EducationQuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Education Quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'education-quote')->first();

        return view('web/pages/education-quote', compact(
            'page_title',
            'page'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'father_name' => 'required',
            'occupation' => 'required',
            'mobile_number' => 'required|numeric',
            'email' => 'required|required',
            'children_number' => 'required|max:10',
            'protection_amount' => 'required',
            'last_name' => 'required',
            'd_o_b' => 'required',
            'contribution_amount' => 'required',
            'contribution_frequency' => 'required',
            'g-recaptcha-response' => 'required'
        ]);

        if ($request->children_number > 0) {
            $this->validate($request, [
                'insurer_name' => 'required|array',
                'insurer_name.*' => 'required',
                'insurer_dob' => 'required|array',
                'insurer_dob.*' => 'required',
                'insurer_saving_age' => 'required|array',
                'insurer_saving_age.*' => 'required'
            ]);
        }



        $row = new EducationQuote;
        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->children_number = $request->children_number;
        $row->protection_amount = $request->protection_amount;
        $row->last_name = $request->last_name;
        $row->d_o_b = $request->d_o_b;
        $row->contribution_amount = $request->contribution_amount;
        $row->contribution_frequency = $request->contribution_frequency;
        $row->insurer_name = json_encode($request->insurer_name);
        $row->insurer_dob = json_encode($request->insurer_dob);
        $row->insurer_saving_age = json_encode($request->insurer_saving_age);
        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'A education quote is requested from Arope Website',
                    'param' => [
                        'first_name',
                        'father_name',
                        'last_name',
                        'email',
                        'mobile_number',
                        'landline_number',
                        'occupation',
                        'children_number',
                        'protection_amount',
                        'd_o_b',
                        'contribution_amount',
                        'contribution_frequency'
                    ]
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Request A Education Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('education-quote', $locale)->withStatus('Quote successfully requested.');
    }
}
