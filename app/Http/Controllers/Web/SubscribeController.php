<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Subscriber;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SubscribeController extends Controller
{

    public function index($locale)
    {

        $page_title = 'Subsubribe';

        return view('web/pages/home', compact(
            'page_title',
        ));
    }

    /**
     * Add a Subscriber
     *
     */
    public function subscribe(Request $request)
    {

        $this->validate($request, [
            'subscription_email' => 'required|email|unique:subscribers,email',
        ]);
        $row = new Subscriber;
        $row->email = $request['subscription_email'];
        $row->save();

        try {
            Mail::send(
                'emails.subscribtionform',
                [
                    'request' => $request->all(),
                    'param' => [
                        'subscription_email',
                    ]
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Request A Personal Quote Form");
                }
            );
            Mail::send(
                'emails.usersubscribtionform',
                [],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Request A Personal Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->back()->with('sub_message','Your email has been added to our subscribers list.');
    }
}
