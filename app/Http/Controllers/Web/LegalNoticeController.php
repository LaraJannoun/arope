<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\FixedSection;

class LegalNoticeController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Legal Notices';
        $legalNotices = FixedSection::select([
            'id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('slug', 'legal-notices')
            ->first();

        return view('web/pages/legal-notices', compact(
            'page_title',
            'legalNotices'
        ));
    }
}
