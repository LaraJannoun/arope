<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\TravelQuote;
use App\Models\Page;

use Illuminate\Http\Request;
use Mail;

class TravelQuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Travel quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'travel-quote')->first();

        return view('web/pages/travel-quote', compact(
            'page_title',
            'page'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {

        $this->validate($request, [
            'first_name' => 'required|min:2',
            'occupation' => 'required',
            'father_name' => 'required|min:2',
            'mobile_number' => 'required|numeric',
            'd_o_b' => 'required',
            'last_name' => 'required|min:2',
            'email' => 'required',
            'plan_type' => 'required',
            'trip_duration' => 'required',
            'geo_zone' => 'required',
            'destination' => 'required',
            'insurer_name' => 'required|array',
            'insurer_relation' => 'required|array',
            'insurer_year' => 'required|array',
            'g-recaptcha-response' => 'required'
        ]);
        $row = new TravelQuote;
        $row->first_name = $request->first_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->father_name = $request->father_name;
        $row->mobile_number = $request->mobile_number;
        $row->year_of_birth = $request->d_o_b;
        $row->last_name = $request->last_name;
        $row->email = $request->email;
        $row->plan_type = $request->plan_type;
        $row->trip_duration = $request->trip_duration;
        $row->geo_zone = $request->geo_zone;
        $row->destination = $request->destination;
        $row->insurer_name = json_encode($request->insurer_name);
        $row->insurer_relation = json_encode($request->insurer_relation);
        $row->insurer_year = json_encode($request->insurer_year);

        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'A travel quote is requested from Arope Website',
                    'param' => [
                        'first_name',
                        'father_name',
                        'last_name',
                        'email',
                        'mobile_number',
                        'landline_number',
                        'occupation',
                        'plan_type',
                        'trip_duration',
                        'geo_zone',
                        'destination',
                        'insurer_name',
                        'insurer_relation',
                        'insurer_year'
                    ]
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_TELE_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->cc(env('MAIL_DM_ADDRESS'));
                    $message->subject(env('APP_NAME') . " - Request A Travel Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('travel-quote', $locale)->withStatus('Quote successfully requested.');
    }
}
