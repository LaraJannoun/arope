<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Offer;

use Illuminate\Http\Request;

class OfferController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Offers';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'offers')->first();

        $offers = Offer::select([
            'id',
            'image',
            'width'
        ])->orderBy('pos')->get();

        return view('web/pages/offers', compact(
            'page_title',
            'page',
            'offers'
        ));
    }
}
