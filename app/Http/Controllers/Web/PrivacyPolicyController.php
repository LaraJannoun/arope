<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\FixedSection;

class PrivacyPolicyController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Privacy Policy';
        $privacypolicy = FixedSection::select([
            'id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('slug', 'privacy-policy')
            ->first();

        return view('web/pages/privacy-policy', compact(
            'page_title',
            'privacypolicy'
        ));
    }
}
