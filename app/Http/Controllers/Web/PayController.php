<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\PaymentStep;

use Illuminate\Http\Request;

class PayController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Pay Online';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'pay-my-policy')->first();

        $payment_steps = PaymentStep::select([
            'id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->orderBy('pos')->get();

        return view('web/pages/pay', compact(
            'page',
            'page_title',
            'payment_steps'
        ));
    }
}
