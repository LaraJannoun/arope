<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\HomeSlider;
use App\Models\Banner;
use App\Models\PersonalSolution;
use App\Models\BusinessSolution;
use App\Models\FinancialHighlight;
use App\Models\HomeLink;
use App\Models\HomeService;
use App\Models\SliderItem;

class HomeController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Insurance';

        $home_sliders = HomeSlider::select([
            'id',
            'image',
            'title_' . $locale . ' as title'
        ])->where('publish', 1)->orderBy('pos')->get();

        $slider_items = SliderItem::select([
            'id',
            'image',
            'title_' . $locale . ' as title',
            'color',
            'link'
        ])->where('publish', 1)->orderBy('pos')->get();

        $banner = Banner::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'button_link',
            'button_text_' . $locale . ' as button_text',
        ])->where('slug', 'home')->first();

        $financial_highlights = FinancialHighlight::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'price_' . $locale . ' as price',
            'home_featured'
        ])->where('home_featured', 1)->get();

        $business_solutions = BusinessSolution::select([
            'id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'home_featured',
            'page_section_id',
            'link',
            'apply_link'
        ])->where('publish', 1)->where('home_featured', 1)->orderBy('pos')->get();

        $personal_solutions = PersonalSolution::select([
            'id',
            'image',
            'home_featured',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'page_section_id',
            'link',
            'apply_link'
        ])->where('publish', 1)->where('home_featured', 1)->orderBy('pos')->get();

        $solutions = $business_solutions->merge($personal_solutions);

        $home_links = HomeLink::select([
            'id',
            'title_' . $locale . ' as title',
            'link'
        ])->orderBy('pos')->get();

        $home_services = HomeService::select([
            'id',
            'slug',
            'image',
            'link',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'button_text_' . $locale . ' as button_text',
        ])->get();

        return view('web/pages/home', compact(
            'page_title',
            'home_sliders',
            'banner',
            'solutions',
            'home_links',
            'home_services',
            'financial_highlights',
            'slider_items'
        ));
    }
}
