<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\MotorQuote;
use App\Models\Page;
use App\Models\ExternalLink;

use Illuminate\Http\Request;
use Mail;

class MotorQuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Motor quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'motor-quote')->first();

        $external_link = ExternalLink::select([
            'id',
            'type',
            'link'
        ])->where('type', 'third_party_liability')->first();


        return view('web/pages/motor-quote', compact(
            'page_title',
            'page',
            'external_link'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'father_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email',
            'mobile_number' => 'required|numeric',
            'occupation' => 'required',
            'insurance_type' => 'required',
            'vehicle_type' => 'required',
            'brand' => 'required',
            'year_of_make' => 'required',
            'usage' => 'required',
            'period' => 'required',
            'g-recaptcha-response' => 'required'

        ]);

        if ($request->insurance_type == "third_party_liability") {
            $this->validate($request, [
                'third_party_value' => 'required',
            ]);
        } elseif ($request->insurance_type == "motor_all_risk") {
            $this->validate($request, [
                'all_risk_value' => 'required',
            ]);
        } elseif ($request->insurance_type == "orange_card") {
            $this->validate($request, [
                'orange_card_destination_value' => 'required',
            ]);
        }

        $row = new MotorQuote;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->insurance_type = $request->insurance_type;
        $row->vehicle_type = $request->vehicle_type;
        $row->brand = $request->brand;
        $row->year_of_make = $request->year_of_make;
        $row->usage = $request->usage;
        if ($request->insurance_type == "third_party_liability") {
            $row->plan_details = $request->third_party_value;
            $plan_details = $request->third_party_value;
        } elseif ($request->insurance_type == "motor_all_risk") {
            $row->plan_details = $request->all_risk_value;
            $plan_details =  $request->all_risk_value;
        } elseif ($request->insurance_type == "orange_card") {
            $row->plan_details = $request->orange_card_destination_value;
            $plan_details = $request->orange_card_destination_value;;
        }
        $row->period = $request->period;

        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'A motor quote is requested from Arope Website',
                    'custom' => $plan_details,
                    'param' => [
                        'first_name',
                        'father_name',
                        'last_name',
                        'email',
                        'mobile_number',
                        'landline_number',
                        'occupation',
                        'insurance_type',
                        'vehicle_type',
                        'brand',
                        'year_of_make',
                        'usage',
                        'period',
                    ],
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_TELE_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->cc(env('MAIL_DM_ADDRESS'));
                    $message->subject(env('APP_NAME') . " - Request A Motor Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('motor-quote', $locale)->withStatus('Quote successfully requested.');
    }
}
