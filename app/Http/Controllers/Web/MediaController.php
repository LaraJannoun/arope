<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\PageSection;
use App\Models\Event;
use App\Models\Blog;
use App\Models\EventImage;
use App\Models\News;
use App\Models\PressItem;
use App\Models\PressHeader;
use App\Models\NewsImage;
use App\Models\BlogImage;
use App\Models\PublicationItem;
use App\Models\PublicationHeader;



use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function index($locale)
    {

        $page_title = 'media';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'media')->first();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'subtitle_' . $locale . ' as subtitle',
            'header_title_' . $locale . ' as header_title'
        ])->where('publish', 1)->where('page_id', $page->id)->get();

        $event_row = $page_sections->filter(function ($value, $key) {
            return $value['slug'] == 'events';
        })->first();

        $events = Event::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'text_preview_' . $locale . ' as text_preview',
            'date_' . $locale . ' as date',
            'link',
            'page_section_id'
        ])->where('publish', 1)->where('page_section_id', $event_row->id)->orderBy('pos')->get();

        $blog_row = $page_sections->filter(function ($value, $key) {
            return $value['slug'] == 'blogs';
        })->first();

        $blogs = Blog::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'date_' . $locale . ' as date',
            'text_preview_' . $locale . ' as text_preview',
            'link',
            'page_section_id'
        ])->where('publish', 1)->where('page_section_id', $blog_row->id)->orderBy('pos')->get();

        $news_row = $page_sections->filter(function ($value, $key) {
            return $value['slug'] == 'news';
        })->first();

        $news = News::select([
            'id',
            'slug',
            'image',
            'link',
            'page_section_id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'text_preview_' . $locale . ' as text_preview',
            'date_' . $locale . ' as date'
        ])->where('publish', 1)->where('page_section_id', $news_row->id)->orderBy('pos')->get();

        $press_headers =
            PressHeader::select([
                'id',
                'title_' . $locale . ' as title',
            ])->where('publish', 1)->orderBy('pos')->get();

        $press_items = PressItem::select([
            'id',
            'slug',
            'attachment',
            'link',
            'press_header_id',
            'title_' . $locale . ' as title',
            'date_' . $locale . ' as date',
        ])->where('publish', 1)->orderBy('pos')->get();

        $publication_headers =
            PublicationHeader::select([
                'id',
                'title_' . $locale . ' as title',
            ])->where('publish', 1)->orderBy('pos')->get();

        $publication_items = PublicationItem::select([
            'id',
            'slug',
            'attachment',
            'link',
            'publication_header_id',
            'title_' . $locale . ' as title',
            'date_' . $locale . ' as date',
        ])->where('publish', 1)->orderBy('pos')->get();

        return view('web/pages/media', compact(
            'page_title',
            'page',
            'page_sections',
            'events',
            'blogs',
            'news',
            'press_items',
            'press_headers',
            'publication_items',
            'publication_headers'
        ));
    }

    public function single($locale, $slug)
    {
        $page_section = request('page_section');

        $page_title = 'Media Details - ' . $slug;

        $news_item = News::select([
            'id',
            'slug',
            'image',
            'link',
            'page_section_id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'date_' . $locale . ' as date'
        ])->where('publish', 1)->where('slug', $slug)->first();

        $news_images = $news_item && $news_item->id ? NewsImage::select([
            'id',
            'image',
            'news_id'
        ])->where('news_id', $news_item->id)->get() : [];

        $events_item = Event::select([
            'id',
            'slug',
            'image',
            'link',
            'page_section_id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'date_' . $locale . ' as date'
        ])->where('publish', 1)->where('slug', $slug)->first();

        $events_images = $events_item && $events_item->id ? EventImage::select([
            'id',
            'image',
            'event_id'
        ])->where('event_id', $events_item->id)->get() : [];


        $blogs_item = Blog::select([
            'id',
            'slug',
            'image',
            'link',
            'page_section_id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'date_' . $locale . ' as date'
        ])->where('publish', 1)->where('slug', $slug)->first();

        $blogs_images = $blogs_item && $blogs_item->id ? BlogImage::select([
            'id',
            'image',
            'blog_id'
        ])->where('blog_id', $blogs_item->id)->get() : [];

        return view('web/pages/media-item-details', compact(
            'page_section',
            'slug',
            'page_title',
            'news_item',
            'news_images',
            'events_item',
            'events_images',
            'blogs_item',
            'blogs_images'
        ));
    }
}
