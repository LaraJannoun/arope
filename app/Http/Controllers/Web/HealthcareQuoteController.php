<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\HealthcareQuote;
use App\Models\Page;

use Illuminate\Http\Request;
use Mail;

class HealthcareQuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Healthcare quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'healthcare-quote')->first();

        return view('web/pages/healthcare-quote', compact(
            'page_title',
            'page'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'father_name' => 'required',
            'occupation' => 'required',
            'mobile_number' => 'required',
            'email' => 'required|required',
            'hospital_network' => 'required',
            'hospital_class' => 'required',
            'last_name' => 'required',
            'insurer_name' => 'required|array',
            'insurer_name.*' => 'required',
            'insurer_relation' => 'required|array',
            'insurer_relation.*' => 'required',
            'insurer_year' => 'required|array',
            'insurer_year.*' => 'required',
            'insurer_nssf' => 'required|array',
            'insurer_nssf.*' => 'required',
            'd_o_b' => 'required',
            'cover_type' => 'required',
            'g-recaptcha-response' => 'required'
        ]);

        $row = new HealthcareQuote;
        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->hospital_network = $request->hospital_network;
        $row->hospital_class = $request->hospital_class;
        $row->last_name = $request->last_name;
        $row->cover_type = json_encode($request->cover_type);
        $row->insurer_name = json_encode($request->insurer_name);
        $row->insurer_relation = json_encode($request->insurer_relation);
        $row->insurer_year = json_encode($request->insurer_year);
        $row->insurer_nssf = json_encode($request->insurer_nssf);
        $row->d_o_b = $request->d_o_b;
        $row->optional_covers = json_encode($request->optional_covers);
        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'A health care quote is requested from Arope Website',
                    'param' => [
                        'first_name',
                        'father_name',
                        'last_name',
                        'email',
                        'mobile_number',
                        'landline_number',
                        'occupation',
                        'hospital_network',
                        'hospital_class',
                        'cover_type',
                        'insurer_name',
                        'insurer_relation',
                        'insurer_year',
                        'insurer_nssf',
                        'd_o_b',
                    ]
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_TELE_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->cc(env('MAIL_DM_ADDRESS'));
                    $message->subject(env('APP_NAME') . " - Request A Health Care Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('healthcare-quote', $locale)->withStatus('Quote successfully requested.');
    }
}
