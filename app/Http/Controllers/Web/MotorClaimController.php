<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ClaimType;
use App\Models\Page;
use App\Models\MotorClaim;
use App\Models\FixedSection;
use App\Models\ClaimProcedure;

use Illuminate\Http\Request;
use Mail;

class MotorClaimController extends Controller
{
    public function page_info()
    {
        $page_info = [
            'link' => 'MotorClaims',
        ];
        return $page_info;
    }

    public function index($locale)
    {

        $page_title = 'Motor Claim';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'claim')->first();

        $claim_car_front = FixedSection::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'claim_car_front')->first();

        $claim_car_back = FixedSection::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'claim_car_back')->first();

        $claim_car_left = FixedSection::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'claim_car_left')->first();

        $claim_car_right = FixedSection::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'claim_car_right')->first();

        $workshops = ClaimType::select([
            'id',
            'title',
            'display_title_' . $locale . ' as display_title',
        ])->where('title', 'workshop')->get();

        $claim_procedures = ClaimProcedure::select([
            'id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->get();

        return view('web/pages/motor-claim', compact(
            'page',
            'page_title',
            'claim_car_front',
            'claim_car_back',
            'claim_car_left',
            'claim_car_right',
            'workshops',
            'claim_procedures'
        ));
    }

    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $page_info = $this->page_info();

        $this->validate($request, [
            'pas_number' => 'required',
            'plate_number' => 'required',
            'plate_code' => 'required',
            'driver_name' => 'required',
            'mobile_number' => 'required|numeric',
            'email' => 'required|email',
            'communication' => 'required',
            'accident_date' => 'required',
            'accident_time' => 'required',
            'accident_location' => 'required',
            'expert' => 'required',
            'expert_name' => 'required_if:expert,yes',
            'bodily_injuries' => 'required',
            'accident_description' => 'required',
            'front_driving_license' => 'required',
            'back_driving_license' => 'required',
            'front_car_reg_id' => 'required',
            'back_car_reg_id' => 'required',
            //            'workshop' => 'required',
            // 'different_workshop' => 'required_without:workshop',
            'data_privacy_confirm' => 'required',
            'g-recaptcha-response' => 'required'
        ]);

        $row = new MotorClaim;
        $row->pas_number = $request->pas_number;
        $row->plate_number = $request->plate_number;
        $row->plate_code = $request->plate_code;
        $row->driver_name = $request->driver_name;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->communication = $request->communication;
        $row->accident_date = $request->accident_date;
        $row->accident_time = $request->accident_time;
        $row->accident_location = $request->accident_location;
        $row->expert = $request->expert;
        $row->expert_name = $request->expert_name;
        $row->bodily_injuries = $request->bodily_injuries;
        $row->accident_description = $request->accident_description;
        $row->front_driving_license =
            parent::store_file($page_info['link'], $request->front_driving_license);
        $row->back_driving_license =
            parent::store_file($page_info['link'], $request->back_driving_license);
        $row->front_car_reg_id =
            parent::store_file($page_info['link'], $request->front_car_reg_id);
        $row->back_car_reg_id =
            parent::store_file($page_info['link'], $request->back_car_reg_id);
        $row->front_car_details = $request->front_car_details;
        $row->back_car_details = $request->back_car_details;
        $row->left_car_details = $request->left_car_details;
        $row->right_car_details = $request->right_car_details;

        $row->workshop = $request->workshop;
        // $row->different_workshop = $request->workshop;
        $row->data_privacy_confirm = $request->data_privacy_confirm;

        $row->ip = $request->ip();

        $row->save();

                 try {
                     Mail::send(
                         'emails.emailForm',
                         [
                             'request' => $request->all(),
                             'param' => [
                                 'pas_number',
                                 'plate_number',
                                 'plate_code',
                                 'driver_name',
                                 'mobile_number',
                                 'email',
                                 'communication',
                                 'accident_date',
                                 'accident_time',
                                 'accident_location',
                                 'expert',
                                 'expert_name',
                                 'bodily_injuries',
                                 'data_privacy_confirm',
                                 'front_car_details',
                                 'back_car_details',
                                 'left_car_details',
                                 'right_car_details',
                                 'accident_description'

                             ]
                         ],
                         function ($message) {
                             $message->from(env('MAIL_FROM_ADDRESS'));
                             $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                             $message->subject(env('APP_NAME') . " - Motor Claim Here Form");
                         }
                     );
                 } catch (\Exception $e) {
                     return $e->getMessage();
                 }

        return redirect()->route('motor-claim', $locale)->withStatus('Motor Claim successfully sent.');
    }
}
