<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ExpatQuote;
use App\Models\Page;

use Illuminate\Http\Request;
use Mail;

class ExpatQuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Expatriate Quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('slug', 'expatriate-quote')->first();

        return view('web/pages/expat-quote', compact(
            'page_title',
            'page'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'father_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email',
            'mobile_number' => 'required|numeric',
            'occupation' => 'required',
            'age_bracket' => 'required',
            'gender' => 'required',
            'occupation_field' => 'required',
            'd_o_b' => 'required',
            'address' => 'required',
            'g-recaptcha-response' => 'required'
        ]);

        $row = new ExpatQuote;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->age_bracket = $request->age_bracket;
        $row->gender = $request->gender;
        $row->d_o_b = $request->d_o_b;
        $row->occupation_field = $request->occupation_field;
        $row->ip = $request->ip();
        $row->address = $request->address;

        $row->save();

        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'A expatriate quote is requested from Arope Website',
                    'param' => [
                        'first_name',
                        'father_name',
                        'last_name',
                        'email',
                        'mobile_number',
                        'landline_number',
                        'occupation',
                        'age_bracket',
                        'gender',
                        'occupation_field',
                        'address'
                    ]
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Request A Expatriate Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('expat-quote', $locale)->withStatus('Quote successfully requested.');
    }
}
