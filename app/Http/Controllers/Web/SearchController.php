<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\BusinessSolution;
use App\Models\PersonalSolution;

use Illuminate\Http\Request;
use Mail;

class SearchController extends Controller
{

    public function index($locale)
    {

        $page_title = 'Search';

        return view('web/pages/search', compact(
            'page',
            'page_title'
        ));
    }

    public function search(Request $request, $locale)
    {
        $page_title = 'Store';
        // Get the search value from the request
        $search = $request->input('search');

        $personalSolutions = PersonalSolution::select([
            'id',
            'image',
            'home_featured',
            'link',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'page_section_id',
            'apply_link'
        ])->where('publish', 1)->orderBy('title_' . $locale)->get()->map(function ($query) {
            $query->page = 'personal';
            return $query;
        });


        $personalSolutionIds = $personalSolutions->filter(function ($value, $key) use ($search, $locale) {

            $title = strtolower(str_replace(" ", "", $value['title_' . $locale]));
            if (str_contains($title, $search)) {
                return str_contains($title, $search);
            }
            $text = strtolower(str_replace(" ", "", $value['text_' . $locale]));
            if (str_contains($text, $search)) {
                return
                    str_contains($text, $search);
            }
        });

        $personalSolutionArray  = [];
        if (count($personalSolutionIds) > 0) {
            foreach ($personalSolutionIds as $key => $personalSolution) {
                array_push($personalSolutionArray, $personalSolution->id);
            }
        }

        $businessSolutions = BusinessSolution::select([
            'id',
            'image',
            'home_featured',
            'link',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'page_section_id',
            'apply_link'
        ])->where('publish', 1)->orderBy('title_' . $locale)->get();

        $businessSolutionIds = $businessSolutions->filter(function ($value, $key) use ($search, $locale) {
            $value = strtolower(str_replace(" ", "", $value['title_' . $locale]));
            return str_contains($value, $search);
        });

        $businessSolutionArray  = [];
        if (count($businessSolutionIds) > 0) {
            foreach ($businessSolutionIds as $key => $businessSolution) {
                array_push($businessSolutionArray, $businessSolution->id);
            }
        }

        // Search in the title and body columns from the posts table
        $personalItems = PersonalSolution::query()
            ->where('title_' . $locale, 'LIKE', "%{$search}%");
        if (count($personalSolutionArray) > 0) {
            $personalItems = $personalItems->orWhereIn('id', $personalSolutionArray);
        }
        $personalItems = $personalItems->where('publish', 1);
        $personalItems = $personalItems->get()->map(function ($query) {
            $query->page = 'personal';
            return $query;
        });

        $businessItems = BusinessSolution::query()
            ->where('title_' . $locale, 'LIKE', "%{$search}%");
        if (count($businessSolutionArray) > 0) {
            $businessItems = $businessItems->orWhereIn('id', $businessSolutionArray);
        }
        $businessItems = $businessItems->where('publish', 1);
        $businessItems = $businessItems->get()->map(function ($query) {
            $query->page = 'business';
            return $query;
        });;

        $items = $businessItems->merge($personalItems);

        // Return the search view with the resluts compacted
        return view('web/pages/search', compact(
            'page_title',
            'items'
        ));
    }
}
