<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Quote;
use App\Models\Page;

use Illuminate\Http\Request;
use Mail;

class QuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Business quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'business-quote')->first();

        return view('web/pages/quote', compact(
            'page_title',
            'page'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'company_name' => 'required|min:2',
            'business_type' => 'required|min:2',
            'email' => 'required|email',
            'mobile_number' => 'required|numeric',
            'employees_nb' => 'required|numeric',
            'landline_number' => 'required|numeric',
            'g-recaptcha-response' => 'required'
        ]);

        $row = new Quote;
        $row->company_name = $request->company_name;
        $row->contact_person = $request->contact_person;
        $row->business_type = $request->business_type;
        $row->email = $request->email;
        $row->mobile_number = $request->mobile_number;
        $row->landline_number = $request->landline_number;
        $row->employees_nb = $request->employees_nb;
        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.requestAQuote',
                [
                    'company_name' => $request->company_name,
                    'contact_person' => $request->contact_person,
                    'business_type' => $request->business_type,
                    'employees_nb' => $request->employees_nb,
                    'mobile_number' => $request->mobile_number,
                    'email' => $request->email,
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Request A Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('quote', $locale)->withStatus('Quote successfully requested.');
    }
}
