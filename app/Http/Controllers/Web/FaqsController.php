<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Faqs;
use App\Models\FaqsHeader;
use App\Models\Page;

use Illuminate\Http\Request;

class FaqsController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Faqs';
        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'faqs')->first();

        $faqs = Faqs::select([
            'id',
            'faqs_header_id',
            'question_' . $locale . ' as question',
            'answer_' . $locale . ' as answer'
        ])->where('publish', 1)->orderBy('pos')->get();

        $faqs_headers = FaqsHeader::select([
            'id',
            'title_' . $locale . ' as title',
        ])->where('publish', 1)->orderBy('pos')->get();

        return view('web/pages/faqs', compact(
            'page',
            'page_title',
            'faqs',
            'faqs_headers'
        ));
    }
}
