<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\BoatQuote;
use App\Models\Page;

use Illuminate\Http\Request;
use Mail;

class BoatQuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Pleasure Boat Quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'boat-quote')->first();

        return view('web/pages/boat-quote', compact(
            'page_title',
            'page'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {

        $this->validate($request, [
            'first_name' => 'required|min:2',
            'father_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email',
            'mobile_number' => 'required|numeric',
            'occupation' => 'required',
            'navigation_limit' => 'required',
            'other_navigation_limit' => 'required_if:navigation_limit,Other',
            'year_of_make' => 'required',
            'basic_cover' => 'required',
            'vessel_name' => 'required',
            'optional_cover' => 'required',
            'g-recaptcha-response' => 'required'

        ]);


        $row = new BoatQuote;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->father_name = $request->father_name;
        $row->landline_number = $request->landline_number;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->email = $request->email;
        $row->navigation_limit = $request->navigation_limit;
        $row->other_navigation_limit = $request->other_navigation_limit ? $request->other_navigation_limit : '';
        $row->year_of_make = $request->year_of_make;
        $row->basic_cover = $request->basic_cover;
        $row->vessel_name = $request->vessel_name;
        $row->optional_cover = $request->optional_cover;
        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'A boat quote is requested from Arope Website',
                    'param' => [
                        'first_name',
                        'last_name',
                        'father_name',
                        'landline_number',
                        'occupation',
                        'mobile_number',
                        'email',
                        'navigation_limit',
                        'other_navigation_limit',
                        'year_of_make',
                        'basic_cover',
                        'vessel_name',
                        'optional_cover'
                    ]
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_TELE_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->cc(env('MAIL_DM_ADDRESS'));
                    $message->subject(env('APP_NAME') . " - Request A Boat Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('boat-quote', $locale)->withStatus('Quote successfully requested.');
    }
}
