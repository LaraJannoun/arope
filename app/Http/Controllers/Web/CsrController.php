<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\PageSection;
use App\Models\CSRItem;

use Illuminate\Http\Request;

class CsrController extends Controller
{
    public function index($locale)
    {

        $page_title = 'CSR';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'csr')->first();


        $csr_items = CSRItem::select([
            'id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->get();

        return view('web/pages/csr', compact(
            'page_title',
            'page',
            'csr_items'
        ));
    }
}
