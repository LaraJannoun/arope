<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\PageSection;
use App\Models\AboutItem;
use App\Models\AboutTimeline;
use App\Models\QualityPolicy;
use App\Models\Team;
use App\Models\Reinsurer;
use App\Models\FinancialHighlight;
use App\Models\AnnualReport;

use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index($locale)
    {

        $page_title = 'About Us';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'about-us')->first();

        $page_sections = PageSection::select([
            'id',
            'slug',
            'page_id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'subtitle_' . $locale . ' as subtitle',
            'header_title_' . $locale . ' as header_title'
        ])->where('publish', 1)->where('page_id', $page->id)->get();

        $about_items_section_1 = AboutItem::select([
            'id',
            'slug',
            'page_section_id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'section-1')->first();

        $about_items_section_3 = AboutItem::select([
            'id',
            'slug',
            'page_section_id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'section-3')->get();

        $about_items_section_4 = AboutItem::select([
            'id',
            'slug',
            'page_section_id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'section-4')->get();


        $vertical_timeline = AboutTimeline::select([
            'id',
            'year_' . $locale . ' as year',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'type',
        ])->where('type', 'vertical')->get();

        $horizontal_timeline = AboutTimeline::select([
            'id',
            'year_' . $locale . ' as year',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'type',
        ])->where('type', 'horizontal')->get();

        $quality_policy = QualityPolicy::select([
            'id',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'subtitle_' . $locale . ' as subtitle',
            'points_' . $locale . ' as points',
            'image',
        ])->first();


        $team = Team::select([
            'id',
            'slug',
            'page_section_id',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->get();

        $shareholders = $team->filter(function ($value, $key) {
            return $value['slug'] == 'shareholders';
        });

        $board_members = $team->filter(function ($value, $key) {
            return $value['slug'] == 'members-of-the-board';
        })->first();

        $general_management_team = $team->filter(function ($value, $key) {
            return $value['slug'] == 'general-management';
        })->first();

        $reinsureres = Reinsurer::select([
            'id',
            'image',
            'text_' . $locale . ' as text',
            'title_' . $locale . ' as title'
        ])->where('publish', 1)->get();

        $financial_highlights = FinancialHighlight::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'price_' . $locale . ' as price',
            'home_featured'
        ])->get();

        $annual_reports = AnnualReport::select([
            'id',
            'page_section_id',
            'title_' . $locale . ' as title',
            'attachment'
        ])->get();

        return view('web/pages/about-us', compact(
            'page_title',
            'page',
            'page_sections',
            'about_items_section_1',
            'about_items_section_3',
            'about_items_section_4',
            'vertical_timeline',
            'horizontal_timeline',
            'quality_policy',
            'shareholders',
            'board_members',
            'general_management_team',
            'reinsureres',
            'financial_highlights',
            'annual_reports'
        ));
    }
}
