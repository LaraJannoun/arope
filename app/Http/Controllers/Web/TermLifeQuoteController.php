<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\TermLifeQuote;
use App\Models\Page;

use Illuminate\Http\Request;
use Mail;

class TermLifeQuoteController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Term life quote';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'term-life-quote')->first();

        return view('web/pages/term-life-quote', compact(
            'page_title',
            'page'
        ));
    }
    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'father_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'occupation' => 'required|min:2',
            'email' => 'required|email',
            'mobile_number' => 'required|numeric',
            'year_of_birth' => 'required',
            'sum_insured' => 'required',
            'other_sum_insured' => 'required_if:sum_insured,other',
            'cover' => 'required',
            'policy_duration' => 'required',
            'currency' => 'required',
            'g-recaptcha-response' => 'required'
        ]);

        $row = new TermLifeQuote;
        $row->first_name = $request->first_name;
        $row->father_name = $request->father_name;
        $row->last_name = $request->last_name;
        $row->occupation = $request->occupation;
        $row->mobile_number = $request->mobile_number;
        $row->landline_number = $request->landline_number;
        $row->year_of_birth = $request->year_of_birth;
        $row->email = $request->email;
        $row->sum_insured = $request->sum_insured;
        $row->other_sum_insured = $request->other_sum_insured;
        $row->cover = json_encode($request->cover);
        $row->policy_duration = $request->policy_duration;
        $row->currency = $request->currency;
        $row->ip = $request->ip();

        $row->save();



        try {
            Mail::send(
                'emails.emailForm',
                [
                    'request' => $request->all(),
                    'title' => 'A term life quote is requested from Arope Website',
                    'param' => [
                        'first_name',
                        'father_name',
                        'last_name',
                        'email',
                        'mobile_number',
                        'landline_number',
                        'occupation',
                        'year_of_birth',
                        'sum_insured',
                        'other_sum_insured',
                        'policy_duration',
                        'cover',
                        'currency'
                    ]
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Request A Term Life Quote Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('term-life-quote', $locale)->withStatus('Quote successfully requested.');
    }
}
