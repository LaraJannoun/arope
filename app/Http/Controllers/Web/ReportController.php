<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ProblemType;
use App\Models\Page;
use App\Models\Problem;

use Illuminate\Http\Request;
use Mail;

class ReportController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Report a problem';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'report-a-problem')->first();

        $types = ProblemType::select([
            'id',
            'title',
            'display_title_' . $locale . ' as display_title',
        ])->get();

        return view('web/pages/report', compact(
            'page',
            'page_title',
            'types'
        ));
    }

    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'value' => 'required',
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email',
            'message' => 'required',
            'mobile_number' => 'required|numeric',
            'title' => 'required'
        ]);

        $row = new Problem;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->title = $request->title;
        $row->email = $request->email;
        $row->mobile_number = $request->mobile_number;
        $row->message = $request->message;
        $row->problem_type_id = $request->value;
        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.reportAProblem',
                [
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'title' => $request->title,
                    'mobile_number' => $request->mobile_number,
                    'problem_type_id' => $request->value,
                    'email' => $request->email,
                    'message' => $request->message,
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_DM_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->subject(env('APP_NAME') . " - Report A Problem Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('report', $locale)->withStatus('Report successfully sent.');
    }
}
