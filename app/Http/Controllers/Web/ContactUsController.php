<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Map;
use App\Models\ContactRequest;

use Illuminate\Http\Request;
use Mail;

class ContactUsController extends Controller
{
    public function index($locale)
    {

        $page_title = 'Contact Us';

        $page = Page::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text'
        ])->where('publish', 1)->where('slug', 'contact-us')->first();

        $maps = Map::select([
            'id',
            'lat',
            'lng',
            'main_title_' . $locale . ' as main_title',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
            'phone_' . $locale . ' as phone',
            'fax_' . $locale . ' as fax',
            'email_' . $locale . ' as email',
            'link'
        ])->where('publish', 1)->orderBy('pos')->get();

        return view('web/pages/contact-us', compact(
            'page_title',
            'page',
            'maps'
        ));
    }


    /**
     * Submit request
     *
     */
    public function submit(Request $request, $locale)
    {
        $this->validate($request, [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email',
            'mobile_number' => 'required|numeric',
            'message' => 'required'
        ]);

        $row = new ContactRequest;
        $row->first_name = $request->first_name;
        $row->last_name = $request->last_name;
        $row->email = $request->email;
        $row->mobile_number = $request->mobile_number;
        $row->message = $request->message;
        $row->time = $request->time;
        $row->subject = $request->subject;

        $row->ip = $request->ip();

        $row->save();

        try {
            Mail::send(
                'emails.contact',
                [
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'mobile_number' => $request->mobile_number,
                    'email' => $request->email,
                    'comment' => $request->message,
                    'time' => $request->time,
                    'subject' => $request->subject,
                ],
                function ($message) {
                    $message->from(env('MAIL_FROM_ADDRESS'));
                    $message->to(env('MAIL_CALL_CENTER_ADDRESS'), env('MAIL_FROM_NAME'));
                    $message->cc(env('MAIL_DM_ADDRESS'));
                    $message->subject(env('APP_NAME') . " - Contact Form");
                }
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->route('contact-us', $locale)->withStatus('Request successfully sent.');
    }
}
