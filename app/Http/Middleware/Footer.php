<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Models\FooterDetail;
use App\Models\SocialMedia;
use App\Models\Subscriber;
use App\Models\FixedSection;
use App\Models\ExternalLink;

use Illuminate\Http\Request;

use Closure;
use Auth;
use View;

class Footer extends Middleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixeds
     */
    public function handle($request, Closure $next)
    {
        $locale = substr(request()->path(), 0, 2);

        $social_media = SocialMedia::select([
            'id',
            'type',
            'link'
        ])->get();

        $footer_links = FooterDetail::select([
            'id',
            'slug',
            'title_' . $locale . ' as title',
            'value_' . $locale . ' as value',
            'link'
        ])->where('slug', 'link')->orderBy('pos')->get();

        $claim_details = FooterDetail::select([
            'id',
            'slug',
            'title_' . $locale . ' as title',
            'value_' . $locale . ' as value',
            'link'
        ])->where('slug', 'claim-details')->orderBy('pos')->get();

        $privacy_policy = FixedSection::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'privacy-policy')->first();


        $legal_notices = FixedSection::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'legal-notices')->first();

        $disclaimers = FixedSection::select([
            'id',
            'slug',
            'image',
            'title_' . $locale . ' as title',
            'text_' . $locale . ' as text',
        ])->where('slug', 'disclaimers')->first();

        $login_external_link = ExternalLink::select([
            'id',
            'type',
            'link'
        ])->where('type', 'login')->first();

        View::share(compact(
            'social_media',
            'footer_links',
            'claim_details',
            'privacy_policy',
            'legal_notices',
            'disclaimers',
            'login_external_link'
        ));
        return $next($request);
    }
}
