<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageSection extends Model
{
    protected $table = 'page_section';

    public function Page()
    {
        return $this->belongsTo('App\Models\Page', 'page_id');
    }
}
