<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogImage extends Model
{
    public function Blog()
    {
        return $this->belongsTo('App\Models\Blog', 'blog_id');
    }
}
