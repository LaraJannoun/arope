<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CSRItem extends Model
{
    protected $table = 'csr_items';
}
