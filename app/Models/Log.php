<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public function Admin()
    {
        return $this->belongsTo('App\Models\Admin', 'admin_id');
    }
}
