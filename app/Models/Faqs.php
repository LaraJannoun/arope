<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faqs extends Model
{
    protected $table = 'faqs';

    public function FaqsHeader()
    {
        return $this->belongsTo('App\Models\FaqsHeader', 'faqs_header_id');
    }
}
