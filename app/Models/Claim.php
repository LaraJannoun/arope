<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    public function ClaimType()
    {
        return $this->belongsTo('App\Models\ClaimType', 'claim_type_id');
    }
}
