<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventImage extends Model
{
    public function Event()
    {
        return $this->belongsTo('App\Models\Event', 'event_id');
    }
}
