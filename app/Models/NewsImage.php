<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsImage extends Model
{
    public function News()
    {
        return $this->belongsTo('App\Models\News', 'news_id');
    }
}
