<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function PageSection()
    {
        return $this->belongsTo('App\Models\PageSection', 'page_section_id');
    }
}
