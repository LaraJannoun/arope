<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PressItem extends Model
{
    public function PressHeader()
    {
        return $this->belongsTo('App\Models\PressHeader', 'press_header_id');
    }
}
