<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemImage extends Model
{
    public function Item()
    {
        return $this->belongsTo('App\Models\Item', 'item_id');
    }

    public function PageSection()
    {
        return $this->belongsTo('App\Models\PageSection', 'page_section_id');
    }
}
