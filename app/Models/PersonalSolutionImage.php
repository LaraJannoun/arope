<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalSolutionImage extends Model
{
    public function PersonalSolution()
    {
        return $this->belongsTo('App\Models\PersonalSolution', 'personal_solution_id');
    }
}
