<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{
    public function ProblemType()
    {
        return $this->belongsTo('App\Models\ProblemType', 'problem_type_id');
    }
}
