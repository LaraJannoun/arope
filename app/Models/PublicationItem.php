<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicationItem extends Model
{
    public function PublicationHeader()
    {
        return $this->belongsTo('App\Models\PublicationHeader', 'publication_header_id');
    }
}
