<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QualityPolicy extends Model
{
    protected $table = 'quality_policy';
}
