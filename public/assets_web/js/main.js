$(document).ready(function() {
    /* main slider in home */
    const slider = $(".slider-images-container");
    const slider2 = $(".slider-clients-container");
    const slider3 = $(".slider-products-container");
    const slider4 = $(".slider-accademy-container");
    const slider5 = $(".slider-csr-container");
    const slider6 = $(".slider-images-solutions-container");
    const slider7 = $(".slider-images-item-container");
    const slider8 = $(".slider-objects-container");

    slider.each(function() {
        $(this).slick({
            dots: true,
            autoplay: false,
            autoplaySpeed: 4000,
            infinite: true,
            arrows: false,
            pauseOnHover: false,
            customPaging: function(slider, i) {
                return '<div class="rotate-left">' + 0 + (i + 1) + '</div>';
            }
        });
    });

    slider2.each(function() {
        $(this).slick({
            dots: false,
            autoplay: false,
            autoplaySpeed: 4000,
            infinite: true,
            arrows: false,
            pauseOnHover: false,
            slidesToShow: 8,
            slidesToScroll: 8,
        });
    });

    /* main slider in home */
    slider3.each(function() {
        $(this).slick({
            dots: false,
            autoplay: false,
            autoplaySpeed: 4000,
            infinite: true,
            arrows: true,
            pauseOnHover: false,
            slidesToShow: 3,
            slidesToScroll: 3,
            prevArrow: $(".arrow-product-left"),
            nextArrow: $(".arrow-product-right"),
            responsive: [{
                    arrows: false,
                    dots: true,
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                    }
                },
                {
                    arrows: false,
                    dots: true,
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    arrows: false,
                    dots: true,
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });

    slider4.each(function() {
        $(this).slick({
            dots: true,
            autoplay: false,
            autoplaySpeed: 4000,
            infinite: true,
            arrows: false,
            pauseOnHover: false,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
    });

    slider5.each(function() {
        $(this).slick({
            dots: true,
            autoplay: false,
            autoplaySpeed: 4000,
            infinite: true,
            arrows: false,
            pauseOnHover: false,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
    });

    slider6.each(function() {
        $(this).slick({
            dots: true,
            autoplay: true,
            autoplaySpeed: 4000,
            infinite: true,
            arrows: false,
            pauseOnHover: false,
            responsive: [{
                    arrows: false,
                    dots: true,
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                    }
                },
                {
                    arrows: false,
                    dots: true,
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    arrows: false,
                    dots: true,
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });

    slider7.each(function() {
        $(this).slick({
            dots: true,
            autoplay: false,
            autoplaySpeed: 4000,
            infinite: true,
            arrows: false,
            pauseOnHover: false
        });
    });

    slider8.each(function() {
        $(this).slick({
            dots: true,
            autoplay: false,
            autoplaySpeed: 4000,
            infinite: false,
            arrows: false,
            pauseOnHover: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [{
                    arrows: false,
                    dots: true,
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true,
                    }
                },
                {
                    arrows: false,
                    dots: true,
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    arrows: false,
                    dots: true,
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
    /*-------------------------------------------------------------------*/


    (function() {

        'use strict';

        // define variables
        var items = document.querySelectorAll(".timeline li");

        // check if an element is in viewport
        // http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
        function isElementInViewport(el) {
            var rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        }

        function callbackFunc() {
            for (var i = 0; i < items.length; i++) {
                if (isElementInViewport(items[i])) {
                    items[i].classList.add("in-view");
                }
            }
        }

        // listen for events
        window.addEventListener("load", callbackFunc);
        window.addEventListener("resize", callbackFunc);
        window.addEventListener("scroll", callbackFunc);

    })();

    ////horizontal timeline
    (function() {

        // VARIABLES
        const timeline = document.querySelector(".horizontal-timeline ol"),
            elH = document.querySelectorAll(".horizontal-timeline li > div"),
            arrows = document.querySelectorAll(".horizontal-timeline .arrows .hor-arrow"),
            arrowPrev = document.querySelector(".horizontal-timeline .arrows .arrow__prev"),
            arrowNext = document.querySelector(".horizontal-timeline .arrows .arrow__next"),
            firstItem = document.querySelector(".horizontal-timeline li:first-child"),
            lastItem = document.querySelector(".horizontal-timeline li:last-child"),
            xScrolling = 280,
            disabledClass = "disabled";

        // START
        window.addEventListener("load", init);

        function init() {
            setEqualHeights(elH);
            animateTl(xScrolling, arrows, timeline);
            setKeyboardFn(arrowPrev, arrowNext);
        }

        // SET EQUAL HEIGHTS
        function setEqualHeights(el) {
            let counter = 0;
            for (let i = 0; i < el.length; i++) {
                const singleHeight = el[i].offsetHeight;

                if (counter < singleHeight) {
                    counter = singleHeight;
                }
            }

            for (let i = 0; i < el.length; i++) {
                el[i].style.height = `${counter}px`;
            }
        }

        // CHECK IF AN ELEMENT IS IN VIEWPORT
        // http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
        function isElementInViewport(el) {
            const rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        }

        // SET STATE OF PREV/NEXT ARROWS
        function setBtnState(el, flag = true) {
            if (flag) {
                el.classList.add(disabledClass);
            } else {
                if (el.classList.contains(disabledClass)) {
                    el.classList.remove(disabledClass);
                }
                el.disabled = false;
            }
        }

        // ANIMATE TIMELINE
        function animateTl(scrolling, el, tl) {
            let counter = 0;
            for (let i = 0; i < el.length; i++) {
                el[i].addEventListener("click", function() {
                    if (!arrowPrev.disabled) {
                        arrowPrev.disabled = true;
                    }
                    if (!arrowNext.disabled) {
                        arrowNext.disabled = true;
                    }
                    const sign = (this.classList.contains("arrow__prev")) ? "" : "-";
                    if (counter === 0) {
                        tl.style.transform = `translateX(-${scrolling}px)`;
                    } else {
                        const tlStyle = getComputedStyle(tl);
                        // add more browser prefixes if needed here
                        const tlTransform = tlStyle.getPropertyValue("-webkit-transform") || tlStyle.getPropertyValue("transform");
                        const values = parseInt(tlTransform.split(",")[4]) + parseInt(`${sign}${scrolling}`);
                        tl.style.transform = `translateX(${values}px)`;
                    }

                    setTimeout(() => {
                        isElementInViewport(firstItem) ? setBtnState(arrowPrev) : setBtnState(arrowPrev, false);
                        isElementInViewport(lastItem) ? setBtnState(arrowNext) : setBtnState(arrowNext, false);
                    }, 1100);

                    counter++;
                });
            }
        }


        // ADD BASIC KEYBOARD FUNCTIONALITY
        function setKeyboardFn(prev, next) {
            document.addEventListener("keydown", (e) => {
                if ((e.which === 37) || (e.which === 39)) {
                    const timelineOfTop = timeline.offsetTop;
                    const y = window.pageYOffset;
                    if (timelineOfTop !== y) {
                        window.scrollTo(0, timelineOfTop);
                    }
                    if (e.which === 37) {
                        prev.click();
                    } else if (e.which === 39) {
                        next.click();
                    }
                }
            });
        }

    })();

    /*---------------------------Dropdown functionality---------------------------------------*/

    $('.dropdown-el').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('expanded');
        $('#' + $(e.target).attr('for')).prop('checked', true);
    });
    $(document).click(function() {
        $('.dropdown-el').removeClass('expanded');
    });
    /*-------------------------------------------------------------------*/
});