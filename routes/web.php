<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home', 'en');
});

Route::namespace('App\Http\Controllers\Web')->middleware('locale')->prefix('{locale}')->group(function () {
    Route::middleware(['footer'])->group(function () {
        Route::get('/', 'HomeController@index')->name('home');

        Route::get('/solutions', 'SolutionsController@index')->name('solutions');
        Route::get('/solutions/{slug}', 'SolutionsController@single')->name('personal-solution-item-details');

        Route::get('/solutions-business', 'SolutionsBusinessController@index')->name('solutions-business');
        Route::get('/solutions-business/{slug}', 'SolutionsBusinessController@single')->name('business-solution-item-details');

        Route::get('/about-us', 'AboutUsController@index')->name('about-us');

        Route::get('subscribe', 'SubscribeController@index')->name('subscribe');
        Route::post('subscribe/submit', 'SubscribeController@subscribe')->name('subscribe.submit');

        Route::get('/careers', 'CareersController@index')->name('careers');
//        Route::post('careers/submit', 'CareersController@submit')->name('careers.submit');
//        Route::post('careers/validatePersonalInfo', 'CareersController@validatePersonalInfo')->name('careers.validatePersonalInfo');
//        Route::post('careers/validateEducationalData', 'CareersController@validateEducationalData')->name('careers.validateEducationalData');
//        Route::post('careers/validateWorkData', 'CareersController@validateWorkData')->name('careers.validateWorkData');
//        Route::post('careers/validateEmploymentData', 'CareersController@validateEmploymentData')->name('careers.validateEmploymentData');
//        Route::post('careers/validateResumeData', 'CareersController@validateResumeData')->name('careers.validateResumeData');
        Route::post('careers/store', 'CareersController@store')->name('careers.store');

        Route::get('/csr', 'CsrController@index')->name('csr');

        Route::get('/media', 'MediaController@index')->name('media');
        Route::get('/media/{slug}', 'MediaController@single')->name('media-item-details');

        Route::get('/report', 'ReportController@index')->name('report');
        Route::post('report/submit', 'ReportController@submit')->name('report.submit');

        Route::get('/quote', 'QuoteController@index')->name('quote');
        Route::post('quote/submit', 'QuoteController@submit')->name('quote.submit');

        Route::get('/personal-quote', 'PersonalQuoteController@index')->name('personal-quote');
        Route::post('personal-quote/submit', 'PersonalQuoteController@submit')->name('personal-quote.submit');

        Route::get('/travel-quote', 'TravelQuoteController@index')->name('travel-quote');
        Route::post('travel-quote/submit', 'TravelQuoteController@submit')->name('travel-quote.submit');

        Route::get('/term-life-quote', 'TermLifeQuoteController@index')->name('term-life-quote');
        Route::post('term-life-quote/submit', 'TermLifeQuoteController@submit')->name('term-life-quote.submit');

        Route::get('/healthcare-quote', 'HealthcareQuoteController@index')->name('healthcare-quote');
        Route::post('healthcare-quote/submit', 'HealthcareQuoteController@submit')->name('healthcare-quote.submit');

        Route::get('/expat-quote', 'ExpatQuoteController@index')->name('expat-quote');
        Route::post('expat-quote/submit', 'ExpatQuoteController@submit')->name('expat-quote.submit');

        Route::get('/retirement-quote', 'RetirementQuoteController@index')->name('retirement-quote');
        Route::post('retirement-quote/submit', 'RetirementQuoteController@submit')->name('retirement-quote.submit');

        Route::get('/education-quote', 'EducationQuoteController@index')->name('education-quote');
        Route::post('education-quote/submit', 'EducationQuoteController@submit')->name('education-quote.submit');

        Route::get('/motor-quote', 'MotorQuoteController@index')->name('motor-quote');
        Route::post('motor-quote/submit', 'MotorQuoteController@submit')->name('motor-quote.submit');

        Route::get('/household-quote', 'HouseholdQuoteController@index')->name('household-quote');
        Route::post('household-quote/submit', 'HouseholdQuoteController@submit')->name('household-quote.submit');

        Route::get('/boat-quote', 'BoatQuoteController@index')->name('boat-quote');
        Route::post('boat-quote/submit', 'BoatQuoteController@submit')->name('boat-quote.submit');

        Route::get('/faqs', 'FaqsController@index')->name('faqs');

        Route::get('/claim', 'ClaimController@index')->name('claim');
        Route::post('claim/submit', 'ClaimController@submit')->name('claim.submit');

        Route::get('/motor-claim', 'MotorClaimController@index')->name('motor-claim');
        Route::post('motor-claim/submit', 'MotorClaimController@submit')->name('motor-claim.submit');

        Route::get('/pay', 'PayController@index')->name('pay');
        Route::get('/offers', 'OfferController@index')->name('offers');

        Route::get('/contact-us', 'ContactUsController@index')->name('contact-us');
        Route::post('contact-us/submit', 'ContactUsController@submit')->name('contact-us.submit');

        Route::get('/privacy-policy', 'PrivacyPolicyController@index')->name('privacy-policy');
        Route::get('/legal-notices', 'LegalNoticeController@index')->name('legal-notices');
        Route::get('/disclaimers', 'DisclaimerController@index')->name('disclaimers');

        Route::get('search', 'SearchController@index')->name('search');
        Route::get('search', 'SearchController@search')->name('search.submit');
    });
});
