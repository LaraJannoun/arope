<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//=========================================================================
//================================== CMS ==================================
//=========================================================================

/*
|--------------------------------------------------------------------------
| CMS routes for guests admins
|--------------------------------------------------------------------------
|
*/

Route::get('/admin', function () {
	return redirect(route('admin.login'));
});

Route::middleware('guest:admin')->prefix('admin')->namespace('App\Http\Controllers\Cms')->name('admin.')->group(function () {

	// ADMIN LOGIN ROUTE
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('login', 'Auth\LoginController@login');
});
/*
|--------------------------------------------------------------------------
| CMS routes for Authenticated Admins
|--------------------------------------------------------------------------
|
*/

Route::middleware('auth:admin')->prefix('admin')->namespace('App\Http\Controllers\Cms')->name('admin.')->group(function () {

	// ADMIN LOGOUT ROUTE
	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	// PROFILE PAGE
	Route::prefix('profile')->name('profile.')->group(function () {
		Route::get('/', 'ProfileController@edit')->name('edit');
		Route::put('/', 'ProfileController@update')->name('update');
		Route::put('/password', 'ProfileController@password')->name('password');
	});

	// SETTINGS PAGE
	Route::get('settings', 'SettingController@index')->name('settings');
	Route::post('settings/maintenance', 'SettingController@maintenance')->name('settings.maintenance');

	// DASHBOARD
	Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

	// ADMINS MANAGEMENT
	Route::resource('admins', 'AdminController', ['except' => ['show']]);
	Route::post('admins/block', 'AdminController@block')->name('admins.block');

	// SEO META TAGS
	Route::resource('seo-meta-tags', 'SeoMetaTagController', ['only' => ['edit', 'update']]);

	//============================================================


	/*
	* PLATFORM COntent
	*/

	Route::resource('about-items', 'AboutItemController');
	Route::resource('about-timelines', 'AboutTimelineController');
	Route::resource('annual-reports', 'AnnualReportController');

	// BANNERS
	Route::resource('banners', 'BannerController');

	Route::get('blogs/order', 'BlogController@order')->name('blogs.order');
	Route::post('blogs/order', 'BlogController@orderSubmit');
	Route::post('blogs/publish', 'BlogController@publish')->name('blogs.publish');
	Route::resource('blogs', 'BlogController');

	Route::get('business-solutions/order', 'BusinessSolutionController@order')->name('business-solutions.order');
	Route::post('business-solutions/order', 'BusinessSolutionController@orderSubmit');
	Route::post('business-solutions/publish', 'BusinessSolutionController@publish')->name('business-solutions.publish');
	Route::resource('business-solutions', 'BusinessSolutionController');

	Route::post('career-items/publish', 'CareerItemController@publish')->name('career-items.publish');
	Route::resource('career-items', 'CareerItemController');

	Route::resource('career-lists', 'CareerListController');

	Route::get('career-positions/order', 'CareerPositionController@order')->name('career-positions.order');
	Route::post('career-positions/order', 'CareerPositionController@orderSubmit');
	Route::post('career-positions/publish', 'CareerPositionController@publish')->name('career-positions.publish');
	Route::resource('career-positions', 'CareerPositionController');

	Route::get('claims/export', 'ClaimController@export')->name('claims.export');
	Route::resource('claims', 'ClaimController');

	Route::get('motor-claims/export', 'MotorClaimController@export')->name('motor-claims.export');
	Route::resource('motor-claims', 'MotorClaimController');


	Route::get('career-applications/export', 'CareerApplicationController@export')->name('career-applications.export');
	Route::resource('career-applications', 'CareerApplicationController');

	Route::resource('claim-procedures', 'ClaimProcedureController');
	Route::resource('claim-types', 'ClaimTypeController');

	Route::get('contact-requests/export', 'ContactRequestController@export')->name('contact-requests.export');
	Route::resource('contact-requests', 'ContactRequestController');

	Route::post('csr-items/publish', 'CSRItemController@publish')->name('csr-items.publish');
	Route::resource('csr-items', 'CSRItemController');

	Route::get('events/order', 'EventController@order')->name('events.order');
	Route::post('events/order', 'EventController@orderSubmit');
	Route::post('events/publish', 'EventController@publish')->name('events.publish');
	Route::resource('events', 'EventController');

	Route::get('faqs-headers/order', 'FaqsHeaderController@order')->name('faqs-headers.order');
	Route::post('faqs-headers/order', 'FaqsHeaderController@orderSubmit');
	Route::post('faqs-headers/publish', 'FaqsHeaderController@publish')->name('faqs-headers.publish');
	Route::resource('faqs-headers', 'FaqsHeaderController');

	Route::post('items/feature', 'ItemController@feature')->name('items.feature');
	Route::post('items/publish', 'ItemController@publish')->name('items.publish');
	Route::resource(
		'items',
		'ItemController'
	);

	Route::post('reinsurers/publish', 'ReinsurerController@publish')->name('reinsurers.publish');
	Route::resource('reinsurers', 'ReinsurerController');

	Route::resource('news-images', 'NewsImageController');
	Route::resource('event-images', 'EventImageController');
	Route::resource('blog-images', 'BlogImageController');
	Route::resource('item-images', 'ItemImageController');

	Route::get('all-faqs/order', 'FaqsController@order')->name('all-faqs.order');
	Route::post('all-faqs/order', 'FaqsController@orderSubmit');
	Route::post('all-faqs/publish', 'FaqsController@publish')->name('all-faqs.publish');
	Route::resource('all-faqs', 'FaqsController');

	Route::resource('financial-highlights', 'FinancialHighlightController');

	Route::get('footer-details/order', 'FooterDetailController@order')->name('footer-details.order');
	Route::post('footer-details/order', 'FooterDetailController@orderSubmit');
	Route::resource('footer-details', 'FooterDetailController');

	Route::get('home-links/order', 'HomeLinkController@order')->name('home-links.order');
	Route::post('home-links/order', 'HomeLinkController@orderSubmit');
	Route::post('home-links/publish', 'HomeLinkController@publish')->name('home-links.publish');
	Route::resource('home-links', 'HomeLinkController');

	Route::resource('home-services', 'HomeServiceController');

	Route::get('home-sliders/order', 'HomeSliderController@order')->name('home-sliders.order');
	Route::post('home-sliders/order', 'HomeSliderController@orderSubmit');
	Route::post('home-sliders/publish', 'HomeSliderController@publish')->name('home-sliders.publish');
	Route::resource('home-sliders', 'HomeSliderController');

	Route::get('slider-items/order', 'SliderItemController@order')->name('slider-items.order');
	Route::post('slider-items/order', 'SliderItemController@orderSubmit');
	Route::post('slider-items/publish', 'SliderItemController@publish')->name('slider-items.publish');
	Route::resource('slider-items', 'SliderItemController');

	Route::get('maps/order', 'MapController@order')->name('maps.order');
	Route::post('maps/order', 'MapController@orderSubmit');
	Route::post('maps/publish', 'MapController@publish')->name('maps.publish');
	Route::resource('maps', 'MapController');

	Route::get('news/order', 'NewsController@order')->name('news.order');
	Route::post('news/order', 'NewsController@orderSubmit');
	Route::post('news/publish', 'NewsController@publish')->name('news.publish');
	Route::resource('news', 'NewsController');

	Route::get('all-offers/order', 'OfferController@order')->name('all-offers.order');
	Route::post('all-offers/order', 'OfferController@orderSubmit');
	Route::post('all-offers/publish', 'OfferController@publish')->name('all-offers.publish');
	Route::resource('all-offers', 'OfferController');

	Route::get('pages/order', 'PageController@order')->name('pages.order');
	Route::post('pages/order', 'PageController@orderSubmit');
	Route::post('pages/publish', 'PageController@publish')->name('pages.publish');
	Route::resource('pages', 'PageController');

	Route::get('page-sections/order', 'PageSectionController@order')->name('page-sections.order');
	Route::post('page-sections/order', 'PageSectionController@orderSubmit');
	Route::post('page-sections/publish', 'PageSectionController@publish')->name('page-sections.publish');
	Route::resource('page-sections', 'PageSectionController');

	Route::get('payment-steps/order', 'PaymentStepController@order')->name('payment-steps.order');
	Route::post('payment-steps/order', 'PaymentStepController@orderSubmit');
	Route::post('payment-steps/publish', 'PaymentStepController@publish')->name('payment-steps.publish');
	Route::resource('payment-steps', 'PaymentStepController');

	Route::get('personal-solutions/order', 'PersonalSolutionController@order')->name('personal-solutions.order');
	Route::post('personal-solutions/order', 'PersonalSolutionController@orderSubmit');
	Route::post('personal-solutions/publish', 'PersonalSolutionController@publish')->name('personal-solutions.publish');
	Route::resource('personal-solutions', 'PersonalSolutionController');

	Route::get('press-headers/order', 'PressHeaderController@order')->name('press-headers.order');
	Route::post('press-headers/order', 'PressHeaderController@orderSubmit');
	Route::post('press-headers/publish', 'PressHeaderController@publish')->name('press-headers.publish');
	Route::resource('press-headers', 'PressHeaderController');

	Route::get('press-items/order', 'PressItemController@order')->name('press-items.order');
	Route::post('press-items/order', 'PressItemController@orderSubmit');
	Route::resource('press-items', 'PressItemController');
	Route::post('press-items/publish', 'PressItemController@publish')->name('press-items.publish');

	Route::get('publication-headers/order', 'PublicationHeaderController@order')->name('publication-headers.order');
	Route::post('publication-headers/order', 'PublicationHeaderController@orderSubmit');
	Route::post('publication-headers/publish', 'PublicationHeaderController@publish')->name('publication-headers.publish');
	Route::resource('publication-headers', 'PublicationHeaderController');

	Route::get('publication-items/order', 'PublicationItemController@order')->name('publication-items.order');
	Route::post('publication-items/order', 'PublicationItemController@orderSubmit');
	Route::post('publication-items/publish', 'PublicationItemController@publish')->name('publication-items.publish');
	Route::resource('publication-items', 'PublicationItemController');

	Route::get('problems/export', 'ProblemController@export')->name('problems.export');
	Route::resource('problems', 'ProblemController');

	Route::resource('problem-types', 'ProblemTypeController');
	Route::resource('quality-policy', 'QualityPolicyController');

	Route::get('quotes/export', 'QuoteController@export')->name('quotes.export');
	Route::resource('quotes', 'QuoteController');

	Route::get('personal-quotes/export', 'PersonalQuoteController@export')->name('personal-quotes.export');
	Route::resource('personal-quotes', 'PersonalQuoteController');

	Route::get('travel-quotes/export', 'TravelQuoteController@export')->name('travel-quotes.export');
	Route::resource('travel-quotes', 'TravelQuoteController');

	Route::get('term-life-quotes/export', 'TermLifeQuoteController@export')->name('term-life-quotes.export');
	Route::resource('term-life-quotes', 'TermLifeQuoteController');

	Route::get('education-quotes/export', 'EducationQuoteController@export')->name('education-quotes.export');
	Route::resource('education-quotes', 'EducationQuoteController');

	Route::get('retirement-quotes/export', 'RetirementQuoteController@export')->name('retirement-quotes.export');
	Route::resource('retirement-quotes', 'RetirementQuoteController');

	Route::get('household-quotes/export', 'HouseholdQuoteController@export')->name('household-quotes.export');
	Route::resource('household-quotes', 'HouseholdQuoteController');

	Route::get('healthcare-quotes/export', 'HealthcareQuoteController@export')->name('healthcare-quotes.export');
	Route::resource('healthcare-quotes', 'HealthcareQuoteController');

	Route::get('boat-quotes/export', 'BoatQuoteController@export')->name('boat-quotes.export');
	Route::resource('boat-quotes', 'BoatQuoteController');

	Route::get('motor-quotes/export', 'MotorQuoteController@export')->name('motor-quotes.export');
	Route::resource('motor-quotes', 'MotorQuoteController');

	Route::get('expat-quotes/export', 'ExpatQuoteController@export')->name('expat-quotes.export');
	Route::resource('expat-quotes', 'ExpatQuoteController');

	Route::resource('seo-meta-tags', 'SeoMetaTagController');
	Route::resource('settings', 'SettingController');
	Route::resource('social-media', 'SocialMediaController');
	Route::resource('external-links', 'ExternalLinkController');

	Route::get('subscribers/export', 'SubscriberController@export')->name('subscribers.export');
	Route::resource('subscribers', 'SubscriberController');

	Route::resource('team', 'TeamController');

	Route::get('testimonials/order', 'TestimonialController@order')->name('testimonials.order');
	Route::post('testimonials/order', 'TestimonialController@orderSubmit');
	Route::post('testimonials/publish', 'TestimonialController@publish')->name('testimonials.publish');
	Route::resource('testimonials', 'TestimonialController');

	// FIXED SECTIONS
	Route::get('fixed-sections/{id}/image/remove', 'FixedSectionController@imageRemove')->name('fixed-sections.image.remove');
	Route::get('fixed-sections/categoryIndex', 'FixedSectionController@categoryIndex')->name('fixed-sections.categoryIndex');
	Route::resource('fixed-sections', 'FixedSectionController');

	// FORMS EMAILS
	// Route::resource('forms-emails', 'FormsEmailController', ['except' => ['destroy']]);
	//============================================================

});
